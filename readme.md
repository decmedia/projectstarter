# Project Starter
Lightweight, production-ready starter REST app. Angular + Laravel.

Web project starter kit including modern tools and workflow based on angular-cli, best practices from the community, a scalable base template and a good learning base.

Проект состоит из двух самостоятельных частей. Angular - frontend, Laravel - backend, которые общаются между собой посредством HTTP API

## Установка
Клонировать в текущую папку и удалить все лишнее (одна команда):
```
git clone https://bitbucket.org/atholding/projectstarter .; rm -rf trunk .gitignore readme.md .git
```

Настроить Frontend и Backend по отдельности

### Frontend
Приложение написано с использованием  [Angular](https://angular.io/)

1. Установите зависимости:
```
cd frontend && npm i
```

2. Отредактируйте файл ```frontend/src/app/config/settings.ts``` указав настройки:
```typescript
APP_NAME = ''; // Название проекта
APP_URL = '';
```

3. Приложение готово к запуску

#### Основные команды
- __npm run start__: создает приложение, запускает сервер разработки, просматривает исходные файлы и перестраивает приложение по мере внесения изменений в эти файлы.
- __npm run build__: создает приложение, удаляет неиспользуемое и копирует то что получилось в backend/public для доступа извне
- __npm run build:stats__: собрать проект для анализа частей и размеров модулей 
- __npm run analyze__: запустить средство просмотра собранного анализа


### Backend
Приложение написано с использованием [Laravel](https://laravel.com/)

1. Установить пакеты
```
cd backend && composer i
```
Если некоторые страницы планируется отображать отрендеренными на сервере, необходимо установить дополнительно:
```
cd backend && npm i
```
2. Переименовать ```/backend/.env.example``` в ```.env``` и заполнить параметры:
```text
APP_NAME=
APP_URL=

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
где,

- APP_NAME: Название вашего проекта. Например: Yandex. Используется в письмах и отрендеренных на сервере страницах (если используются)
- APP_URL: Url вашего проекта доступного локально. Пример: http://yandex.local

3. Создать новый ключ приложения
```
php artisan key:generate
```

4. Создать таблицы в БД и наполнить их тестовым содержимым
```
php artisan migrate
php artisan db:seed
```
Будет создан пользователь: ```admin@example.com``` с паролем ```password``` 

5. Создать символические ссылки, если файловое хранилище будет использовано по умолчанию
```text
php artisan storage:link
```
6. Запустить тесты
```text
php artisan test
```


----------
### Работа с Websockets

На Backend поднимается демон Websocket сервера. Каждое подключение с Frontend, авторизуется с 
использованием того же токена, что используется и для запросов HTTP API.

Направление связи доступное по умолчанию: Frontend <-> Backend.

Связь Frontend <-> Frontend, по умолчанию отключена.

#### Активация
1. Указать параметры подключения в ```/backend/.env```:
```
WEBSOCKET_BROADCAST_HOST=192.168.10.10
WEBSOCKET_BROADCAST_PORT=6001

PUSHER_APP_ID=ex
PUSHER_APP_KEY=ex
PUSHER_APP_SECRET=

BROADCAST_DRIVER=pusher
```
где,
- WEBSOCKET_BROADCAST_HOST: локальный адрес сервера, на котором запущено приложение. Для prod как правило будет: 127.0.0.1. Для локальной разработки: 192.168.10.10 при использовании с [Homestead](https://laravel.com/docs/8.x/homestead)
- PUSHER_APP_KEY: Идентификатор приложения
- PUSHER_APP_SECRET: Секретный ключ приложения

2. Отредактируйте файл ```frontend/src/app/config/settings.ts``` указав настройки:
```typescript
WS_ENABLED = true;
WS_HOST = '';
WS_PORT = '';
PUSHER_APP_KEY = '';
```
где,
- WS_HOST: Домен проекта
- PUSHER_APP_KEY: Идентификатор приложения. Должен совпадать с одноименным в ```.env``` файле

3. Запустите сервер
```text
php artisan websockets:serve
```

После установки соединения, Frontend посылает запрос на /broadcasting/auth и передает пользовательский токен. 
Если авторизация прошла успешно, пользователь подписывается на сообщения с сервера.

Имя приватного канала, на который подпишется Frontend после авторизации, будет:
```User.{id пользователя}```

Пример отправки сообщения с Backend на Frontend по websockets 

```php
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;

class VerifiedEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function via()
    {
        return ['broadcast'];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'payload' => [
                'id' => $notifiable->id,
                'email' => $notifiable->email
                //...
                // полезная нагрузка
            ]
        ]);
    }
}
```
Где в массиве по ключу payload, передается полезная нагрузка
```php
$user->notify(new VerifiedEmailNotification());
```

На Frontend при этом, сообщение может быть получено так:
```typescript
private onEvent(): void {
    this.pubSubService.on('App\\Notifications\\User\\VerifiedEmailNotification')
		.pipe(takeUntil(this.destroy$))
		.subscribe(() => this.notifyBlock$.next(true));
}
```

Подробнее про использование и настройку, можно прочитать тут: [Laravel Websockets](https://beyondco.de/docs/laravel-websockets/getting-started/introduction) 

----------

##Upload

1. Выполнить сборку Frontend
```
cd frontend && npm run build
```
Frontend будет собран в папку ./backend/public/assets/angular, а содержимое index.html скопировано в ./backend/resources/views/angular.blade.php
2. Загрузить содержимое директории ./backend/ на сервер, в директорию проекта, что указана как root в настройках web сервера 

#### Пример файла конфигурации автоматической сборки и загрузки, для Bitbucket. В корне проекта создайте файла ```bitbucket-pipelines.yml```
```text
image: node:14

pipelines:
    branches:
        develop:
            - step:
                caches:
                    - node
                    - composer
                script:
                    - mkdir -p backend/public/assets/angular
                    - cd frontend
                    - echo "$(ls -la)"
                    - npm i -D @angular/cli@latest @angular/compiler@latest @angular/compiler-cli@latest
                    - npm install -g json
                    - json -I -f package.json -e "this.version='1.0.$BITBUCKET_BUILD_NUMBER'"
                    - npm install
                    - npm run build
                    - echo "$(ls -la ../backend/public/assets/angular)"
                    - cp -r ../backend/public/assets/angular/assets/ ../backend/public/
                    - scp -r ../backend/* {user}@{ip-host}:~
                    - ssh {user}@{ip-host} "composer install --optimize-autoloader --no-dev && php artisan migrate --force && php artisan config:cache && php artisan route:cache && php artisan view:cache && php artisan queue:restart"

```

----------

#### Пример конфигурации Nginx
```text
server {
    listen *:443 ssl;

    server_name                 example.org;

    ssl_certificate             /etc/letsencrypt/live/example.org/fullchain.pem;
    ssl_certificate_key         /etc/letsencrypt/live/example.org/privkey.pem;
    ssl_trusted_certificate     /etc/letsencrypt/live/example.org/chain.pem;

    root                        /var/www/example/public;
    error_log                   /var/www/example/storage/logs/nginx-error.log;
    index                       index.php;
    rewrite_log                 off;
    access_log                  off;

    charset utf-8;

    location / {
        try_files /nonexistent @$type;
    }

    location @web  {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location @ws  {
        proxy_pass              http://127.0.0.1:6001;
        proxy_set_header Host   $host;
        proxy_read_timeout      60;
        proxy_connect_timeout   60;
        proxy_redirect          off;

        # Allow the use of websockets
        proxy_http_version      1.1;
        proxy_set_header        Upgrade $http_upgrade;
        proxy_set_header        Connection 'upgrade';
        proxy_set_header        Host $host;
        proxy_cache_bypass      $http_upgrade;
    }

    location ~ \.php$ {
        include                 fastcgi_params;
        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_pass            backend-example;
    }

    location ~* ^.+\.(jpg|jpeg|gif|css|png|js|ico|bmp)$ {
       access_log               off;
       expires                  10d;
       break;
    }

    location ~ /\.ht {
        deny all;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }

    error_page 404 /index.php;
}
```