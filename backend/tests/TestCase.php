<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function getResponseData($response, string $key)
    {
        return $response->decodeResponseJson()->json($key);
    }

    protected function makeUserToken(): string
    {
        $user = $this->getUser();
        return $user->createToken($user->email)->plainTextToken;
    }

    protected function getUser()
    {
        return User::factory()->create();
    }
}
