<?php

namespace Tests\Feature;

use Tests\TestCase;

class ServerRequiredTest extends TestCase
{
    public function testOpenssl()
    {
        $this->assertEquals(true, extension_loaded("openssl"));
    }

    public function testPdo()
    {
        $this->assertEquals(true,  defined('PDO::ATTR_DRIVER_NAME'));
    }

    public function testMbstring()
    {
        $this->assertEquals(true,  extension_loaded("mbstring"));
    }

    public function testTokenizer()
    {
        $this->assertEquals(true,  extension_loaded("tokenizer"));
    }

    public function testXml()
    {
        $this->assertEquals(true,  extension_loaded("xml"));
    }

    public function testCtype()
    {
        $this->assertEquals(true,  extension_loaded("ctype"));
    }

    public function testJson()
    {
        $this->assertEquals(true,  extension_loaded("json"));
    }

    public function testBcmath()
    {
        $this->assertEquals(true,  extension_loaded("bcmath"));
    }

    public function testImagick()
    {
        $this->assertEquals(true,  extension_loaded("imagick"));
    }

    public function testCurl()
    {
        $this->assertEquals(true,  extension_loaded("curl"));
    }

    public function testRedis()
    {
        $this->assertEquals(true,  extension_loaded("redis"));
    }

    public function testLibxml()
    {
        $this->assertEquals(true,  extension_loaded("libxml"));
    }

    public function testGhostscript()
    {
        $this->assertEquals(true, shell_exec("gs --version") >= 9);
    }

    public function testFfmpeg ()
    {
        $this->assertEquals(true, shell_exec('ffmpeg -version | sed -n "s/ffmpeg version \([-0-9.]*\).*/\1/p;"') >= 4);
    }

    public function testPhpConfig ()
    {
        $this->assertEquals(true, (int)ini_get('post_max_size') >= 200);
        $this->assertEquals(true, (int)ini_get('upload_max_filesize') >= 200);
    }
}
