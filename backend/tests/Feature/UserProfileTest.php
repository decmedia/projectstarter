<?php

namespace Tests\Feature;

use App\Constants\SettingsConst;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserProfileTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    // нет доступа без токена
    public function testCheckAccessWithoutTokenFailed()
    {
        $this
            ->json('POST', 'api/user/checkAccess', ['password' => 'password'])
            ->assertStatus(401);
    }

    // просмотр профиля пользователя
    public function testUserProfileViewSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('GET', 'api/user/view')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name',
                    'email',

                    'twa_enabled',
                    'sms_enabled',

                    'created_at',
                    'updated_at',

                    'has_verified_email',
                ]
            ]);
    }

    // проверка пароля на корректность
    // требуется в интерфейсе для предварительной проверки прав пользователя на изменение важной информации
    public function testCheckAccessByPasswordSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('POST', 'api/user/checkAccess', ['password' => 'password'])
            ->assertStatus(200);
    }

    // смена адреса эл. почты. Новый адрес уже существует у другого пользователя
    public function testChangeEmailAlreadyExists()
    {
        $user = $this->getUser();
        $user2 = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('PUT', 'api/user/profile', [
                'email' => $user2->email
            ])
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'email'
                    ]
                ]
            ])
        ;
    }

    // изменяем имя пользователя и его адерс электронной почты
    // при изменении адреса, на него отправляется письмо и для завершения процедуры
    // необходимо нажать на ссылку в нем. Проверяем факт нахождения нового адреса в таблице ожидания подтверждений
    public function testChangeNameAndEmailSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('PUT', 'api/user/profile', [
                'name' => 'User Name',
                'email' => 'user@example.com',
            ])
            ->assertStatus(200);

        $userChecked = User::findOrFail($user->id);

        $this->assertEquals($userChecked->name, 'User Name');
        $this->assertEquals($userChecked->changeEmail()->first()->email, 'user@example.com');
    }

    // меняем текущий пароль пользователя
    public function testChangeUserPasswordSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('PUT', 'api/user/password', [
                'old_password' => 'password',
                'new_password' => 'password',
            ])
            ->assertStatus(200);
    }

    // Two Factor Authenticator
    // активация двухфакторной аутентификации посредством приложения google authenticator
    public function testEnableTwoFaAuthenticatorSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('POST', 'api/user/two-factor/authenticator', [
                'code' => '111111',
                'secret' => '111111',
            ])
            ->assertStatus(200);
    }

    // генерация резервного кода доступа если телефон был утрачен
    public function testGenRecoveryCodeTwoFaAuthenticatorSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('POST', 'api/user/two-factor/recovery-code', [
                'password' => 'password'
            ])
            ->assertJsonStructure([
                'data'
            ])
            ->assertStatus(200);
    }

    // отключение двухфакторной аутентификации
    public function testDisableTwoFaAuthenticatorSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $user->forceFill(['two_factor_secret' => 'secret'])->save();

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('DELETE', 'api/user/two-factor/authenticator')
            ->assertStatus(200);
    }

    // изменяем настройки пользвоателя
    // получаем весь список и проверяем факт сохранеия изменений
    public function testUpdateUserPreferencesSuccessfully()
    {
        $changed_name = SettingsConst::NEWSLETTER_SUBSCRIBE()->getValue();

        $this->seed();

        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('PUT', 'api/user/preferences', [
                $changed_name => false
            ])
            ->assertStatus(200)
        ;

        $response = $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('GET', 'api/user/preferences')
            ->assertStatus(200)
        ;

        $newsletter_subscribe = $this->getResponseData($response, 'data.'.$changed_name);
        $this->assertNotTrue($newsletter_subscribe);
    }

    // повторная отпрака письма подтверждения адреса эл. почты
    public function testResendEmailVerifyRequestSuccessfully()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('POST', 'api/user/resend-email-verify')
            ->assertStatus(200)
        ;
    }

    // список выданных токенов (активных сессий) пользователя
    public function testAuthSessionsList()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('GET', 'api/user/sessions')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'last_used_at',
                        'location',
                        'ip',
                        'device',
                        'current',
                    ]
                ]
            ]);
    }

    // очистка всех сессий кроме текущей
    // (разлогиниться везде, кроме текущего устройства)
    public function testAuthSessionsClearOther()
    {
        $user = $this->getUser();
        $token = $user->createToken($user->email)->plainTextToken;
        $token = $user->createToken($user->email)->plainTextToken;

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('DELETE', 'api/user/sessions')
            ->assertStatus(200);

        $this->assertEquals(1, $user->tokens()->count());
    }
}
