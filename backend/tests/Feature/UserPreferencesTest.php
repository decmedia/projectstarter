<?php

namespace Tests\Feature;

use App\Constants\SettingsConst;
use App\Service\Settings\SettingsService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserPreferencesTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testCheckAccessWithoutTokenFailed()
    {
        $this
            ->json('GET', '/api/user/preferences')
            ->assertStatus(401);
    }

    public function testGetAllPreferencesSuccessfully()
    {
        $this->artisan('db:seed');
        $token = $this->makeUserToken();

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('GET', '/api/user/preferences')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => array_values(SettingsConst::toArray())
            ]);
    }

    public function testUpdatePreferencesSuccessfully()
    {
        $this->artisan('db:seed');
        $token = $this->makeUserToken();

        $this
            ->withHeader('Authorization', "Bearer $token")
            ->json('PUT', '/api/user/preferences', ['newsletter_subscribe' => false])
            ->assertStatus(200)
        ;

        $this->assertFalse(app(SettingsService::class)->get('newsletter_subscribe'));
    }
}
