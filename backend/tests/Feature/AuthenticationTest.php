<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    // попытка авторизации без обязательных полей
    public function testUserLoginCheckForRequiredFields()
    {
        $this->json('POST', 'api/auth')
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'email',
                        'password'
                    ]
                ]
            ]);
    }

    // успешная авторизация
    public function testUserLoginSuccessfully()
    {
        $user = $this->getUser();

        $this
            ->json('POST', 'api/auth', [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'user',
                ]
            ])
        ;
    }

    // авторизация требует ввода код двухфакторной аутентификации из приложения google authenticator
    public function testUserLoginTwoFaAuthenticatorCodeRequest()
    {
        $user = $this->getUser();
        $user->forceFill(['two_factor_secret' => 'secret'])->save();

        $response = $this
            ->json('POST', 'api/auth', [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertStatus(500);

        $code = $this->getResponseData($response, 'error.code');
        $this->assertEquals(2001, $code);
    }

    // авторизация с кодом из приложения google authenticator
    public function testUserLoginTwoFaAuthenticatorSuccessfully()
    {
        $user = $this->getUser();
        $user->forceFill(['two_factor_secret' => encrypt((app(TwoFactorAuthenticationProvider::class))->generateSecretKey())])->save();

        $this
            ->json('POST', 'api/auth', [
                'email' => $user->email,
                'password' => 'password',
                'code' => '111111',
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'user',
                ]
            ])
        ;
    }

    // REGISTER
    // попытка регистрации без наличия обязательных полей
    public function testUserRegisterCheckForRequiredFields()
    {
        $this->json('POST', 'api/auth/register')
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'name',
                        'email',
                        'password'
                    ]
                ]
            ]);
    }

    // успешная регистрация
    public function testUserRegisterSuccessfully()
    {
        $this
            ->json('POST', 'api/auth/register', [
                'name' => $this->faker->name(),
                'email' => $this->faker->unique()->safeEmail(),
                'password' => 'password',
                'not_subscribe' => false
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'token',
                    'user',
                ]
            ])
        ;
    }

    // REMEMBER PASSWORD
    // попытка восстановить доступ без обязательных полей
    public function testResetPasswordCheckForRequiredFieldsForTheRequestCreateRequest()
    {
        $this->json('POST', 'api/auth/rememberRequest')
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'contact'
                    ]
                ]
            ]);
    }

    // попытка проверить код подтверждения без обязательных полей
    public function testResetPasswordCheckForRequiredFieldsForTheRequestConfirmCode()
    {
        $this->json('POST', 'api/auth/rememberConfirm')
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'request_id',
                        'code',
                    ]
                ]
            ]);
    }

    // попытка создать новый пароль при восстановлении, без обязательных полей
    public function testResetPasswordCheckForRequiredFieldsForTheRequestSetNewPassword()
    {
        $this->json('POST', 'api/auth/rememberSetPassword')
            ->assertStatus(422)
            ->assertJsonStructure([
                'error' => [
                    'code',
                    'message',
                    'fields' => [
                        'request_id',
                        'code',
                        'new_password',
                    ]
                ]
            ]);
    }

    // успешное восстановление доступа
    // создаем запрос - создаем новый пароль указав код подтверждения что был отправлен на почту
    public function testResetPasswordSuccessfully()
    {
        $user = $this->getUser();

        $response = $this
            ->json('POST', 'api/auth/rememberRequest', [
                'contact' => $user->email
            ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data'
            ])
        ;

        $request_id = $this->getResponseData($response, 'data');

        $this
            ->json('POST', 'api/auth/rememberConfirm', [
                'request_id' => $request_id,
                'code' => '111111',
                'new_password' => 'password',
                'new_password_confirmation' => 'password',
            ])
            ->assertStatus(200)
        ;
    }
}
