<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\File;
use Tests\TestCase;

class LibInstalledTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testMedialibrary()
    {
        $user = $this->getUser();
        $user
            ->addMedia(storage_path('app/tests/slide.png'))
            ->preservingOriginal()
            ->toMediaCollection();

        $media = $user->getMedia();

        $this->assertCount(1, $media);
        $user->clearMediaCollection();
        File::deleteDirectory(storage_path('app/public/1'));
    }

    public function testMedialibraryPdfConverting()
    {
        $user = $this->getUser();
        $user
            ->addMedia(storage_path('app/tests/sample.pdf'))
            ->preservingOriginal()
            ->toMediaCollection();

        $this->assertFileExists($user->getFirstMedia()->getPath('thumb'));
        $user->clearMediaCollection();
        File::deleteDirectory(storage_path('app/public/1'));
    }

    public function testMedialibraryImageConverting()
    {
        $user = $this->getUser();
        $user
            ->addMedia(storage_path('app/tests/slide.png'))
            ->preservingOriginal()
            ->toMediaCollection();

        $this->assertFileExists($user->getFirstMedia()->getPath('thumb'));
        $user->clearMediaCollection();
        File::deleteDirectory(storage_path('app/public/1'));
    }

}
