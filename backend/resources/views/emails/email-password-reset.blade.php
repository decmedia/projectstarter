@component('mail::message')
# Здравствуйте

Код подтверждения что потребуется вам для изменения пароля
@component('mail::panel')
<div style="text-align: center; font-weight: bold; color: black; font-size: 1.5em">{{ $code }}</div>
@endcomponent

Если вы не восстанавливаете пароль и это письмо получено по ошибке, ничего предпринимать не требуется. Ваш аккаунт в безопасности

С уважением,<br>
{{ config('app.name') }}

@endcomponent

