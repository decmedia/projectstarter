@component('mail::message')
Привет!

Мы получили запрос на изменение вашего адреса электронной почты с <a href="mailto:{{$email}}">{{$email}}</a> на <a href="mailto:{{$new_email}}">{{$new_email}}</a>

@component('mail::button', ['url' => $url])
Подтвердить новый адрес
@endcomponent

Если вы не запрашивали это изменение, немедленно сообщите нам об этом, ответив на это письмо.

С уважением,<br>
{{ config('app.name') }}

<hr style="margin-top: 2rem; margin-bottom: 1rem">
<div style="font-size: 12px; color: #a0a0a0">
Если у вас возникли проблемы с нажатием кнопки "Подтвердите адрес электронной почты", скопируйте и вставьте приведенный ниже URL-адрес в свой веб-браузер:
<a href="{{$url}}">{{$url}}</a>
</div>
@endcomponent
