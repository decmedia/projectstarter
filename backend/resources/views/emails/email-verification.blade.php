@component('mail::message')
Подтвердите свой адрес электронной почты, чтобы мы знали, что это действительно вы,
и чтобы мы могли отправлять вам важную информацию о вашей учетной записи {{config('app.name')}}

@component('mail::button', ['url' => $url])
Подтвердите адрес электронной почты
@endcomponent

С уважением,<br>
{{ config('app.name') }}

<hr style="margin-top: 2rem; margin-bottom: 1rem">
<div style="font-size: 12px; color: #a0a0a0">
Если у вас возникли проблемы с нажатием кнопки "Подтвердите адрес электронной почты", скопируйте и вставьте приведенный ниже URL-адрес в свой веб-браузер:
<a href="{{$url}}">{{$url}}</a>
</div>
@endcomponent
