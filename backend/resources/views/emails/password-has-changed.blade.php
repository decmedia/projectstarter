@component('mail::message')
Ваш пароль {{ config('app.name') }} был успешно изменен.

Если вы не выполнили это действие, вам следует немедленно перейти на <a href="{{ config('app.url') }}/auth/remember">{{ config('app.url') }}/auth/remember</a>, чтобы сбросить пароль.

Чтобы просмотреть это и другие события, происходящие в вашей учетной записи {{ config('app.name') }}, вы можете посетить
<a href="{{ config('app.url') }}/profile/security_history">{{ config('app.url') }}/profile/security_history</a>. Если вы заметите какую-либо подозрительную активность в своей учетной записи, немедленно сообщите нам об этом, ответив на это письмо.

Вы можете найти ответы на большинство вопросов и связаться с нами на <a href="{{ config('app.url') }}/support">{{ config('app.url') }}/support</a>. Мы здесь, чтобы помочь вам на любом этапе вашего пути.

С уважением,<br>
{{ config('app.name') }}

@endcomponent

