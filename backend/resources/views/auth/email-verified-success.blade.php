<x-app-layout>

    <div class="w-full h-full flex flex-col justify-center items-center bg-rectangle py-24">
        <div class="rounded shadow-none md:shadow-xl bg-white w-full max-w-md p-8 mb-12">
            <div class="sm:flex sm:items-start">
                <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-green-100 sm:mx-0 sm:h-10 sm:w-10">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-green-600" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" ><polyline points="20 6 9 17 4 12"></polyline></svg>
                </div>
                <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <h3 class="text-lg leading-6 font-medium text-gray-900" id="dialog-1-title">
                        Адрес подтвержден
                    </h3>
                    <div class="mt-2">
                        <p class="text-sm text-gray-500">
                            Спасибо, ваш адрес электронной почты был подтвержден. Можете продолжить работу с системой
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
