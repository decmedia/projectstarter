<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="root">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- favicon -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="theme-color" content="#ffffff">

    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>


    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    @stack('styles')
    @stack('scripts')

    <script>
        window.__capturedErrors = [];
        window.onerror = function (message, url, line, column, error) {
            __capturedErrors.push(error);
        };
        window.onunhandledrejection = function (evt) {
            __capturedErrors.push(evt.reason);
        }
        new MutationObserver(e => {
            for (const d of e) if (d.addedNodes) for (const e of d.addedNodes) e instanceof HTMLLinkElement && void 0 !== e.dataset.jsLazyStyle && e.addEventListener("load", function () {
                this.media = "all"
            })
        }).observe(document.head, {childList: !0});

        (() => {
            function displayContentFoState(state) {
                document
                    .querySelectorAll(`template[data-mount-on-state="${state}"]`)
                    .forEach((template) => document
                        .querySelectorAll(template.dataset.mountTarget)
                        .forEach((target) => {
                            while (target.firstChild) target.removeChild(target.firstChild);
                            target.appendChild(template.content.cloneNode(true));
                        }));
            }

            displayContentFoState(
                document.cookie.match(/(^|;) *site-auth=[^;]+;/) ? 'logged-in' : 'logged-out'
            );
        })();

    </script>

</head>
<body style="-webkit-top-highlight-color: transparent" class="body has-scrollbar font-sans antialiased"  data-js-controller="Page" data-page-id="Home">

{{ $slot }}

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
