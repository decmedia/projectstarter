<?php

use App\Http\Controllers\AngularController;
use App\Http\Controllers\Auth\EmailVerifyChangeController;
use App\Http\Controllers\Auth\EmailVerifyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/web/verify-email/{id}/{hash}', EmailVerifyController::class)->name('email.verify');
Route::get('/web/verify-change-email/{id}/{hash}', EmailVerifyChangeController::class)->name('email.verify.change');
Route::view('/web/verified-email', 'auth.email-verified-success')->name('email.verified');
Route::any('/{any}', [AngularController::class, 'index'])->where('any', '^(?!api).*$');
