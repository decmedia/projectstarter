<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Media\MediaController;
use App\Http\Controllers\User\ActivityLogController;
use App\Http\Controllers\User\PreferencesController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\TwoFactorAuthenticationController;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Features;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/user/resend-email-verify', [ProfileController::class, 'resendEmailVerify']);

    Route::get('/user/restore-avatar', [ProfileController::class, 'restoreAvatar']);
    Route::post('/user/checkAccess', [ProfileController::class, 'checkAccess']);
    Route::get('/user/view', [ProfileController::class, 'view']);
    Route::put('/user/profile', [ProfileController::class, 'update']);
    Route::delete('/user/profile', [ProfileController::class, 'destroy']);
    Route::put('/user/password', [ProfileController::class, 'passwordUpdate']);
    Route::put('/user/pin_code', [ProfileController::class, 'pinUpdate']);

    Route::get('/user/sessions', [ProfileController::class, 'sessions']);
    Route::delete('/user/sessions', [ProfileController::class, 'closeOtherSessions']);

    Route::get('/user/security', [ActivityLogController::class, 'viewLog']);

    Route::get('/user/preferences', [PreferencesController::class, 'view']);
    Route::put('/user/preferences', [PreferencesController::class, 'update']);

    // Media files
    Route::post('/file/download', [MediaController::class, 'download']);
    Route::get('/file/{id}', [MediaController::class, 'show']);
    Route::delete('/file', [MediaController::class, 'pondDestroy']);
    Route::delete('/file/{id}', [MediaController::class, 'destroy']);
    Route::delete('/file/cancel-upload', [MediaController::class, 'cancelUpload']);
    Route::post('/file/upload', [MediaController::class, 'upload']);
    Route::get('/file/restore/{id}', [MediaController::class, 'restore']);
    Route::post('/file/{id}/rename', [MediaController::class, 'rename']);

    // Two-Factor Authentication...
    if (Features::enabled(Features::twoFactorAuthentication())) {
        Route::get('/user/two-factor/authenticator', [TwoFactorAuthenticationController::class, 'status']);
        Route::post('/user/two-factor/authenticator', [TwoFactorAuthenticationController::class, 'store']);
        Route::delete('/user/two-factor/authenticator', [TwoFactorAuthenticationController::class, 'destroy']);
        Route::post('/user/two-factor/recovery-code', [TwoFactorAuthenticationController::class, 'recovery']);
        Route::get('/user/two-factor-qr', [TwoFactorAuthenticationController::class, 'qr']);
    }
});

Route::prefix('auth')->group(function () {
    Route::post('', LoginController::class);
    Route::post('/register', RegisterController::class);
    Route::post('/rememberRequest', [ForgotPasswordController::class, 'request']);
    Route::post('/rememberConfirm', [ForgotPasswordController::class, 'confirm']);
    Route::post('/rememberSetPassword', [ForgotPasswordController::class, 'setPassword']);
});

