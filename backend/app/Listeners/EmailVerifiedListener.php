<?php

namespace App\Listeners;

use App\Notifications\User\VerifiedEmailNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;
use Illuminate\Auth\Events\Verified;

/**
 * Class MediaVideoConverterListener
 * @package App\Listeners
 */
class EmailVerifiedListener
{
    use InteractsWithQueue;
    use SerializesModels;

    protected $media;

    /**
     * @param Verified $event
     * @throws Throwable
     */
    public function handle(Verified $event)
    {
        $event->user->notify(new VerifiedEmailNotification());
        return;
    }

}
