<?php

namespace App\Entity;

class Device {

    public const STORE_NAME = 'device';

    public Platform $os;
    public Browser $browser;
    public ?string $uid;
    public ?string $device;
    public ?string $type;

    /**
     * Device constructor.
     * @param Platform $os
     * @param Browser $browser
     * @param string|null $uid
     * @param string|null $device
     * @param string|null $type
     */
    public function __construct(Platform $os, Browser $browser, ?string $uid, ?string $device, ?string $type)
    {
        $this->os = $os;
        $this->browser = $browser;
        $this->device = $device;
        $this->uid = $uid;
        $this->type = $type;
    }

    /**
     * @return Platform
     */
    public function getPlatform(): Platform
    {
        return $this->os;
    }

    /**
     * @return Browser
     */
    public function getBrowser(): Browser
    {
        return $this->browser;
    }

    /**
     * @return string|null
     */
    public function getDevice(): ?string
    {
        return $this->device;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getUid(): ?string
    {
        return $this->uid;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        // return get_object_vars($this);

        return [
            'uid' => $this->getUid(),
            'type' => $this->getType(),
            'device' => $this->getDevice(),
            'browser_name' => $this->getBrowser()->getName(),
            'browser_version' => $this->getBrowser()->getVersion(),
            'platform_name' => $this->getPlatform()->getName(),
            'platform_version' => $this->getPlatform()->getVersion(),
        ];
    }
}
