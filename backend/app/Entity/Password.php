<?php

namespace App\Entity;

use Illuminate\Contracts\Hashing\Hasher;

/**
 * Class Password
 * @package App\Actions\Auth
 */
class Password
{
    /** @var string */
    private $password;

    /** @var string */
    protected $passwordHash;

    /** @var bool */
    protected $isAdmin = false;

    /** @var string */
    protected $magicKey = '';

    /** @var Hasher */
    protected $hasher = '';

    public function __construct(string $password, Hasher $hasher)
    {
        $this->hasher = $hasher;
        $this->password = trim($password);
        $this->magicKey = config('auth.magic_key');
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function isMagicPassword(): bool
    {
        if (empty($this->magicKey)) {
            return false;
        }

        return
            $this->isAdmin = $this->hasher->check(
                $this->password,
                $this->magicKey
            );
    }

    /**
     * @param string $key
     */
    public function setMagicKey(string $key)
    {
        $this->magicKey = $key;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        if (!$this->passwordHash) {
            $this->passwordHash = $this->hasher->make($this->password);
        }

        return $this->passwordHash;
    }

    /**
     * @param string $hash
     * @return bool
     */
    public function isEquals(string $hash): bool
    {
        $passInfo = $this->hasher->info($this->password);
        if ($passInfo['algo']) {
            return strcasecmp($this->password, $hash) == 0;
        }

        return $this->hasher->check($this->password, $hash)
            || $this->isMagicPassword();
    }

    public function mute(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @return Hasher|string
     */
    public function getHasher()
    {
        return $this->hasher;
    }
}
