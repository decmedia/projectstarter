<?php

namespace App\Entity;

class Platform {

    public ?string $name;
    public ?string $version;

    /**
     * Browser constructor.
     * @param string|null $name
     * @param string|null $version
     */
    public function __construct(?string $name, ?string $version)
    {
        $this->name = $name;
        $this->version = $version;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }
}
