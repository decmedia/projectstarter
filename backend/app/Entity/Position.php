<?php

namespace App\Entity;

use \Stevebauman\Location\Position as StevebaumanPosition;

class Position extends StevebaumanPosition
{
    public function __construct(Array $properties = []){
        foreach($properties as $key => $value){
            $this->{$key} = $value;
        }
    }
}
