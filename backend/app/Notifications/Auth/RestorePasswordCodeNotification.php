<?php

namespace App\Notifications\Auth;

use App\Mail\PasswordResetCodeMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class RestorePasswordCodeNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /** @var int */
    protected $code;

    public function __construct(int $code)
    {
        $this->code = $code;
    }

    /**
     * Get the notification's channels.
     *
     * @return array|string
     */
    public function via()
    {
        return [
            'mail'
        ];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return PasswordResetCodeMail
     */
    public function toMail($notifiable)
    {
        return
            (new PasswordResetCodeMail($this->code))
                ->to($notifiable->email)
                ->withSwiftMessage(function ($message) use ($notifiable) {
                    $message->causer = $notifiable;
                })
            ;
    }

}
