<?php

namespace App\Notifications\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class ChangeEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public string $new_email;
    public static $createUrlCallback;

    public function __construct(string $new_email)
    {
        $this->new_email = $new_email;
    }

    /**
     * Get the notification's channels.
     *
     * @return array|string
     */
    public function via()
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->withSwiftMessage(function ($message) use ($notifiable) {
                $message->causer = $notifiable;
            })
            ->markdown('emails.email-change-confirmation', [
                'url' => $this->verificationUrl($notifiable),
                'email' => $notifiable->email,
                'new_email' => $this->new_email,
            ])
            ->subject('Подтвердите свой новый адрес электронной почты ' . config('app.name'));
    }

    protected function verificationUrl($notifiable)
    {
        if (static::$createUrlCallback) {
            return call_user_func(static::$createUrlCallback, $notifiable);
        }

        return URL::temporarySignedRoute(
            'email.verify.change',
            Carbon::now()->addMinutes(Config::get('auth.verification.expire', 60)),
            [
                'id' => $notifiable->id,
                'hash' => sha1($this->new_email),
            ]
        );
    }
}
