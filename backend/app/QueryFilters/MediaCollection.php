<?php

namespace App\QueryFilters;

class MediaCollection extends Filter
{
    protected function applyFilters($builder)
    {
        return $builder->where('collection_name', $this->request->get($this->filterName()));
    }
}
