<?php

namespace App\QueryFilters;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

abstract class Filter
{

    /**
     * The request.
     *
     * @var Request
     */
    protected $request;

    /**
     * Load the request via dependency injection into the filter.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle($query, Closure $next)
    {
        if(!$this->request->has($this->filterName())) {
            return $next($query);
        }

        $builder = $next($query);

        return $this->applyFilters($builder);
    }

    protected abstract function applyFilters($builder);

    protected function filterName()
    {
        return Str::snake(class_basename($this));
    }
}
