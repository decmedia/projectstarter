<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LocationCache
 * @package App\Models
 */
class LocationCache extends Model
{
    protected $table = 'location_cache';
    protected $fillable = ['ip', 'iso_code', 'zip_code', 'area_code', 'city_name', 'latitude', 'longitude', 'postal_code', 'region_code', 'region_name', 'country_code', 'country_name'];
}
