<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ChangeEmail
 * @package App\Models
 */
class ChangeEmail extends Model
{
    protected $table = 'change_email';
    protected $fillable = ['user_id', 'email'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
