<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DeviceInfo
 * @package App\Models
 */
class DeviceInfo extends Model
{
    protected $table = 'device_info';
    protected $fillable = ['uid', 'type', 'device', 'browser_name', 'browser_version', 'platform_name', 'platform_version'];
}
