<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserPasswordReset
 * @package App\Models
 */
class UserPasswordReset extends Model
{
    protected $table = 'user_password_reset';
    protected $fillable = ['user_id', 'contact', 'code', 'updated_at'];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
