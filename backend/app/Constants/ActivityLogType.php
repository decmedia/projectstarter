<?php
declare(strict_types=1);

namespace App\Constants;

use MyCLabs\Enum\Enum;

/**
 * Class ActivityLogType
 * @package Cs\App\Constants
 *
 * @method static ActivityLogType SECURITY()
 * @method static ActivityLogType AUTH()
 */
class ActivityLogType extends Enum
{
    const SECURITY = 'security';
    const AUTH = 'auth';
}
