<?php
declare(strict_types=1);

namespace App\Constants;

class Relationships
{
    const LOCATION          = 'location';
    const DEVICE            = 'device';
    const MEDIA_ADDITIONAL  = 'media_additional';
    const MEDIA             = 'media';
}
