<?php
declare(strict_types=1);

namespace App\Constants;

use MyCLabs\Enum\Enum;

/**
 * Class Settings
 * @package Cs\App\Constants
 *
 * @method static SettingsConst NEWSLETTER_SUBSCRIBE()
 */
class SettingsConst extends Enum
{
    const NEWSLETTER_SUBSCRIBE = 'newsletter_subscribe';
}
