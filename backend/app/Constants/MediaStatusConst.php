<?php
declare(strict_types=1);

namespace App\Constants;

use MyCLabs\Enum\Enum;

/**
 * Class MediaStatusConst
 * @package Cs\App\Constants
 *
 * @method static MediaStatusConst MEDIA_STATUS_READY()
 * @method static MediaStatusConst MEDIA_STATUS_PROCESSING()
 * @method static MediaStatusConst MEDIA_STATUS_FAILED()
 */
class MediaStatusConst extends Enum
{
    const MEDIA_STATUS_READY = 'ready';
    const MEDIA_STATUS_PROCESSING = 'processing';
    const MEDIA_STATUS_FAILED = 'failed';
}
