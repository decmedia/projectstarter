<?php

namespace App\Trait;

use Illuminate\Support\Facades\Event;

trait EventTrait
{
    private $events = [];

    /**
     * @param mixed $event
     */
    public function registerEvent($event)
    {
        $this->events[] = $event;
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function releaseEvents()
    {
        foreach ($this->events as $event) {
            Event::dispatch($event);
        }

        $this->events = [];
    }
}
