<?php

namespace App\Http\Responses\Decorators;

use Flugg\Responder\Http\Responses\Decorators\ResponseDecorator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;

/**
 * Class EtagDecorator
 * @package App\Http\Responses\Decorators
 */
class MetaDecorator extends ResponseDecorator
{
    /**
     * Generate a JSON response.
     *
     * @param  array $data
     * @param  int   $status
     * @param  array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    public function make(array $data, int $status, array $headers = []): JsonResponse
    {
        $timestamp = date('c');
        $hash      = sha1($timestamp . json_encode($data));
        $eTag      = sha1($content ?? '');

        $jsonapi = [
            'jsonapi' => [
                'version' => '1.0',
            ],
        ];
        $meta = [
            'meta' => [
                'timestamp' => $timestamp,
                'hash'      => $hash,
            ]
        ];

        // Responder::success(... не примет Primitive объект, поэтому заворачиваем в массив с ключом data
        $data = Arr::has($data, 'data.data') ? $data['data'] : $data;

        return $this->factory->make(
            array_merge($jsonapi, $data, $meta),
            $status,
            array_merge($headers, ['E-Tag' => $eTag])
        );
    }
}
