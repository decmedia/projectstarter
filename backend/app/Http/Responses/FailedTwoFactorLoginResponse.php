<?php

namespace App\Http\Responses;

use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\FailedTwoFactorLoginResponse as FailedTwoFactorLoginResponseContract;

class FailedTwoFactorLoginResponse implements FailedTwoFactorLoginResponseContract
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws ValidationException
     */
    public function toResponse($request)
    {
        $field_name = !empty($request->get('recovery_code')) ? 'recovery_code' : 'code';

        $message = __('The provided two factor authentication code was invalid.');

        if ($request->wantsJson()) {
            throw ValidationException::withMessages([
                $field_name => [$message],
            ]);
        }

        return redirect()->route('login')->withErrors(['email' => $message]);
    }
}
