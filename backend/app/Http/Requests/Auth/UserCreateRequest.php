<?php

namespace App\Http\Requests\Auth;

use App\Entity\Password;
use App\Http\Requests\Request;
use App\Models\User;
use Illuminate\Validation\Rule;

class UserCreateRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //TODO: удалять все что после + в адресе, для исключения регистрации с почтами вида: qwert2001qwert+test@gmail.com
        return [
            'parent_id' => ['nullable', 'string'],
            'name' => ['required', 'string', 'max:255'],
            'email'     => [
                'bail',
                'required',
                'email:rfc',
                Rule::unique(User::class, 'email')
                    ->using(fn ($q) => $q->whereNotNull('email_verified_at'))
            ],
            'password' => ['required', 'string', 'min:6'],
            'not_subscribe' => ['boolean'],
        ];
    }

    public function getPassword(): Password
    {
        return app(Password::class, ['password' => $this->password]);
    }
}
