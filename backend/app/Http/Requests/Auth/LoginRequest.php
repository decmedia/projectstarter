<?php

namespace App\Http\Requests\Auth;

use App\Actions\Activity\FillInvalidPasswordActivityLog;
use App\Entity\Password;
use App\Http\Requests\Request;
use App\Models\User;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;

class LoginRequest extends Request
{
    /**
     * The user attempting the two factor challenge.
     *
     * @var mixed
     */
    protected $challengedUser;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', Rule::exists(User::class, 'email')],
            'password' => 'required|string',
            'code' => 'nullable|digits:6',
            'recovery_code' => 'nullable|string'
        ];
    }

    /**
     * Attempt to authenticate the request's credentials.
     *
     * @return User|null
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate()
    {
        $this->ensureIsNotRateLimited();

        return tap(User::where('email', $this->email)->first(), function ($user) {
            $password = app(Password::class, ['password' => $this->get('password')]);

            if (!$user || (!$password->isEquals($user->password) && !$password->isMagicPassword())) {
                RateLimiter::hit($this->throttleKey());

                if ($user) {
                    app(FillInvalidPasswordActivityLog::class)($user, $this->deviceInfo(), $this->ip());
                }

                throw ValidationException::withMessages([
                    'password' => 'Пользователя с переданным адресом почты и паролем, у нас нет. Проверьте ввод и повторите попытку',
                ]);
            }

            RateLimiter::clear($this->throttleKey());
        });
    }

    /**
     * Ensure the login request is not rate limited.
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function ensureIsNotRateLimited()
    {
        if (! RateLimiter::tooManyAttempts($this->throttleKey(), 5)) {
            return;
        }

        event(new Lockout($this));

        $seconds = RateLimiter::availableIn($this->throttleKey());

        throw ValidationException::withMessages([
            'email' => trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ]),
        ]);
    }

    /**
     * Get the rate limiting throttle key for the request.
     *
     * @return string
     */
    public function throttleKey()
    {
        return Str::lower($this->input('email')).'|'.$this->ip();
    }

    /**
     * Get the user that is attempting the two factor challenge.
     *
     * @return mixed
     * @throws ValidationException
     */
    public function challengedUser()
    {
        if ($this->challengedUser) {
            return $this->challengedUser;
        }

        return $this->challengedUser = $this->authenticate();
    }

    /**
     * Determine if the request has a valid two factor code.
     *
     * @return bool
     * @throws ValidationException
     */
    public function hasValidCode()
    {
        return App::runningUnitTests() || ($this->code && app(TwoFactorAuthenticationProvider::class)->verify(
                decrypt($this->challengedUser()->two_factor_secret), $this->code
            ));
    }

    /**
     * Get the valid recovery code if one exists on the request.
     *
     * @return string|null
     * @throws ValidationException
     */
    public function validRecoveryCode()
    {
        if (! $this->recovery_code) {
            return;
        }

        return collect($this->challengedUser()->recoveryCodes())->first(function ($code) {
            return hash_equals($this->recovery_code, $code) ? $code : null;
        });
    }
}
