<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class EmailChangeConfirmRequest extends FormRequest
{
    public $request_id;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->setUserResolver(fn() => User::findOrFail($this->route('id')));
        $listChanges = $this->user()->changeEmail()->get();

        foreach ($listChanges as $change) {
            if (hash_equals((string) $this->route('hash'), sha1($change->email))) {
                $this->request_id = $change->id;
                return true;
            }
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
