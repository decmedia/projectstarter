<?php

namespace App\Http\Requests;

use App\Entity\Browser;
use App\Entity\Device;
use App\Entity\Platform;
use Illuminate\Foundation\Http\FormRequest;
use Jenssegers\Agent\Agent;

abstract class Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract function rules();

    /**
     * @return Device
     */
    public function deviceInfo(): Device
    {
        $agent = new Agent();

        $agent->setUserAgent($this->userAgent());
        // $agent->setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36');

        $browser = $agent->browser();
        $platform = $agent->platform();

        return new Device(
            new Platform($platform, $agent->version($platform)),
            new Browser($browser, $agent->version($browser)),
            $this->cookie(Device::STORE_NAME, $this->header("X-".Device::STORE_NAME)),
            $agent->device(),
            $agent->deviceType()
        );
    }
}
