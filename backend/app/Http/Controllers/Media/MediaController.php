<?php

namespace App\Http\Controllers\Media;

use App\Constants\MediaStatusConst;
use App\Constants\Relationships;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Transformers\MediaTransformer;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class MediaController extends Controller
{
    /**
     * @param int $id
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function rename(int $id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);

        $media = $request->user()->media()->findOrFail($id);
        $media->name = $request->get('name');
        $media->save();

        return $this->responder->success();
    }

    /**
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function download(Request $request)
    {
        $request->validate([
            'id' => 'required|array'
        ]);

        return $this->responder->success([
            'data' => URL::signedRoute(
                'media.download',
                [
                    'id' => $request->user()->id,
                    'media' => $request->get('id'),
                ]
            )
        ]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function show(int $id, Request $request)
    {
        $media = $request->user()->media()
            ->where('collection_name', 'default')
            ->where('id', $id)
            ->first()
        ;

        return $this->responder->success($media, MediaTransformer::class)
            ->with(Relationships::MEDIA_ADDITIONAL)
        ;
    }

    /**
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\ErrorResponseBuilder|\Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function upload(Request $request)
    {
        $request->validate([
            'filepond' => 'required|mimes:jpg,bmp,png,gif,pdf,mp4,ogv,webm,mov,avi,mp3|max:2000000',
            'mediaCollection' => 'nullable|string'
        ]);

        if ($request->hasFile('filepond')) {
            $mediaCollection = $request->get('mediaCollection', 'default');

            $media = $request->user()
                ->addMediaFromRequest('filepond')
                ->toMediaCollection($mediaCollection)
                ->setCustomProperty('status', MediaStatusConst::MEDIA_STATUS_PROCESSING())
                ;

            return $this->responder->success($media, MediaTransformer::class);
        }

        return $this->responder->error();
    }

    /**
     * Восстановление файла в форме загрузки по его номеру
     * @url https://pqina.nl/filepond/docs/patterns/api/server/#introduction
     *
     * @param int $primaryMediaId
     * @param User $user
     * @return mixed
     */
    public function restore(int $primaryMediaId, User $user)
    {
        $media = $user->media()->where('id', $primaryMediaId)->firstOrFail();
        $file = File::get($media->getPath('thumb'));

        return Response::make($file, 200, [
            'Content-Type' => $media->mime_type,
            'Content-Disposition' => 'inline; filename="'.$media->id.'"',
        ]);
    }

    /**
     * Удаление файла по его номеру или массиву номеров через запятую
     *
     * @param int | string $id
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function destroy($id, Request $request)
    {
        $ids = Str::contains($id, ',') ? explode(',', $id) : [$id];

        if (count($ids) > 0) {
            $request
                ->user()
                ->media()
                ->whereIn('id', $ids)
                ->get()
                ->each(fn($item) => $item->delete())
            ;
        }

        return $this->responder->success();
    }

    /**
     * Удаление файла из формы загрузки (средствами Filepond)
     * Подробнее: https://pqina.nl/filepond/docs/api/server/#remove
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function pondDestroy(Request $request)
    {
        if (is_numeric($request->getContent())) {
            $media = $request->user()->media()->where('id', $request->getContent())->first();
            $media?->delete();
        }

        return $this->responder->success();
    }

    /**
     * Перемещение файлов в папку с переданным номером
     *
     * @param int | string $id relocatable file numbers (id or id,id,id...)
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function move($id, Request $request)
    {
        $validatedData = $request->validate([
            'folder_id' => ['required', 'int'],
        ]);

        if ($validatedData['folder_id'] !== 0) {
            $request->user()->mediaFolder()->findOrFail($validatedData['folder_id']);
        }

        $ids = Str::contains($id, ',') ? explode(',', $id) : [$id];

        if (count($ids) > 0) {
            $request
                ->user()
                ->media()
                ->whereIn('id', $ids)
                ->get()
                ->each(fn($item) => $item->update([
                    'folder_id' => $validatedData['folder_id'] ?: null
                ]))
            ;
        }

        return $this->responder->success();
    }

    /**
     * Удаление файла из формы Filepond
     * Номер передается тектом в теле запроса
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function cancelUpload(Request $request)
    {
        if (is_numeric($request->getContent())) {
            //TODO
            print_r($request->getContent());
            die;
        }

        return $this->responder->success();
    }
}
