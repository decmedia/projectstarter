<?php

namespace App\Http\Controllers\Media;

use App\Http\Controllers\Controller;
use App\Http\Requests\Media\DownloadVerificationRequest;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\Support\MediaStream;

class DownloadMediaController extends Controller
{
    /**
     * Загрузка файлов по переданным идентификаторам
     * media[] - если передан один файл, он будет отдан как есть.
     * Если номеров файлов будет несколько, будет создан архив zip с файлами в нем, что были указаны
     *
     * @param DownloadVerificationRequest $request
     * @return MediaStream|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    function index(DownloadVerificationRequest $request)
    {
        $media = $request->user()
            ->media()
            ->whereIn('id', $request->get('media'));
        ;

        if (count($request->get('media')) > 1) {
            $archive_name = Str::slug(config('app.name').' '.now()).'.zip';

            return MediaStream::create($archive_name)->addMedia($media->get());
        } else {
            return response()->download($media->first()->getPath(), $media->first()->file_name);
        }
    }
}
