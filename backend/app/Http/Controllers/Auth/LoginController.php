<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Activity\FillAuthActivityLog;
use App\Exceptions\RuntimeException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Transformers\UserTransformer;
use Laravel\Fortify\Contracts\FailedTwoFactorLoginResponse;

class LoginController extends Controller
{
    /**
     * @param LoginRequest $request
     * @param FillAuthActivityLog $log
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     * @throws RuntimeException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function __invoke(LoginRequest $request, FillAuthActivityLog $log)
    {
        $user = $request->challengedUser();

        if ($user->two_factor_secret) {
            if (!$request->get('code') && !$request->get('recovery_code')) {

                // TODO: Убрать хранение из таблицы пользователя. Реализовать остальные
                // 2001 => Two-step authentication [app]
                // 2002 => Two-step authentication [sms]
                // 2003 => Two-step authentication [key]
                throw new RuntimeException('Two-step authentication', 2001);
            }

            if ($code = $request->validRecoveryCode()) {
                // TODO: Коды одноразовые. Удалять использованный и предлагать создать новый после входа
                // $user->replaceRecoveryCode($code);
            } elseif (! $request->hasValidCode()) {
                return app(FailedTwoFactorLoginResponse::class);
            }
        }

        $token = $user->createToken('dashboard', ['*'], $request->deviceInfo(), $request->ip());

        $log($token->accessToken, $request->deviceInfo(), $request->ip());

        return $this->responder
            ->success([
                'token' => $token->plainTextToken,
                'user' => (new UserTransformer)->transform($user)
            ]);
    }
}
