<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserCreateRequest;
use App\Models\User;
use App\Transformers\UserTransformer;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    public function __invoke(UserCreateRequest $input)
    {
        $user = $this->connection->transaction(function () use ($input) {
            return tap(User::create([
                'name' => $input->get('name'),
                'email' => $input->get('email'),
                'password' => $input->getPassword()->getHash()

            ]), function (User $user) use ($input) {

                // TODO создавать записи в нужных таблицах
                // $this->createAccount($user);

                $this->dispatcher->dispatch(new Registered($user));
            });
        });

        return $this->responder->success([
            'token' => $user->createToken('dashboard', ['*'], $input->deviceInfo(), $input->ip())->plainTextToken,
            'user' => (new UserTransformer)->transform($user)
        ]);
    }
}
