<?php

namespace App\Http\Controllers\Auth;

use App\Actions\User\ChangePasswordAction;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserPasswordReset;
use App\Notifications\Auth\PasswordHasChangedNotification;
use App\Notifications\Auth\RestorePasswordCodeNotification;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Jenssegers\Agent\Agent;

class ForgotPasswordController extends Controller
{
    /**
     * Восстановление пароля. Шаг 1
     * Принимаем контакт пользователя, ищем среди подтвержденных и отправляем туда код
     * POST
     * - contact
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function request(Request $request)
    {
        $request->validate([
            'contact' => ['required', Rule::exists(User::class, 'email')]
        ]);

        /** @var User $contact */
        $user = User::where('email', $request->get('contact'))
            ->whereNotNull('email_verified_at')
            ->firstOrFail();

        $resetToken = UserPasswordReset::firstOrCreate(
            ['contact' => $user->email],
            ['code' => $this->genSecretCode(), 'user_id' => $user->id]
        );

        if (
            $resetToken->updated_at->addMinutes(2)->lte(now())
            || $resetToken->updated_at->eq($resetToken->created_at)
        ) {
            $resetToken->update(['updated_at' => now()]);
            $user->notify(new RestorePasswordCodeNotification($resetToken->code));
        }

        return $this->responder->success(['data' => $resetToken->id]);
    }

    /**
     * Восстановление пароля. Шаг 2
     * Сверяем переданный код с отправленным на контакт
     * POST
     * - request_id
     * - code
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     * @throws ValidationException
     */
    public function confirm(Request $request)
    {
        $request->validate([
            'request_id' => ['required', 'integer'],
            'code' => ['required', 'string', 'min:6']
        ]);

        $passwordReset = UserPasswordReset::where('id', $request->request_id)->firstOrFail();
        if ($passwordReset->code !== $request->get('code')) {
            throw ValidationException::withMessages(['code' => 'Код указан некорректно']);
        }

        return $this->responder->success();
    }

    public function setPassword(Request $request, ChangePasswordAction $changePasswordAction)
    {
        $request->validate([
            'request_id' => ['required', 'integer'],
            'code' => ['required', 'string', 'min:6'],
            'new_password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $passwordReset = UserPasswordReset::where('id', $request->request_id)->firstOrFail();
        if ($passwordReset->code !== $request->get('code')) {
            throw ValidationException::withMessages(['code' => 'Код указан некорректно']);
        }

        /** @var User $user */
        $user = $passwordReset->user()->firstOrFail();

        $passwordReset->delete();

        $changePasswordAction($user, $request->get('new_password'));

        activity('security')
            ->by($user)
            ->performedOn($user)
            ->withProperties([
                'ip' => $request->ip()
            ])
            ->log('security.restore_password')
        ;

        return $this->responder->success(['data' => $user->login]);
    }

    /**
     * @return int
     */
    private function genSecretCode(): int
    {
        return once(function () {
            return App::runningUnitTests() ? 111111 : rand(111111, 999999);
        });
    }
}
