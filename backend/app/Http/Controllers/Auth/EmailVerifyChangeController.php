<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\EmailChangeConfirmRequest;
use App\Models\ChangeEmail;
use Illuminate\Auth\Events\Verified;

class EmailVerifyChangeController extends Controller
{
    /**
     * Подтверждение изменения адреса почты
     *
     * @param  \App\Http\Requests\Auth\EmailChangeConfirmRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(EmailChangeConfirmRequest $request)
    {
        $changed = ChangeEmail::findOrFail($request->request_id);

        $request->user()->update(['email' => $changed->email]);

        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        $request->user()->changeEmail()->delete();

        return redirect()->route('email.verified');
    }
}
