<?php

namespace App\Http\Controllers\User;

use App\Actions\Auth\EnableTwoFactorAuthentication;
use App\Actions\Auth\GenBackupCodesTwoFactorAuthentication;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\TwoFactorLoginRequest;
use App\Transformers\User2faTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Actions\DisableTwoFactorAuthentication;
use Laravel\Fortify\Contracts\FailedTwoFactorLoginResponse;
use Laravel\Fortify\Contracts\TwoFactorAuthenticationProvider;

class TwoFactorAuthenticationController extends Controller
{
    /**
     * @param TwoFactorLoginRequest $request
     * @param EnableTwoFactorAuthentication $enable
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder|\Illuminate\Contracts\Foundation\Application|mixed
     * @throws ValidationException
     */
    public function store(TwoFactorLoginRequest $request, EnableTwoFactorAuthentication $enable)
    {
        if ($request->user()->two_factor_secret
            && !$request->get('password')
        ) {
            throw ValidationException::withMessages([
                'password' => 'The current password field cannot be empty'
            ]);
        }

        if (! $request->hasValidCodeBySecret() ) {
            return app(FailedTwoFactorLoginResponse::class);
        }

        $enable($request->user(), $request->secret);

        return $this->responder->success();
    }

    /**
     * Статус двухэтапной аутентификации
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function status()
    {
        return $this->responder->success(
            Auth::user(), User2faTransformer::class
        );
    }

    /**
     * Disable two factor authentication for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Laravel\Fortify\Actions\DisableTwoFactorAuthentication  $disable
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function destroy(Request $request, DisableTwoFactorAuthentication $disable)
    {
        $disable($request->user());

        return $this->responder->success();
    }

    /**
     * Создание новых резервных кодов доступа
     *
     * @param Request $request
     * @param GenBackupCodesTwoFactorAuthentication $generate
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function recovery(Request $request, GenBackupCodesTwoFactorAuthentication $generate)
    {
        $request->validate(['password' => 'password']);

        $generate($request->user());

        $codes = json_decode(decrypt(
            $request->user()->two_factor_recovery_codes
        ), true);

        return $this->responder->success($codes);
    }

    /**
     * Генерация QR кода без сохранения его в профиль пользователя
     * Показываем - настраивает - сохраняем если код введет правильно (настрйока удалась)
     *
     * @param \Illuminate\Http\Request $request
     * @param TwoFactorAuthenticationProvider $provider
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function qr(Request $request, TwoFactorAuthenticationProvider $provider)
    {
        $newKey = $provider->generateSecretKey();
        $request->user()->two_factor_secret = encrypt($newKey);

        return $this->responder->success([
            'svg' => $request->user()->twoFactorQrCodeSvg(),
            'code' => $newKey
        ]);
    }
}
