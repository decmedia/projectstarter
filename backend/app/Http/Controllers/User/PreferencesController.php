<?php

namespace App\Http\Controllers\User;

use App\Service\Settings\SettingsService;
use App\Constants\SettingsConst;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class PreferencesController
 * @package App\Http\Controllers\User
 */
class PreferencesController extends Controller
{
    /**
     * В системе есть настройки (key => value), со значениями по умолчанию.
     * Настройки пользователя добавленные с таким же именем ключа, имеют большие приоритет и перетирают системную.
     * Не все что лежат в базе доступны к выводу и изменению пользователем.
     * Перечень доступных к изменению (а также выводу на фронт), определяется в App\Constants\SettingsConst
     * Хранятся в x_user_settings
     *
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function view(Request $request, SettingsService $settingsService)
    {
        return $this->responder->success(
            $settingsService
                ->by($request->user())
                ->all()
        );
    }

    /**
     * В системе есть настройки (key => value), со значениями по умолчанию.
     * Настройки пользователя добавленные с таким же именем ключа, имеют большие приоритет и перетирают системную.
     * Не все что лежат в базе доступны к выводу и изменению пользователем.
     * Перечень доступных к изменению (а также выводу на фронт), определяется в App\Constants\SettingsConst
     * Хранятся в x_user_settings
     *
     * @param Request $request
     * @param SettingsService $settingsService
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function update(Request $request, SettingsService $settingsService)
    {
        $rulesCollection = collect(SettingsConst::toArray())->mapWithKeys(function ($item) {
            return [$item => ['nullable', 'bool']];
        });

        $validatedData = $request->validate($rulesCollection->all());

        collect($validatedData)->each(
            fn($value, $key) => $settingsService
                ->by($request->user())
                ->set($key, $value)
        );

        return $this->responder->success();
    }
}
