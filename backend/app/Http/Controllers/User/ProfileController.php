<?php

namespace App\Http\Controllers\User;

use App\Actions\Activity\FillChangePasswordActivityLog;
use App\Actions\User\ChangeEmailAction;
use App\Actions\User\ChangePasswordAction;
use App\Constants\Relationships;
use App\Http\Controllers\Controller;
use App\Http\Requests\BaseRequest;
use App\Models\PersonalAccessToken;
use App\Models\User;
use App\Trait\EventTrait;
use App\Transformers\SessionTransformer;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    use EventTrait;

    /**
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function view()
    {
        return $this->responder->success(Auth::user(), UserTransformer::class);
    }

    /**
     * POST
     * Сравнение переданного пароля с паролем авторизованного в данный момент пользователя
     * Используется для подтверждения сесси пользователя в интерфейсе сайта
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function checkAccess(Request $request)
    {
        $request->validate([
            'password' => ['required', 'password', 'max:255']
        ], [
            'password.password' => 'Неверный пароль',
        ]);

        return $this->responder->success(['data' => true]);
    }

    /**
     * PUT
     * Обновление профиля пользователя:
     * { "email": string, "name": string }
     * Если поле email !== текущему адресу, на него будет отправлено подтверждение
     * Нажав на ссылку в письме, этот адрес заменит текущий в профиле пользователя
     * и сразу будет отмечен подтвержденным
     *
     * @param Request $request
     * @param ChangeEmailAction $action
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     * @throws \App\Exceptions\RuntimeException
     */
    public function update(Request $request, ChangeEmailAction $action)
    {
        $validatedData = $request->validate([
            'email' => ['nullable', 'bail', 'required', 'max:255', 'email:rfc', Rule::unique(User::class)->ignore($request->user()->id)],
            'name' => ['nullable', 'string', 'max:255'],
        ]);

        if (isset($validatedData['email']) && $request->user()->email !== $validatedData['email']) {
            $action($request->user(), $validatedData['email']);
        }

        if (isset($validatedData['name'])) {
            $request->user()->update(['name' => $validatedData['name']]);
        }

        return $this->responder->success();
    }

    /**
     * @param BaseRequest $request
     * @param ChangePasswordAction $action
     * @param FillChangePasswordActivityLog $activityLog
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function passwordUpdate(BaseRequest $request, ChangePasswordAction $action, FillChangePasswordActivityLog $activityLog)
    {
        $request->validate([
            'old_password' => ['required', 'password'],
            'new_password' => ['string', 'min:6', 'max:255'],
        ]);

        $action($request->user(), $request->get('new_password'));
        $activityLog($request->user(), $request->deviceInfo(), $request->ip());

        return $this->responder->success();
    }

    /**
     * Повторная отправка письма подтверждения адреса эл.почты
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function resendEmailVerify(Request $request)
    {
        if (!$request->user()->hasVerifiedEmail()) {
            $request->user()->sendEmailVerificationNotification();
        }

        return $this->responder->success();
    }

    /**
     * Список выданных токенов с информацией из лога (ip, устройство, локация)
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function sessions(Request $request)
    {
        $current_token = PersonalAccessToken::findToken($request->bearerToken());
        $tokens = $request->user()
            ->tokens()
            ->leftJoin('device_info', 'personal_access_tokens.properties->uid', '=', 'device_info.uid')
            ->leftJoin('location_cache', 'personal_access_tokens.properties->ip', '=', 'location_cache.ip')
            ->select(
                'personal_access_tokens.id as id', 'last_used_at', 'properties',
                'device_info.uid', 'device_info.type', 'device_info.device', 'device_info.browser_name', 'device_info.browser_version', 'device_info.platform_name', 'device_info.platform_version',
                'location_cache.ip', 'location_cache.city_name', 'location_cache.latitude', 'location_cache.longitude', 'location_cache.country_code', 'location_cache.country_name',
            )
            ->get()
            ->map(function ($item) use ($current_token) {
                $item->current = $item->id === $current_token->id;
                return $item;
            })
        ;

        return $this->responder
            ->success($tokens, SessionTransformer::class)
            ->with(Relationships::LOCATION)
            ->with(Relationships::DEVICE)
        ;
    }

    public function closeOtherSessions(Request $request)
    {
        $current_token = PersonalAccessToken::findToken($request->bearerToken());
        $tokens = $request
            ->user()
            ->tokens()
            ->where('id', '!=', $current_token->id)
            ->get()
        ;

        foreach ($tokens as $token) {
            $token->delete();
        }

        return $this->responder->success();
    }

    /**
     * Возвращает файл последнего загруженного пользователем, аватара.
     * Используется в профиле, для восстановления загруженного ранее изображения, в форме загрузки файлов
     * Подробнее: https://pqina.nl/filepond/docs/api/server/#restore
     *
     * @param Request $request
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder|\Illuminate\Http\Response
     */
    public function restoreAvatar(Request $request)
    {
        $avatars = $request->user()->getMedia('avatar');
        return $this->responder->success(['data' => $avatars->isNotEmpty() ? $avatars->last()->id : 0]);
    }
}
