<?php

namespace App\Http\Controllers\User;

use App\Constants\Relationships;
use App\Http\Controllers\Controller;
use App\Transformers\ActivityTransformer;
use Illuminate\Http\Request;

/**
 * Class ActivityLogController
 * @package App\Http\Controllers\User
 * Filter: https://github.com/mehradsadeghi/laravel-filter-querystring#Usage
 * Responder: https://github.com/flugg/laravel-responder#creating-transformers
 */
class ActivityLogController extends Controller
{
    /**
     * @return \Flugg\Responder\Http\Responses\SuccessResponseBuilder
     */
    public function viewLog(Request $request)
    {
        $request->validate([
            'per_page' => 'nullable|numeric',
            'page' => 'nullable|numeric'
        ]);

        $perPage = $request->get('per_page', 25);

        $log = $request->user()
            ->actions()
            ->leftJoin('device_info', 'activity_log.properties->uid', '=', 'device_info.uid')
            ->leftJoin('location_cache', 'activity_log.properties->ip', '=', 'location_cache.ip')
            ->select(
                'activity_log.id as id', 'log_name', 'description', 'activity_log.created_at', 'activity_log.updated_at', 'activity_log.properties',
                'device_info.uid', 'device_info.type', 'device_info.device', 'device_info.browser_name', 'device_info.browser_version', 'device_info.platform_name', 'device_info.platform_version',
                'location_cache.city_name', 'location_cache.latitude', 'location_cache.longitude', 'location_cache.country_code', 'location_cache.country_name',
            )
            ->orderBy('id', 'desc')
        ;

        return $this->responder->success($log->paginate($perPage), ActivityTransformer::class)
            ->with(Relationships::LOCATION)
            ->with(Relationships::DEVICE)
            ;
    }


}
