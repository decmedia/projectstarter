<?php

namespace App\Service\Settings;

use App\Constants\SettingsConst;
use App\Models\Settings;
use App\Models\User;
use Exception;
use Illuminate\Support\Collection;

class SettingsService
{
    protected User $user;
    protected Collection $settings;

    /**
     * SettingsService constructor.
     * @param User|null $user
     */
    public function __construct(?User $user = null)
    {
        if ($user) {
            $this->user = $user;
        }
    }

    /**
     * @param SettingsConst|string $key
     * @return mixed
     * @throws Exception
     */
    public function get($key): mixed
    {
        $collection = $this->collection()->where('key', $key);

        $defaultValue = $collection->whereNull('user_id')->first();
        $userValue = isset($this->user) ? $collection->where('user_id', $this->user->id)->first() : null;

        if (!$defaultValue && !$userValue) {
            throw new Exception("Setting '{$key->getValue()}' not found in DB");
        }

        $value = $userValue ? $userValue->toArray() : $defaultValue->toArray();
        return $this->convertValue($value['type'], $value['value']);
    }

    /**
     * @param $key
     * @param $value
     * @param string|null $description
     */
    public function set($key, $value, ?string $description = null): void
    {
        if (is_bool($value) || is_int($value)) {
            $method = 'setAsBool';
        } elseif (is_array($value)) {
            $method = 'setAsJson';
        } else {
            $method = 'setAsString';
        }

        $this->$method($key, $value, $description);
    }

    /**
     * @param User $user
     * @return $this
     */
    public function by(User $user): static
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param string|SettingsConst $key
     * @throws Exception
     */
    public function delete($key): void
    {
        if (!isset($this->user)) {
            throw new Exception('Only personal settings can be deleted');
        }

        Settings::where([
            'user_id' => $this->user->id,
            'key' => $key
        ])->delete();
    }

    /**
     * @return $this
     */
    public function refresh(): static
    {
        $default_settings = Settings::query()->whereNull('user_id')->get();
        $user_settings = isset($this->user) ? Settings::where('user_id', $this->user->id)->get() : [];

        // Настройки пользователя (user_id not null) перетирают системные
        $this->settings = $default_settings->merge($user_settings);

        return $this;
    }

    public function setAsBool($key, $value, ?string $description = null): void
    {
        if (is_string($value)) {
            $value = in_array(strtolower($value), ['1', 'true', 'y', 'true', 'yes']) ? '1' : '0';
        } elseif (is_bool($value) || is_int($value)) {
            $value = $value ? '1' : '0';
        } else {
            $value = (bool)$value;
        }

        $item = $this->firstOrNew($key, 'bool', $value, $description);
        if ($item) {
            $item->save();
        }
    }

    public function setAsString($key, $value, ?string $description = null): void
    {
        $item = $this->firstOrNew($key, 'string', $value, $description);
        $item?->save();
    }

    public function setAsJson($key, $value, ?string $description = null): void
    {
        $item = $this->firstOrNew($key, 'json', json_encode($value), $description);
        $item?->save();
    }

    /**
     * @return Collection
     */
    public function collection(): Collection
    {
        if (!isset($this->settings)) {
            $this->refresh();
        }

        return $this->settings;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        if (!isset($this->settings)) {
            $this->refresh();
        }

        $collection = $this->collection()->mapWithKeys(function ($item) {
            return [$item['key'] => $this->convertValue($item['type'], $item['value'])];
        });

        return $collection->toArray();
    }

    /**
     * @param string $type
     * @param string $value
     * @return bool|string
     */
    private function convertValue(string $type, string $value): bool|string
    {
        return match ($type) {
            'bool' => (bool)$value,
            'string' => $value,
            'json' => json_decode($value, true),
            default => '',
        };
    }

    /**
     * @param $key
     * @param string $type
     * @param $value
     * @param string|null $description
     * @return Settings|null
     */
    private function firstOrNew($key, string $type, $value, ?string $description = null): ?Settings
    {
        $collection = $this->collection()->where('key', $key);
        $default_value = $collection->whereNull('user_id')->first();
        if (isset($this->user) && $default_value->value === $value) {
            $user_value = $collection->where('user_id', $this->user->id)->first();
            $user_value?->delete();

            return null;
        }

        return Settings::firstOrNew([
            'user_id' => optional($this->user)->id ?: null,
            'key' => $key
        ], [
            'type' => $type,
            'value' => $value,
            'description' => $description,
        ]);
    }
}
