<?php

namespace App\Service\Settings\Facade;

use App\Constants\SettingsConst;
use App\Models\User;
use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed[] all()
 * @method static bool|string get($key)
 * @method static void set($key, $value, ?string $description = null)
 * @method static void delete($key)
 * @method static static by(User $user)
 *
 * @see SettingsService
 */
class Settings extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'Settings';
    }
}
