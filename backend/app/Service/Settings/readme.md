# SettingsService

* В системе есть настройки (key => value), со значениями по умолчанию.
* Настройки пользователя добавленные с таким же именем ключа, имеют большие приоритет и перетирают системную.
* Не все, что добавлены в DB, доступны к выводу и изменению пользователем.
* Перечень доступных к изменению (а также выводу на фронт), определяется в App\Constants\SettingsConst
* Хранятся в x_user_settings
* 1, '1', true, 'true', 'y', 'yes' => будет преобразовано в bool

### Use

Через фасад
```php
use App\Service\Settings\Facade\SettingsService

Settings::all();
Settings::get('key_name');
Settings::set('key_name', 'value');
Settings::delete('key_name');
```
В классах доступных [Laravel Service Container](https://laravel.com/docs/8.x/container) (контроллер и т.п.)
```php
use App\Service\Settings\Facade\SettingsService

public function method(SettingsService $settingsService)
{
    if ($settingsService->get('key_name')) {
        //...
    }
}
```
