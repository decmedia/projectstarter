<?php

namespace App\Transformers;

use App\Constants\Relationships;
use Flugg\Responder\Transformers\Transformer;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class MediaTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class MediaTransformer extends Transformer
{
    protected $relations = [
        Relationships::MEDIA_ADDITIONAL => MediaTransformer::class,
    ];

    /**
     * @param $row
     * @return mixed
     */
    public function transform($row)
    {
        [$mime, $type] = explode("/", $row->mime_type);

        return array_merge(
            [
                'id' => (int) $row->id,
                'custom_properties' => $row->custom_properties,
                'collection_name' => $row->collection_name,
                'name' => $row->name,
                'file_name' => $row->file_name,
                'mime_type' => $row->mime_type,
                'size' => $row->size,
                'preview_url' => $row->generated_conversions && $row->generated_conversions['thumb'] ? $row->getUrl('thumb') : '',
                'original_url' => $row->getUrl(),
                'folder_id' => (int)$row->folder_id,
                'created_at' => $row->created_at,
                'updated_at' => $row->updated_at,

                'mime' => $mime,
                'type' => $type,
            ]
        );
    }

    /**
     * @param $row
     * @return mixed
     */
    public function includeMediaAdditional($row)
    {
        $user = $row->model()->first();

        return $user->getMedia('additional', function (Media $media) use ($row, $user) {
            return $media->custom_properties['parent_id'] === $row->id;
        });
    }
}
