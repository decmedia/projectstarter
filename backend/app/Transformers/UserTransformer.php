<?php

namespace App\Transformers;

use App\Actions\User\CheckingAvailabilityOfEditingProfileAction;
use App\Models\User;
use Flugg\Responder\Transformers\Transformer;

/**
 * Class UserTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class UserTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => (int) $user->id,
            'parent_id' => (int) $user->parent_id,
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->date_add,
            'updated_at' => $user->date_add,

            'avatar' => $this->getUserAvatar($user),

            'twa_enabled' => (bool)$user->two_factor_secret,
            'sms_enabled' => (bool)$user->two_factor_secret,

            'has_verified_email' => $user->hasVerifiedEmail(),
        ];
    }

    private function getUserAvatar(User $user): string
    {
        $avatars = $user->getMedia('avatar');
        $conversion = $avatars->isNotEmpty() && $avatars->last()->hasGeneratedConversion('thumb') ? 'thumb' : '';

        return $avatars->isNotEmpty() ? $avatars->last()->getUrl($conversion) : '';
    }
}
