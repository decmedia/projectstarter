<?php

namespace App\Transformers;

use App\Constants\Relationships;
use App\Entity\Position;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Arr;

/**
 * Class ActivityTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class ActivityTransformer extends Transformer
{
    protected $relations = [
        Relationships::LOCATION => LocationTransformer::class,
        Relationships::DEVICE => DeviceTransformer::class,
    ];

    /**
     * @param $row
     * @return mixed
     */
    public function transform($row)
    {
        $properties = !empty($row->properties) ? json_decode($row->properties, true) : [];

        return array_merge(
            [
                'id' => (int) $row->id,
                'name' => $row->log_name,
                'description' => $row->description,
                'created_at' => $row->created_at ? $row->created_at->isoFormat('D MMM YYYY, h:mm') : null,
                'updated_at' => $row->updated_at ? $row->updated_at->isoFormat('D MMM YYYY, h:mm') : null,
                'ip' => Arr::get($properties, 'ip')
            ]
        );
    }

    /**
     * @param $row
     * @return Position
     */
    public function includeLocation($row)
    {
        return new Position([
            "countryName" => $row->country_name,
            "countryCode" => $row->country_code,
            "regionCode" => $row->region_code,
            "regionName" => $row->region_name,
            "cityName" => $row->city_name,
            "zipCode" => $row->zip_code,
            "isoCode" => $row->iso_code,
            "postalCode" => $row->postal_code,
            "latitude" => $row->latitude,
            "longitude" => $row->longitude,
            "metroCode" => $row->metro_code,
            "areaCode" => $row->area_code,
        ]);
    }

    /**
     * @param $row
     * @return array|mixed
     */
    public function includeDevice($row)
    {
        return $row->toArray();
    }
}
