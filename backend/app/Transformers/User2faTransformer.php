<?php

namespace App\Transformers;

use App\Models\User;
use Flugg\Responder\Transformers\Transformer;

/**
 * Class User2faTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class User2faTransformer extends Transformer
{
    /**
     * Transform the model.
     *
     * @param  \App\Models\User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'twa_enabled' => (bool)$user->two_factor_secret,
            'sms_enabled' => false, // TODO еще не реализовано
        ];
    }
}
