<?php

namespace App\Transformers;

use App\Entity\Position;
use Flugg\Responder\Transformers\Transformer;

/**
 * Class LocationTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class LocationTransformer extends Transformer
{
    /**
     * @param \App\Entity\Position $row
     * @return int[]
     */
    public function transform(?Position $row = null)
    {
        return $row ? [
            'country_name' => $row->countryName,
            'country_code' => $row->countryCode,
            'region_code' => $row->regionCode,
            'region_name' => $row->regionName,
            'city_name' => $row->cityName,
            'zip_code' => $row->zipCode,
            'iso_code' => $row->isoCode,
            'postal_code' => $row->postalCode,
            'latitude' => $row->latitude,
            'longitude' => $row->longitude,
            'metro_code' => $row->metroCode,
            'area_code' => $row->areaCode
        ] : [];
    }
}
