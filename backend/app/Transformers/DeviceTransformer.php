<?php

namespace App\Transformers;

use App\Entity\Position;
use Flugg\Responder\Transformers\Transformer;
use Illuminate\Support\Arr;

/**
 * Class DeviceTransformer
 * @package App\Transformers
 * @url https://github.com/flugg/laravel-responder#creating-transformers
 */
class DeviceTransformer extends Transformer
{
    /**
     * @param array $properties
     * @return array
     */
    public function transform(array $properties)
    {
        return Arr::only($properties, ['uid', 'browser_name', 'browser_version', 'platform_name', 'platform_version', 'device', 'type']);
    }
}
