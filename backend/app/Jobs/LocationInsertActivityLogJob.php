<?php

namespace App\Jobs;

use App\Models\LocationCache;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Stevebauman\Location\Facades\Location;

class LocationInsertActivityLogJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $ip;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * Получение страны и региона по ip дресу
     * и занесение его в кеш для вывода
     *
     * @return void
     */
    public function handle()
    {
        $location = Location::get($this->ip);
        if ($location) {
            $log = new LocationCache();
            $log->ip = $this->ip;
            $log->iso_code = $location->isoCode;
            $log->zip_code = $location->zipCode;
            $log->area_code = $location->areaCode;
            $log->city_name = $location->cityName;
            $log->latitude = $location->latitude;
            $log->longitude = $location->longitude;
            $log->postal_code = $location->postalCode;
            $log->region_code = $location->regionCode;
            $log->region_name = $location->regionCode;
            $log->country_code = $location->countryCode;
            $log->country_name = $location->countryName;
            $log->save();
        }
    }
}
