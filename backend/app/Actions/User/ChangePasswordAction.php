<?php

namespace App\Actions\User;

use App\Notifications\Auth\PasswordHasChangedNotification;
use Illuminate\Contracts\Hashing\Hasher;

/**
 * Class ChangePasswordAction
 * @package App\Actions\User
 */
class ChangePasswordAction
{
    /** @var \Illuminate\Contracts\Hashing\Hasher */
    private $hasher;

    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    /**
     * @param \App\Models\User $user
     * @param string $new_password
     */
    public function __invoke($user, string $new_password)
    {
        $user->forceFill([
            'password' => $this->hasher->make($new_password)
        ])->save();

        $user->notify(new PasswordHasChangedNotification());
    }
}
