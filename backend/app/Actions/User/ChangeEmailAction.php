<?php

namespace App\Actions\User;

use App\Exceptions\RuntimeException;
use App\Models\User;
use App\Models\ChangeEmail as ChangeEmailModel;
use App\Notifications\User\ChangeEmailNotification;
use Webmozart\Assert\Assert;

/**
 * Class ChangeEmail
 * @package App\Actions\Auth
 */
class ChangeEmailAction
{
    /**
     * @param User $user
     * @param string $new_email
     * @throws RuntimeException
     */
    public function __invoke($user, string $new_email)
    {
        Assert::email($new_email);

        if (isset($user->email) &&  $user->email === $new_email) {
            return;
        }

        if (User::query()->where('email', $new_email)->first()) {
            throw new RuntimeException('Email already exist');
        }

        if ($user->hasVerifiedEmail()) {
            ChangeEmailModel::firstOrCreate(
                ['user_id' =>  $user->id, 'email' => $new_email]
            );

            $user->notify(new ChangeEmailNotification($new_email));
        } else {
            $user->update(['email' => $new_email]);
            $user->sendEmailVerificationNotification();
        }
    }
}
