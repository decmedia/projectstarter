<?php

namespace App\Actions;

use App\Entity\Device;
use App\Models\DeviceInfo;

/**
 * Class FillDeviceInfoLog
 * @package App\Actions
 */
class FillDeviceInfoLog
{
    /**
     * @param Device $device
     */
    public function __invoke(Device $device)
    {
        DeviceInfo::firstOrCreate(['uid' => $device->getUid()])
            ->update($device->toArray())
        ;
    }
}
