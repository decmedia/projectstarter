<?php

namespace App\Actions\Activity;

use App\Actions\FillDeviceInfoLog;
use App\Constants\ActivityLogType;
use App\Entity\Device;
use App\Jobs\LocationInsertActivityLogJob;
use App\Models\LocationCache;
use Laravel\Sanctum\PersonalAccessToken;

/**
 * Class FillAuthActivityLog
 * @package App\Actions\Activity
 */
class FillAuthActivityLog
{
    /**
     * @param PersonalAccessToken $token
     * @param Device $device
     * @param string $ip
     */
    public function __invoke(PersonalAccessToken $token, Device $device, string $ip)
    {
        $user = $token->tokenable()->first();

        // В лог безопасности заносим факты авторизации с нового девайса
        // Номер девайса задается в cookie
        if ($device->getUid()) {
            $inputsFromThisDevice = $user->actions()
                ->where('properties->uid', $device->getUid())
                ->get()
            ;

            if ($inputsFromThisDevice->isEmpty()) {
                activity(ActivityLogType::SECURITY())
                    ->by($user)
                    ->performedOn($token)
                    ->withProperties([
                        'uid' => $device->getUid(),
                        'ip' => $ip,
                    ])
                    ->log(ActivityLogType::SECURITY() . '.new_device')
                ;
            }

            // Логируем параметры устройства, с которого был сделан запрос
            app(FillDeviceInfoLog::class)($device);
        }

        // В очереди получаем по ip адресу локацию и заносим
        // ее в лог для последующего вывода без новых запросов
        LocationInsertActivityLogJob::dispatchIf(!LocationCache::first('ip', $ip), $ip);
    }
}
