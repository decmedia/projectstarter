<?php

namespace App\Actions\Activity;

use App\Constants\ActivityLogType;
use App\Entity\Device;
use App\Jobs\LocationInsertActivityLogJob;
use App\Models\User;
use Stevebauman\Location\Position;

/**
 * Class FillChangePasswordActivityLog
 * @package App\Actions\Auth
 */
class FillChangePasswordActivityLog
{
    /**
     * @param User $user
     * @param Device $device
     * @param string $ip
     */
    public function __invoke(User $user, Device $device, string $ip)
    {
        $log = activity(ActivityLogType::SECURITY())
            ->by($user)
            ->performedOn($user)
            ->withProperties(
                array_merge(
                    $device->toArray(),
                    (new Position())->toArray(),
                    ['ip' => $ip],
                )
            )
            ->log('security.change_password')
        ;

        // в очереди получаем по ip адресу локацию и заносим
        // ее в лог для последующего вывода без новых запросов
        LocationInsertActivityLogJob::dispatch($log);
    }
}
