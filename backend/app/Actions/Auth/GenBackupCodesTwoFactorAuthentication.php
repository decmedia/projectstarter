<?php

namespace App\Actions\Auth;

use Illuminate\Support\Collection;
use Laravel\Fortify\RecoveryCode;

/**
 * Class GenBackupCodesTwoFactorAuthentication
 * @package App\Actions\Auth
 */
class GenBackupCodesTwoFactorAuthentication
{
    /**
     * Генерация и сохранение новых резервных кодов доступа
     *
     * @param  mixed  $user
     * @return void
     */
    public function __invoke($user)
    {
        $user->forceFill([
            'two_factor_recovery_codes' => encrypt(json_encode(Collection::times(1, function () {
                return RecoveryCode::generate();
            })->all())),
        ])->save();
    }
}
