<?php

namespace App\Actions\Auth;

/**
 * Class EnableTwoFactorAuthentication
 * @package App\Actions\Auth
 */
class EnableTwoFactorAuthentication
{
    /**
     * Enable two factor authentication for the user.
     *
     * @param  mixed  $user
     * @return void
     */
    public function __invoke($user, string $secret)
    {
        $user->forceFill([
            'two_factor_secret' => encrypt($secret)
        ])->save();
    }
}
