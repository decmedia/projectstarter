<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LogMailSendingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_mail', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('message_id')->unique();
            $table->string('log_name')->nullable();
            $table->string('description')->nullable();
            $table->string('event')->nullable();
            $table->string('mail_from');
            $table->string('mail_subject');
            $table->text('mail_body');
            $table->string('mail_recipient');
            $table->nullableMorphs('subject', 'log_mail_subject');
            $table->nullableMorphs('causer', 'log_mail_causer');
            $table->json('properties')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_mail');
    }
}
