<?php

namespace Database\Seeders;

use App\Constants\SettingsConst;
use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::create(['key' => SettingsConst::NEWSLETTER_SUBSCRIBE(), 'value' => 1, 'type' => 'bool', 'description' => 'Подписка на новости по умолчанию']);
    }
}
