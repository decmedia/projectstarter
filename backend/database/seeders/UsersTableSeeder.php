<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::runningUnitTests()) {
            return;
        }

        User::create([
            'id' => 1,
            'password' => Hash::make('password'),
            'email' => 'admin@example.com',
            'name' => 'John Doe',
            'parent_id' => 0
        ]);
    }
}
