import {Component, OnInit} from '@angular/core';
import {PubSubService} from "./core/services";
import {RouteConfigLoadEnd, RouteConfigLoadStart, Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {WindowRefService} from "./core/services/window.service";
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";
import {RxJsLoggingLevel, setRxJsLoggingLevel} from "./core/util/debug";
import {appAssert} from "./core/util/assert";
import {CookieService} from "./core/services/cookie-service/cookie.service";
import {random} from "./core/util";
import {environment} from '../environments/environment';
import {AppSettings} from "./config";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

	public loadingRouteConfig: boolean = false;

	constructor(
		private pubSubService: PubSubService,
		private router: Router,
		private snackBar: MatSnackBar,
		private windowRefService: WindowRefService,
		private matIconRegistry: MatIconRegistry,
		private domSanitizer: DomSanitizer,
		private cookieService: CookieService,
	) {
		// Set debug log level
		setRxJsLoggingLevel(environment.production ? RxJsLoggingLevel.DEBUG : RxJsLoggingLevel.ERROR);
		appAssert.enabled = environment.production;

		this.setDeviceUid();

		this.matIconRegistry
			.addSvgIcon(`alert-triangle`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/alert-triangle.svg'))
			.addSvgIcon(`dashboard`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/dashboard.svg'))
			.addSvgIcon(`account`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/account.svg'))
			.addSvgIcon(`plus-circle`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/plus-circle.svg'))
			.addSvgIcon(`success-circle`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/success-circle.svg'))
			.addSvgIcon(`desk`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/desk.svg'))
			.addSvgIcon(`binary`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/binary.svg'))
			.addSvgIcon(`certificate`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/certificate.svg'))
			.addSvgIcon(`leader`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/leader.svg'))
			.addSvgIcon(`team`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/team.svg'))
			.addSvgIcon(`news`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/news.svg'))
			.addSvgIcon(`promotion`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/promotion.svg'))
			.addSvgIcon(`brand-product`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/brand-product.svg'))
			.addSvgIcon(`support`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/support.svg'))
			.addSvgIcon(`webinar`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/webinar.svg'))
			.addSvgIcon(`eye`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/eye.svg'))
			.addSvgIcon(`eye-off`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/eye-off.svg'))
			.addSvgIcon(`menu`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/menu.svg'))
			.addSvgIcon(`edit`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/edit.svg'))
			.addSvgIcon(`plus`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/plus.svg'))
			.addSvgIcon(`check-circle`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/check-circle.svg'))
			.addSvgIcon(`alert-circle`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/alert-circle.svg'))
			.addSvgIcon(`calendar`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/calendar.svg'))
			.addSvgIcon(`camera`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/photo_camera.svg'))
			.addSvgIcon(`x`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/x.svg'))
			.addSvgIcon(`download`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/download.svg'))
			.addSvgIcon(`copy`, this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/images/icons/copy.svg'))

		;
	}

	ngOnInit() {
		this.loadIndicator();
		this.onCoreEvent();
	}

	/**
	 * Отображение сообщения о загрузке при переходе между модулями (ленивая загрузка)
	 */
	private loadIndicator(): void {
		let timeOutId: undefined;
		this.router.events.subscribe(event => {
			if (event instanceof RouteConfigLoadStart) {
				timeOutId = this.windowRefService.nativeWindow.setTimeout(() => {
					this.loadingRouteConfig = true;
				}, 1);
			} else if (event instanceof RouteConfigLoadEnd) {
				if (timeOutId !== undefined) {
					this.loadingRouteConfig = false;
					this.windowRefService.nativeWindow.clearTimeout(timeOutId);
				}
			}
		});
	}

	/**
	 * Обработка событий внутри системы от pubSubService
	 * this.pubSubService.publish('core', err) - отправляем
	 * this.pubSubService.on('core').subscribe - принимаем
	 */
	private onCoreEvent(): void {

		this.pubSubService.on('core')
			.subscribe((data): void => {
				console.log(data);

				switch (data.mode) {
					case 'login':
						break;
					default:
						this.errorHandler(data);
						break;
				}
			});
	}

	private setDeviceUid() {
		const cookieExists: boolean = this.cookieService.check('device');
		if (!cookieExists) {
			this.cookieService.set('device', random());
		}
	}

	private errorHandler(err: any): void {

		// TODO: добавить обработку потери соединения (напр. включить прокси)

		console.log(err);

		// Только ошибки в действиях пользователя или в ситуациях что от него зависят
		// Для всех остальных, выводим только номер и показываем кнопку связи с поддержкой
		// ибо все остальное БАГИ системы и подлежат отладке
		const codes: { [index: string]: any } = {
			'page_not_found': 'The session is out of date. Refresh the page and start over',

		};

		// Ошибки, о которых сообщение не выводим
		const mute = [
			2001,
			'validation_failed'
		];

		const code = err.code || (err.error?.error?.code) || 500;

		if (mute.includes(code)) {
			return;
		}

		const btnMessage = code === 'page_not_found' ? 'Refresh' : 'Contact support';
		const message = codes[code] ? codes[code] : 'An error has occurred';
		const snackBarRef = this.snackBar.open(`${message}. [code: ${code}]`, btnMessage, {
			verticalPosition: 'top',
			duration: 10000,
		});

		snackBarRef.onAction().subscribe(() => {
			code === 'page_not_found'
				? window.location.reload()
				: window.location.href = `mailto:${AppSettings.APP_EMAIL_SUPPORT}`
			;
		});
	}
}
