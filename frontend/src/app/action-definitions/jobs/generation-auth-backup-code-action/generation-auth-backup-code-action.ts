import {Injectable} from '@angular/core';
import {ActionDefinition} from '../../action-definition';
import {GenerationAuthBackupCodeActionParams} from './generation-auth-backup-code-action-params';
import {ConfirmationDialogService} from '../../../dialogs/confirmation-dialog/services/confirmation-dialog.service';
import {filter, switchMap, tap} from 'rxjs/operators';
import {ActionResult} from '../../action-result';
import {PasswordProtectionDialogService} from "../../../dialogs/password-protection-dialog/services/password-protection-dialog.service";
import {ActionDefinitionContextMenu} from "../../action-definition-context-menu";
import {GenerationAuthBackupCodeDialogService} from "../../../dialogs/generation-auth-backup-code-dialog/services/generation-auth-backup-code-dialog.service";

@Injectable({
	providedIn: 'root',
})
export class GenerationAuthBackupCodeAction extends ActionDefinition<GenerationAuthBackupCodeActionParams> {

	constructor(
		private confirmationDialogService: ConfirmationDialogService,
		private passwordProtectionDialogService: PasswordProtectionDialogService,
		private generationAuthBackupCodeDialogService: GenerationAuthBackupCodeDialogService,
	) {
		super();
	}

	async invoke(params: GenerationAuthBackupCodeActionParams): Promise<ActionResult> {
		this.loading$.next(true);

		return (await this.passwordProtectionDialogService
			.pass$())
			.pipe(
				tap(() => this.loading$.next(false)),
				filter(password => typeof password === "string"),
				switchMap((password) => this.generationAuthBackupCodeDialogService.open$({password: String(password)}))
			);
	}

	protected getMenu(): ActionDefinitionContextMenu {
		return {
			name: 'Создать новый резервный код',
		};
	}
}
