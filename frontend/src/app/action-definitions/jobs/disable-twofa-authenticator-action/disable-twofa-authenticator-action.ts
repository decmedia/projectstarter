import {Injectable} from '@angular/core';
import {ActionDefinition} from '../../action-definition';
import {DisableTwoFaAuthenticatorActionParams} from './disable-twofa-authenticator-action-params';
import {ConfirmationDialogService} from '../../../dialogs/confirmation-dialog/services/confirmation-dialog.service';
import {filter, switchMap, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActionResult} from '../../action-result';
import {PasswordProtectionDialogService} from "../../../dialogs/password-protection-dialog/services/password-protection-dialog.service";
import {Observable} from "rxjs";
import {AppSettings} from "../../../config";
import {HttpClient} from "@angular/common/http";
import {ActionDefinitionContextMenu} from "../../action-definition-context-menu";

@Injectable({
	providedIn: 'root',
})
export class DisableTwoFaAuthenticatorAction extends ActionDefinition<DisableTwoFaAuthenticatorActionParams> {

	constructor(
		private confirmationDialogService: ConfirmationDialogService,
		private passwordProtectionDialogService: PasswordProtectionDialogService,
		private snackBar: MatSnackBar,
		private http: HttpClient,
	) {
		super();
	}

	async invoke(params: DisableTwoFaAuthenticatorActionParams): Promise<ActionResult> {
		this.loading$.next(true);

		return (await this.confirmationDialogService
			.open$({
				title: 'Подтвердите отключение',
				content: 'В последствии, для новой настройки вашего приложения двухэтапной аутентификации, необходимо будет добавить и настроить его занового. Если были созданы резервные ключи доступа, они также будут удалены',
				confirmButtonText: 'Да, отключить',
				cancelButtonText: 'Отмена'
			}))
			.pipe(
				tap(() => this.loading$.next(false)),
				filter(Boolean),
				switchMap(() => this.passwordProtectionDialogService.pass$()),
				filter(password => typeof password === "string"),
				switchMap(() => this.twoFaDisable()),
				tap(() => this.snackBar.open('Отключено'))
			);
	}

	protected getMenu(): ActionDefinitionContextMenu {
		return {
			name: 'Не использовать двухэтапную аутентификацию',
		};
	}

	private twoFaDisable(): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`);
	}
}
