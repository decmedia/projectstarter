import {Injectable} from '@angular/core';
import {ActionDefinition} from '../../action-definition';
import {EnableTwoFaAuthenticatorActionParams} from './enable-twofa-authenticator-action-params';
import {filter, switchMap, tap} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActionResult} from '../../action-result';
import {PasswordProtectionDialogService} from "../../../dialogs/password-protection-dialog/services/password-protection-dialog.service";
import {Observable, of} from "rxjs";
import {AppSettings} from "../../../config";
import {HttpClient} from "@angular/common/http";
import {ActionDefinitionContextMenu} from "../../action-definition-context-menu";
import {SettingsTwoFactorAuthenticationDialogService} from "../../../dialogs/settings-two-factor-authentication-dialog/services/settings-two-factor-authentication-dialog.service";
import {ITwoFaStatus} from "../../../models/TwoFaStatus";

@Injectable({
	providedIn: 'root',
})
export class EnableTwoFaAuthenticatorAction extends ActionDefinition<EnableTwoFaAuthenticatorActionParams> {

	constructor(
		private settingsTwoFactorAuthenticationDialogService: SettingsTwoFactorAuthenticationDialogService,
		private passwordProtectionDialogService: PasswordProtectionDialogService,
		private snackBar: MatSnackBar,
		private http: HttpClient,
	) {
		super();
	}

	async invoke(params: EnableTwoFaAuthenticatorActionParams): Promise<ActionResult> {
		this.loading$.next(true);

		return (await this.twoFaAuthStatus()
			.pipe(
				switchMap((status) => {
					if (status.twa_enabled) {
						return this.passwordProtectionDialogService.pass$();
					} else {
						return of('')
					}
				}),
				tap(() => this.loading$.next(false)),
				filter(password => typeof password === "string"),
				tap(() => this.loading$.next(true)),
				switchMap((password) => this.settingsTwoFactorAuthenticationDialogService.open$({password})),
				tap(() => this.loading$.next(false)),
				filter(Boolean),
				tap(() => this.snackBar.open('Настройка завершена'))
			)
		);
	}

	protected getMenu(): ActionDefinitionContextMenu {
		return {
			name: 'Настроить двухэтапную аутентификацию',
		};
	}

	private twoFaAuthStatus(): Observable<ITwoFaStatus> {
		return this.http.get<ITwoFaStatus>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`);
	}
}
