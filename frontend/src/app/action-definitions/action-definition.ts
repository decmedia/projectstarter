import {ActionDefinitionContextMenu} from './action-definition-context-menu';
import {BuildConfig} from './build-config';
import {BehaviorSubject, isObservable} from 'rxjs';
import {take} from 'rxjs/operators';
import {ActionResult} from './action-result';
import {ContextMenuActionModel} from "../components/context-menu/models/context-menu-action.model";

export abstract class ActionDefinition<Params> {

	loading$ = new BehaviorSubject<boolean>(false);

	build<Actor>(config ?: BuildConfig<Actor, Params>): ContextMenuActionModel<Actor> {
		const menu = this.getMenu();

		return {
			name: menu.name,
			icon: menu.icon ?? '',
			loading$: this.loading(),
			isHidden: actor => config?.isHidden?.(actor),
			action: async actor => {
				const result = await this.invoke(config?.resolveParams?.(actor));
				if (isObservable(result)) {
					result
						.pipe(take(1))
						.subscribe((a) => config?.onSuccess?.(a));
				} else {
					config?.onSuccess?.();
				}
			},
		};
	}

	abstract invoke(params ?: Params): ActionResult;

	protected abstract getMenu(): ActionDefinitionContextMenu;

	// protected abstract loading(): BehaviorSubject<boolean>;

	protected loading(): BehaviorSubject<boolean> {
		return this.loading$;
	}
}
