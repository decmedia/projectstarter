import {ILocation} from "./Location";
import {IDevice} from "./Device";
import {IPagination} from "../core/services/http/models/Pagination";

export interface IActivity extends IPagination {
	id: number;
	ip: string;
	name: string;
	description: string;
	created_at: string;
	updated_at: string;
	location: ILocation;
	device: IDevice;
}
