export interface IPreferences {
	newsletter_subscribe: boolean;
	new_invite_subscribe: boolean;
	new_webinar_subscribe: boolean;
	desc_update_subscribe: boolean;
}
