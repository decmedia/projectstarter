export interface IDevice {
	uid: string;
	type: string;
	device: string;
	browser_name: string;
	platform_name: string;
	browser_version: string;
	platform_version: string;
}
