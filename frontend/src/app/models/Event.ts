export interface IEvent<T> {
	id: string;
	type: string;
	payload: T;
}
