export interface ITwoFaStatus {
	twa_enabled: boolean;
	sms_enabled: boolean;
}
