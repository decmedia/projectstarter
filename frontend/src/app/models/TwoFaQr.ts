export interface ITwoFaQr {
	svg: string;
	code: string;
}
