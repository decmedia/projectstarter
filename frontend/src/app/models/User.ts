import {ITimestamps} from "./Timestamps";

export interface IUser extends ITimestamps {
	id: number;
	parent_id: number;
	name: string;
	avatar: string;
	email: string;
	has_verified_email: boolean;
}
