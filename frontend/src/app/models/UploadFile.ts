import {BehaviorSubject} from "rxjs";

export interface IUploadFile {
	id: string;
	size: number;
	name: string;
	progress$: BehaviorSubject<number>;
}
