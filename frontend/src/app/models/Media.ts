import {ITimestamps} from "./Timestamps";

export interface IMedia extends ITimestamps {
	id: number;
	size: number;
	custom_properties?: IMediaCustomProperties;
	collection_name: string;
	name: string;
	file_name: string;
	mime_type: string;
	preview_url: string;
	original_url: string;
	folder_id: number;
	mime: string;
	type: string;
	media_additional?: IMedia[];
}

export interface IMediaCustomProperties {
}
