export interface ILocation {
	country_name: string;
	country_code: string;
	region_code: string;
	region_name: string;
	city_name:string;
	zip_code: string;
	iso_code: string;
	postal_code: string;
	latitude: string;
	longitude: string;
	metro_code: string;
	area_code: string;
}
