import {IPagination} from "./Pagination";
import {ILocation} from "./Location";
import {IDevice} from "./Device";

export interface ISession extends IPagination {
	id: number;
	ip: string;
	last_used_at: string;
	current: boolean;
	location: ILocation[];
	device: IDevice[];
}
