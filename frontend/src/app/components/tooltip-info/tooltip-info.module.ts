import {NgModule} from '@angular/core';
import {TooltipInfoComponent} from './tooltip-info.component';
import {NzPopoverModule} from "ng-zorro-antd/popover";

@NgModule({
	imports: [
		NzPopoverModule
	],
	declarations: [
		TooltipInfoComponent,
	],
	exports: [
		TooltipInfoComponent
	],
})
export class TooltipInfoModule {
}
