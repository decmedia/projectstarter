import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';

@Component({
	selector: 'app-tooltip-info',
	templateUrl: './tooltip-info.component.html',
	styles: [
		`:host {
			display: inline-block;
		}`
	],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class TooltipInfoComponent {}
