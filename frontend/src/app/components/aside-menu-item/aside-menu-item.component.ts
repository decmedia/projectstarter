import {Component, Input} from '@angular/core';
import {changeDetection} from "../../core/util";
import {IsActiveMatchOptions} from "@angular/router";

@Component({
	selector: 'app-aside-menu-item',
	templateUrl: './aside-menu-item.component.html',
	styleUrls: ['./aside-menu-item.component.scss'],
	changeDetection
})
export class AsideMenuItemComponent {

	@Input() icon!: string;
	@Input() link!: string;
	@Input() linkOptions?: { exact: boolean; } | IsActiveMatchOptions | any;
	@Input() counter?: number;

}
