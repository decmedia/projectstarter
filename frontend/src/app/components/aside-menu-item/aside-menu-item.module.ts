import {NgModule} from '@angular/core';
import {AsideMenuItemComponent} from './aside-menu-item.component';
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {CommonModule} from "@angular/common";

@NgModule({
	imports: [
		RouterModule,
		MatIconModule,
		CommonModule
	],
	declarations: [
		AsideMenuItemComponent
	],
	exports: [
		AsideMenuItemComponent
	],
})
export class AsideMenuItemModule {}
