import {NgModule} from '@angular/core';
import {MatButtonSpinnerDirective} from "./mat-button-loading";

@NgModule({
	imports: [],
	declarations: [
		MatButtonSpinnerDirective
	],
	exports: [
		MatButtonSpinnerDirective
	],
})
export class QwMatButtonSpinnerModule {}
