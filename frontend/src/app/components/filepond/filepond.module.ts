import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilePondComponent} from './filepond.component';
import {registerPlugin} from "filepond";

// @ts-ignore
import FilePondPluginFileValidateType from "filepond-plugin-file-validate-type";
// @ts-ignore
import FilePondPluginFileValidateSize from "filepond-plugin-file-validate-size";
// @ts-ignore
import FilePondPluginImagePreview from "filepond-plugin-image-preview";

registerPlugin(FilePondPluginFileValidateType);
registerPlugin(FilePondPluginFileValidateSize);
registerPlugin(FilePondPluginImagePreview);

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [FilePondComponent],
	exports: [
		FilePondComponent
	]
})
export class FilePondModule {
}
