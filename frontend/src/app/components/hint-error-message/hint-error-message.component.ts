import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from "@angular/forms";
import {Observable} from "rxjs";
import {changeDetection} from "../../core/util";

@Component({
	selector: 'app-hint-error-message',
	templateUrl: './hint-error-message.component.html',
	styleUrls: ['./hint-error-message.component.scss'],
	changeDetection
})
export class HintErrorMessageComponent implements OnInit {

	@Input() field: AbstractControl | null | undefined;
	message$ ?: Observable<string>;

	ngOnInit() {
		this.message$ = this.field?.statusChanges;
	}
}
