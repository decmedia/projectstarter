import {NgModule} from '@angular/core';
import {HintErrorMessageComponent} from './hint-error-message.component';
import {CommonModule} from "@angular/common";

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [
		HintErrorMessageComponent,
	],
	exports: [
		HintErrorMessageComponent
	],
})
export class HintErrorMessageModule {}
