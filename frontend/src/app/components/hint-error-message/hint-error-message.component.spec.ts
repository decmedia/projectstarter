import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HintErrorMessageComponent } from './hint-error-message.component';

describe('HintErrorMessageComponent', () => {
  let component: HintErrorMessageComponent;
  let fixture: ComponentFixture<HintErrorMessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HintErrorMessageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HintErrorMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
