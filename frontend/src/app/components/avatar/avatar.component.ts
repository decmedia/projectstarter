import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';
import {requiredSetter, defaultProp} from "../../core/decorators";
import {stringHashToHsl} from "../../core/util/string-hash-to-hsl";
import {SizeRubber, SizeXS, SizeXXL} from "../../core/types/size";
import {sizeBigger} from "../../core/util/size-bigger";

@Component({
	selector: 'app-avatar',
	changeDetection: ChangeDetectionStrategy.OnPush,
	templateUrl: './avatar.component.html',
	styleUrls: ['./avatar.component.scss'],
})
export class AvatarComponent {
	@Input()
	@HostBinding('attr.data-host-size')
	@defaultProp()
	size: SizeXS | SizeXXL | SizeRubber = 'inherit';

	@Input('avatarUrl')
	@requiredSetter()
	set avatarUrlSetter(avatarUrl: string) {
		this.isUrlValid = !!avatarUrl;
		this.avatarUrl = avatarUrl;
	}

	@Input()
	@defaultProp()
	text = '';

	@Input()
	@defaultProp()
	autoColor = false;

	@Input()
	@HostBinding('class.rounded')
	@defaultProp()
	rounded = true;

	avatarUrl!: string;

	isUrlValid = false;

	@HostBinding('style.background')
	get bgColor(): string {
		return this.autoColor ? stringHashToHsl(this.text) : '';
	}

	@HostBinding('class.has-avatar')
	get hasAvatar(): boolean {
		return this.avatarUrl !== undefined && this.isUrlValid;
	}

	get computedText(): string {
		if (this.hasAvatar || this.text === '') {
			return '';
		}

		const words = this.text.split(' ');

		return words.length > 1 && sizeBigger(this.size)
			? words[0].substr(0, 1) + words[1].substr(0, 1)
			: words[0].substr(0, 1);
	}

	onError() {
		this.isUrlValid = false;
	}
}
