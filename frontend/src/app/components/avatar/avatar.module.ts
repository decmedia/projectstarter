import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {AvatarComponent} from './avatar.component';
import {LazyLoadingModule} from "../../core/directive/lazy-loading/lazy-loading.module";

@NgModule({
	imports: [CommonModule, LazyLoadingModule],
	declarations: [AvatarComponent],
	exports: [AvatarComponent],
})
export class AvatarModule {}
