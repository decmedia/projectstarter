import {Component, Inject, OnInit} from '@angular/core';
import {changeDetection} from "../../core/util";
import {BehaviorSubject, Observable} from "rxjs";
import {IUser} from "../../models/User";
import {DestroyService, PubSubService} from "../../core/services";
import {UserStateService} from "../../core/services";
import {finalize, takeUntil} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-confirmation-email-notification',
	templateUrl: './confirmation-email-notification.component.html',
	styleUrls: ['./confirmation-email-notification.component.scss'],
	changeDetection
})
export class ConfirmationEmailNotificationComponent implements OnInit {

	notifyBlock$ = new BehaviorSubject<boolean>(false);
	resendEmailVerifyLoading$ = new BehaviorSubject<boolean>(false);
	user$: Observable<IUser | null>;

	constructor(
		private pubSubService: PubSubService,
		private userStateService: UserStateService,
		private snackBar: MatSnackBar,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.user$ = this.userStateService.user$;
	}

	resendEmailVerify() {
		this.resendEmailVerifyLoading$.next(true);
		this.userStateService.resendEmailVerify()
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.resendEmailVerifyLoading$.next(false))
			)
			.subscribe(() => {
				this.notifyBlock$.next(true);
				this.snackBar.open('Письмо отправлено');
			});
	}

	ngOnInit(): void {
		this.onEvent();
	}

	private onEvent(): void {
		this.pubSubService.on('App\\Notifications\\User\\VerifiedEmailNotification')
			.pipe(takeUntil(this.destroy$))
			.subscribe(() => this.notifyBlock$.next(true));
	}
}
