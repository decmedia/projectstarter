import {NgModule} from '@angular/core';
import {ConfirmationEmailNotificationComponent} from './confirmation-email-notification.component';
import {MatIconModule} from "@angular/material/icon";
import {CommonModule} from "@angular/common";
import {MatButtonModule} from "@angular/material/button";
import {QwMatButtonSpinnerModule} from "../mat-button-spinner/mat-button-spinner.module";

@NgModule({
	imports: [
		MatIconModule,
		CommonModule,
		MatButtonModule,
		QwMatButtonSpinnerModule,
	],
	declarations: [
		ConfirmationEmailNotificationComponent
	],
	exports: [
		ConfirmationEmailNotificationComponent
	],
})
export class ConfirmationEmailNotificationModule {}
