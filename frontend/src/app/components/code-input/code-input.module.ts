import {ModuleWithProviders, NgModule} from '@angular/core';
import {QwCodeInputComponent } from './code-input.component';
import {CommonModule} from '@angular/common';
import {CodeInputComponentConfig, CodeInputComponentConfigToken} from './code-input.component.config';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	  QwCodeInputComponent
  ],
  exports: [
	  QwCodeInputComponent
  ]
})
export class QwCodeInputModule {
  static forRoot(config: CodeInputComponentConfig): ModuleWithProviders<QwCodeInputModule> {
    return {
      ngModule: QwCodeInputModule,
      providers: [
        {provide: CodeInputComponentConfigToken, useValue: config }
      ]
    };
  }
}
