import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Injectable} from '@angular/core';
import {first} from 'rxjs/operators';
import {InlineDialogService} from "./inline.service";
import {DialogComponent} from "./dialog.component";


@Injectable()
export class DialogsService<T = undefined> {

	constructor(private dialog: MatDialog) {}

	// inline dialog
	open(dialogData: any, options: DialogOptions = {width: 500, disableClose: false}): InlineDialogService<T> {
		const dialogRef = this.dialog.open<DialogComponent<T>, DialogData<T>>(DialogComponent, {
			...this.fetchOptions(options),
			data: dialogData,
		});

		dialogRef.afterClosed().pipe(first());

		return new InlineDialogService(dialogRef);
	}

	private fetchOptions({width, disableClose}: DialogOptions): Pick<MatDialogConfig<DialogData<T>>, 'width' | 'disableClose'> {
		return {
			width: `${width}px`,
			disableClose,
		};
	}
}

class DialogOptions<T = any> {
	public width = 500;
	public disableClose = true;
}
class DialogData<T = any> {}
