import {TemplateRef} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {first} from 'rxjs/operators';
import {DialogComponent} from "./dialog.component";

type DialogRef<T> = MatDialogRef<DialogComponent<T>>;

export class InlineDialogService<T = undefined> {

	opened$ = this.dialogRef.afterOpened().pipe(first());

	constructor(private dialogRef: DialogRef<T>) {}

	get context(): any {
		return this.dialogRef.componentInstance.data.context;
	}

	close(): void {
		this.dialogRef.close();
	}

	setTemplate(template: TemplateRef<any>): void {
		this.dialogRef.componentInstance.data.template = template;
	}
}
