import {Component, Inject, TemplateRef} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

/**
 * A common component rendered as a Material dialog
 */
@Component({
	selector: 'app-esm-dialog',
	template: `
		<ng-container [ngTemplateOutlet]="data.template"></ng-container>
	`,
})
export class DialogComponent<T> {
	/**
	 * Initializes the component.
	 *
	 * @param dialogRef - A reference to the dialog opened.
	 */
	constructor(
		public dialogRef: MatDialogRef<DialogComponent<T>>,
		@Inject(MAT_DIALOG_DATA)
		public data: {
			headerText: string;
			template: TemplateRef<any>;
			context: T;
		}
	) {
		console.log(this.data);
	}
}
