import {NgModule} from '@angular/core';
import {DialogComponent} from "./dialog.component";
import {InlineDialogService} from "./inline.service";
import {CommonModule} from "@angular/common";

@NgModule({
	imports: [
		CommonModule
	],
	exports: [DialogComponent],
	declarations: [DialogComponent],
	providers: [InlineDialogService],
	entryComponents: [DialogComponent],
})
export class InlineDialogsModule {}
