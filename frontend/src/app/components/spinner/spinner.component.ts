import {
	ChangeDetectionStrategy,
	Component, ElementRef,
	Inject, InjectionToken, Input,
	Optional,
	ViewEncapsulation
} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {CanColor, mixinColor} from "@angular/material/core";
import {coerceNumberProperty} from "@angular/cdk/coercion";

/**
 * Base reference size of the spinner.
 * @docs-private
 */
const BASE_SIZE = 25;

/**
 * Base reference stroke width of the spinner.
 * @docs-private
 */
const BASE_STROKE_WIDTH = 2;

const _MatProgressSpinnerBase = mixinColor(class {
	constructor(public _elementRef: ElementRef) {}
}, 'primary');

export interface MatProgressSpinnerDefaultOptions {
	/** Diameter of the spinner. */
	diameter?: number;
	/** Width of the spinner's stroke. */
	strokeWidth?: number;
}

/** Injection token to be used to override the default options for `mat-progress-spinner`. */
export const MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS =
	new InjectionToken<MatProgressSpinnerDefaultOptions>('mat-progress-spinner-default-options', {
		providedIn: 'root',
		factory: MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS_FACTORY,
	});

/** @docs-private */
export function MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS_FACTORY(): MatProgressSpinnerDefaultOptions {
	return {diameter: BASE_SIZE};
}

@Component({
	selector: 'app-spinner',
	templateUrl: './spinner.component.html',
	styleUrls: ['./spinner.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None,
	inputs: ['color'],
	host: {
	'role': 'progressbar',
	'class': 'app-progress-spinner',
	// set tab index to -1 so screen readers will read the aria-label
	// Note: there is a known issue with JAWS that does not read progressbar aria labels on FireFox
	'tabindex': '-1',

	'[style.width.px]': 'diameter',
	'[style.height.px]': 'diameter',
},
})
export class SpinnerComponent extends _MatProgressSpinnerBase implements CanColor {

	private _diameter = BASE_SIZE;
	private _strokeWidth: number | undefined;

	/** The diameter of the progress spinner (will set width and height of svg). */
	@Input()
	get diameter(): number { return this._diameter; }
	set diameter(size: number) {
		this._diameter = coerceNumberProperty(size);
	}

	/** Stroke width of the progress spinner. */
	@Input()
	get strokeWidth(): number {
		return this._strokeWidth || this.diameter / 10;
	}
	set strokeWidth(value: number) {
		this._strokeWidth = coerceNumberProperty(value);
	}

	constructor(
		public _elementRef: ElementRef<HTMLElement>,
		@Optional() @Inject(DOCUMENT) private _document: any,
		@Inject(MAT_PROGRESS_SPINNER_DEFAULT_OPTIONS) defaults?: MatProgressSpinnerDefaultOptions
	) {
		super(_elementRef);

		if (defaults) {
			if (defaults.diameter) {
				this.diameter = defaults.diameter;
			}

			if (defaults.strokeWidth) {
				this.strokeWidth = defaults.strokeWidth;
			}
		}
	}
}
