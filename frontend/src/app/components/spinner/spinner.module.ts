import {NgModule} from '@angular/core';
import {SpinnerComponent} from './spinner.component';
import {SpinnerButtonLoadingDirective} from "./spinner-button-loading.directive";

@NgModule({
	imports: [],
	declarations: [
		SpinnerComponent,
		SpinnerButtonLoadingDirective
	],
	exports: [
		SpinnerComponent,
		SpinnerButtonLoadingDirective
	],
})
export class SpinnerModule {}
