import {BehaviorSubject} from "rxjs";

export interface ContextMenuActionModel<T> {
	icon: string;
	name: string;
	isHidden: (actor: T) => boolean | undefined;
	action: (actor: T) => void;
	loading$ ?: BehaviorSubject<boolean>;
}
