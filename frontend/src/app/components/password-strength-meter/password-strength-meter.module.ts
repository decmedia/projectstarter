import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {QwPasswordStrengthMeterComponent} from './password-strength-meter.component';
import {MatIconModule} from "@angular/material/icon";

@NgModule({
	imports: [CommonModule, MatIconModule],
	declarations: [QwPasswordStrengthMeterComponent],
	exports: [QwPasswordStrengthMeterComponent]
})
export class QwPasswordStrengthMeterModule {
}
