import { TestBed, inject } from '@angular/core/testing';

import { QwPasswordStrengthMeterService } from './password-strength-meter.service';

describe('PasswordStrengthMeterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QwPasswordStrengthMeterService]
    });
  });

  it('should be created', inject([QwPasswordStrengthMeterService], (service: QwPasswordStrengthMeterService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a number as score', inject(
    [QwPasswordStrengthMeterService],
    (service: QwPasswordStrengthMeterService) => {
      expect(service.score('aarasddasdsad')).toEqual(jasmine.any(Number));
    }
  ));

  it('should return a score and feedback', inject(
    [QwPasswordStrengthMeterService],
    (service: QwPasswordStrengthMeterService) => {
      const result = service.scoreWithFeedback('aarasddasdsad');
      expect(result).toEqual(jasmine.any(Object));
      expect(Object.keys(result)).toContain('score');
      expect(Object.keys(result)).toContain('feedback');
    }
  ));
});
