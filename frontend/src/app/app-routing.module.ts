import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
	{path: 'account', loadChildren: () => import(/* webpackChunkName: "account-module" */'./views/account/account.module').then(m => m.AccountModule), pathMatch: 'prefix'},
	{path: 'auth', loadChildren: () => import(/* webpackChunkName: "auth-module" */'./views/auth/auth.module').then(m => m.AuthModule), pathMatch: 'prefix'},
	{path: '', loadChildren: () => import(/* webpackChunkName: "main-module" */'./views/main/main.module').then(m => m.MainIndexModule), pathMatch: 'prefix'},
	{path: '**', redirectTo: ''}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
