import {Injectable} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {PasswordProtectionDialogDataModel} from '../models/password-protection-dialog-data.model';
import {PasswordProtectionDialogComponent} from '../password-protection-dialog.component';
import {Observable, of} from 'rxjs';
import {AsyncDialog} from '../../async-dialog';
import {fromPromise} from 'rxjs/internal-compatibility';
import {switchMap, tap} from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class PasswordProtectionDialogService extends AsyncDialog<PasswordProtectionDialogComponent, PasswordProtectionDialogDataModel, string | undefined> {

	private password ?: string;
	private sessionDuration: number = 5 * 60;
	private sessionStartDate ?: Date;

	pass$(): Observable<string | undefined> {
		if (this.passwordSessionValidate()) {
			this.sessionUpdate();
			return of(this.password);
		} else {
			return this.open$({})
				.pipe(tap((res) => {
					if (res) {
						this.sessionUpdate();
						return this.password = res;
					} else {
						return undefined;
					}
				}));
		}
	}

	async open(data: PasswordProtectionDialogDataModel): Promise<MatDialogRef<PasswordProtectionDialogComponent, string | undefined>> {
		const {PasswordProtectionDialogModule} = await import('../password-protection-dialog.module');

		return this.matDialog.open(PasswordProtectionDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: PasswordProtectionDialogDataModel): Observable<string | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}

	/**
	 * Проверяем права доступа у пользователя запросом у него текущего пароля. В случае успеха,
	 * сохраняем дату последнего успешного ответа, чтобы не тревожить пользователя
	 * запросами слишком часто. Если время вышло, показываем окно снова
	 */
	private sessionUpdate() {
		this.sessionStartDate = new Date;
	}

	private passwordSessionValidate(): boolean {
		if (!this.sessionStartDate || !this.password) {
			return false;
		}

		const diff = (this.sessionStartDate.getTime() - (new Date).getTime()) / 1000;
		return Math.abs(Math.round(diff)) < this.sessionDuration;
	}
}
