import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {PasswordProtectionDialogComponent} from "./password-protection-dialog.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../components/hint-error-message";
import {SpinnerModule} from "../../components/spinner";

@NgModule({
	declarations: [PasswordProtectionDialogComponent],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		ReactiveFormsModule,
		MatIconModule,
		HintErrorMessageModule,
		SpinnerModule,
	],
})
export class PasswordProtectionDialogModule {
	static getComponent(): typeof PasswordProtectionDialogComponent {
		return PasswordProtectionDialogComponent;
	}
}
