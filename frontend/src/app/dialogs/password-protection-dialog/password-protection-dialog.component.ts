import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PasswordProtectionDialogDataModel} from "./models/password-protection-dialog-data.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {AppSettings} from "../../config";
import {finalize, takeUntil} from "rxjs/operators";
import {setFormErrorMessage} from "../../core/util";
import {DestroyService} from "../../core/services";

@Component({
	selector: 'app-password-protection-dialog',
	templateUrl: './password-protection-dialog.component.html',
	styleUrls: ['./password-protection-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordProtectionDialogComponent {

	form: FormGroup;
	hide = true;
	loading = false;

	constructor(
		private fb: FormBuilder,
		private http: HttpClient,
		public dialogRef: MatDialogRef<PasswordProtectionDialogComponent>,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
		@Inject(MAT_DIALOG_DATA) public data: PasswordProtectionDialogDataModel
	) {
		this.form = this.fb.group({
			password: ['', Validators.required],
		});
	}

	next() {
		const password = this.form.get('password')?.value;
		this.loading = true;
		this.http
			.post<boolean>(`${AppSettings.API_ENDPOINT}/user/checkAccess`, JSON.stringify({password}))
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading = false)
			)
			.subscribe(
				() => this.dialogRef.close(password),
				(err: HttpErrorResponse) => setFormErrorMessage(err, this.form)
			);
	}
}
