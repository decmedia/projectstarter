export interface AlertDialogDataModel {
	title: string;
	content: string;
	confirmButtonText?: string;
}
