import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AlertDialogDataModel} from './models/alert-dialog-data.model';

@Component({
	selector: 'app-alert-dialog',
	templateUrl: './alert-dialog.component.html',
	styleUrls: ['./alert-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertDialogComponent {
	constructor(@Inject(MAT_DIALOG_DATA) public data: AlertDialogDataModel) {
	}
}
