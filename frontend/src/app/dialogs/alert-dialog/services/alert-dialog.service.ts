import {Injectable} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {AlertDialogDataModel} from '../models/alert-dialog-data.model';
import {AlertDialogComponent} from '../alert-dialog.component';
import {Observable} from 'rxjs';
import {AsyncDialog} from '../../async-dialog';
import {fromPromise} from 'rxjs/internal-compatibility';
import {switchMap} from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class AlertDialogService extends AsyncDialog<AlertDialogComponent, AlertDialogDataModel, boolean> {
	async open(data: AlertDialogDataModel): Promise<MatDialogRef<AlertDialogComponent, boolean>> {
		const {AlertDialogModule} = await import('../alert-dialog.module');

		return this.matDialog.open(AlertDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: AlertDialogDataModel): Observable<boolean | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}
}
