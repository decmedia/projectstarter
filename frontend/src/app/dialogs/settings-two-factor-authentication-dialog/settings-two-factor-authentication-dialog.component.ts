import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {finalize, takeUntil, tap} from 'rxjs/operators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpErrorResponse} from '@angular/common/http';
import {HttpService} from "./services/http.service";
import {DestroyService} from "../../core/services";
import {setFormErrorMessage} from "../../core/util";
import {Observable} from "rxjs";
import {ITwoFaStatus} from "../../models/TwoFaStatus";
import {ITwoFaQr} from "../../models/TwoFaQr";
import {SettingsTwoFactorAuthenticationDialogDataModel} from "./models/settings-two-factor-authentication-dialog-data.model";

@Component({
	selector: 'app-edit-password-dialog',
	templateUrl: './settings-two-factor-authentication-dialog.component.html',
	styleUrls: ['./settings-two-factor-authentication-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [HttpService]
})
export class SettingsTwoFactorAuthenticationDialogComponent {

	public reset: any[] = [{}];
	step = 1;
	manual = false;
	status$: Observable<ITwoFaStatus>;
	setData$: Observable<ITwoFaQr>;
	form: FormGroup;
	loading: boolean = false;

	constructor(
		private dialogRef: MatDialogRef<SettingsTwoFactorAuthenticationDialogComponent>,
		private fb: FormBuilder,
		private httpService: HttpService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
		@Inject(MAT_DIALOG_DATA) public data: SettingsTwoFactorAuthenticationDialogDataModel
	) {
		this.form = this.fb.group({
			secret: ['', Validators.required],
			code: ['', Validators.required],
			password: [this.data?.password || ''],
		});

		this.status$ = this.httpService.twoFaAuthStatus();
		this.setData$ = this.httpService.twoFaQr().pipe(tap(data => {
			this.form.patchValue({secret: data.code})
		}));
	}

	back(): void {
		if (this.step > 1) {
			--this.step;
		} else {
			this.dialogRef.close(false);
		}
	}

	next(): void {
		if (this.step < 2) {
			++this.step;
		} else {

		}
	}

	onCodeChanged(code: string) {
		this.form.get('code')?.reset();
	}

	onCodeCompleted(code: string) {
		const data = this.form.getRawValue();
		this.loading = true;

		this.httpService
			.twoFaEnable({...data, code})
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading = false)
			)
			.subscribe(
				() => this.dialogRef.close(true),
				(err: HttpErrorResponse) => {
					this.reset[0] = {};
					setFormErrorMessage(err, this.form);
				}
			)
		;
	}
}
