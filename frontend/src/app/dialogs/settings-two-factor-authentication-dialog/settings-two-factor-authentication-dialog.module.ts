import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../components/hint-error-message";
import {QwPasswordStrengthMeterModule} from "../../components/password-strength-meter";
import {SpinnerModule} from "../../components/spinner";
import {SettingsTwoFactorAuthenticationDialogComponent} from "./settings-two-factor-authentication-dialog.component";
import {QwCodeInputModule} from "../../components/code-input";
import {QwSafeHtmlModule} from "../../core/safe-html";

@NgModule({
	declarations: [SettingsTwoFactorAuthenticationDialogComponent],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		ReactiveFormsModule,
		MatIconModule,
		HintErrorMessageModule,
		QwPasswordStrengthMeterModule,
		SpinnerModule,
		QwCodeInputModule,
		QwSafeHtmlModule,
	],
})
export class SettingsTwoFactorAuthenticationDialogModule {
	static getComponent(): typeof SettingsTwoFactorAuthenticationDialogComponent {
		return SettingsTwoFactorAuthenticationDialogComponent;
	}
}
