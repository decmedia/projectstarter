import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppSettings} from "../../../config";
import {ITwoFaStatus} from "../../../models/TwoFaStatus";
import {publishReplay, refCount} from "rxjs/operators";
import {ITwoFaQr} from "../../../models/TwoFaQr";

@Injectable()
export class HttpService {

	// @ts-ignore
	twoFaStatus$: Observable<ITwoFaStatus> | null = null;

	constructor(
		private http: HttpClient,
	) {}

	clearCache() {
		this.twoFaStatus$ = null;

		return this;
	}

	twoFaAuthStatus(): Observable<ITwoFaStatus> {
		if (!this.twoFaStatus$) {
			this.twoFaStatus$ = this.http.get<ITwoFaStatus>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`).pipe(publishReplay(), refCount());
		}

		return this.twoFaStatus$;
	}

	twoFaQr(): Observable<ITwoFaQr> {
		return this.http.get<ITwoFaQr>(`${AppSettings.API_ENDPOINT}/user/two-factor-qr`);
	}

	twoFaEnable(data: any): Observable<boolean> {
		return this.http.post<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`, JSON.stringify(data));
	}

	twoFaGenBackupCodes(data: any): Observable<string[]> {
		return this.http.post<string[]>(`${AppSettings.API_ENDPOINT}/user/two-factor/recovery-code`, JSON.stringify(data));
	}

	twoFaDisable(): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`);
	}
}
