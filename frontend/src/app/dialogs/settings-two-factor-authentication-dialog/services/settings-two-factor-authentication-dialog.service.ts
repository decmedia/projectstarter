import {Injectable} from '@angular/core';
import {SettingsTwoFactorAuthenticationDialogComponent} from '../settings-two-factor-authentication-dialog.component';
import {SettingsTwoFactorAuthenticationDialogDataModel} from '../models/settings-two-factor-authentication-dialog-data.model';
import {AsyncDialog} from '../../async-dialog';
import {MatDialogRef} from '@angular/material/dialog';
import {Observable} from "rxjs";
import {fromPromise} from "rxjs/internal-compatibility";
import {switchMap} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class SettingsTwoFactorAuthenticationDialogService extends AsyncDialog<SettingsTwoFactorAuthenticationDialogComponent, SettingsTwoFactorAuthenticationDialogDataModel, boolean> {

	async open(data: SettingsTwoFactorAuthenticationDialogDataModel): Promise<MatDialogRef<SettingsTwoFactorAuthenticationDialogComponent, boolean>> {
		const {SettingsTwoFactorAuthenticationDialogModule} = await import('../settings-two-factor-authentication-dialog.module');

		return this.matDialog.open(SettingsTwoFactorAuthenticationDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: SettingsTwoFactorAuthenticationDialogDataModel): Observable<boolean | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}
}
