import {Injectable} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';
import {PromptDialogDataModel} from '../models/prompt-dialog-data.model';
import {PromptDialogComponent} from '../prompt-dialog.component';
import {Observable} from 'rxjs';
import {AsyncDialog} from '../../async-dialog';
import {fromPromise} from 'rxjs/internal-compatibility';
import {switchMap} from 'rxjs/operators';

@Injectable({
	providedIn: 'root',
})
export class PromptDialogService extends AsyncDialog<PromptDialogComponent, PromptDialogDataModel, boolean> {
	async open(data: PromptDialogDataModel): Promise<MatDialogRef<PromptDialogComponent, boolean>> {
		const {PromptDialogModule} = await import('../prompt-dialog.module');

		return this.matDialog.open(PromptDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: PromptDialogDataModel): Observable<boolean | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}
}
