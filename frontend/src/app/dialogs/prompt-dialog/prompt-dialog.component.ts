import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {PromptDialogDataModel} from './models/prompt-dialog-data.model';
import {NgModel} from "@angular/forms";
import {KEY_CODE} from "../../constants/shortcut";

@Component({
	selector: 'app-prompt-dialog',
	templateUrl: './prompt-dialog.component.html',
	styleUrls: ['./prompt-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PromptDialogComponent {

	valueField !: NgModel;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: PromptDialogDataModel,
		private dialogRef: MatDialogRef<PromptDialogComponent>
	) {

	}

	handleKeyUp(event: KeyboardEvent) {
		if (this.valueField && [(KEY_CODE.NumpadEnter as string), KEY_CODE.Enter].includes(event.code)) {
			this.dialogRef.close(this.valueField);
		}
	}
}
