import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PromptDialogComponent} from './prompt-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from "@angular/forms";

@NgModule({
	declarations: [PromptDialogComponent],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		FormsModule,
	],
})
export class PromptDialogModule {
	static getComponent(): typeof PromptDialogComponent {
		return PromptDialogComponent;
	}
}
