export interface PromptDialogDataModel {
	title: string;
	content: string;
	confirmButtonText?: string;
}
