import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../components/hint-error-message";
import {QwPasswordStrengthMeterModule} from "../../components/password-strength-meter";
import {SpinnerModule} from "../../components/spinner";
import {GenerationAuthBackupCodeDialogComponent} from "./generation-auth-backup-code-dialog.component";
import {QwCodeInputModule} from "../../components/code-input";
import {QwSafeHtmlModule} from "../../core/safe-html";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ClipboardModule} from "@angular/cdk/clipboard";

@NgModule({
	declarations: [GenerationAuthBackupCodeDialogComponent],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		ReactiveFormsModule,
		MatIconModule,
		HintErrorMessageModule,
		QwPasswordStrengthMeterModule,
		SpinnerModule,
		QwCodeInputModule,
		QwSafeHtmlModule,
		MatTooltipModule,
		ClipboardModule,
	],
})
export class GenerationAuthBackupCodeDialogModule {
	static getComponent(): typeof GenerationAuthBackupCodeDialogComponent {
		return GenerationAuthBackupCodeDialogComponent;
	}
}
