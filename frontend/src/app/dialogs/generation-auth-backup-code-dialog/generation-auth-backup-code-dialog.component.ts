import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {mergeMap} from 'rxjs/operators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpService} from "./services/http.service";
import {downloadTxt} from "../../core/util";
import {BehaviorSubject, Observable} from "rxjs";
import {AppSettings} from "../../config";
import {GenerationAuthBackupCodeDialogDataModel} from "./models/generation-auth-backup-code-dialog-data.model";

@Component({
	selector: 'app-generation-auth-backup-code-dialog',
	templateUrl: './generation-auth-backup-code-dialog.component.html',
	styleUrls: ['./generation-auth-backup-code-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [HttpService]
})
export class GenerationAuthBackupCodeDialogComponent {

	refresh$ = new BehaviorSubject(false);
	codes$: Observable<string[]>;

	constructor(
		public dialogRef: MatDialogRef<GenerationAuthBackupCodeDialogComponent>,
		private httpService: HttpService,
		@Inject(MAT_DIALOG_DATA) public data: GenerationAuthBackupCodeDialogDataModel
	) {
		this.codes$ = this.refresh$.pipe(
			mergeMap((): Observable<string[]> => this.httpService.twoFaGenBackupCodes({password: this.data.password}))
		);
	}

	download(str: string) {
		downloadTxt(`${AppSettings.APP_NAME}_backup_code`, str);
	}
}
