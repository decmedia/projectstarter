import {Injectable} from '@angular/core';
import {GenerationAuthBackupCodeDialogComponent} from '../generation-auth-backup-code-dialog.component';
import {GenerationAuthBackupCodeDialogDataModel} from '../models/generation-auth-backup-code-dialog-data.model';
import {AsyncDialog} from '../../async-dialog';
import {MatDialogRef} from '@angular/material/dialog';
import {Observable} from "rxjs";
import {fromPromise} from "rxjs/internal-compatibility";
import {switchMap} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class GenerationAuthBackupCodeDialogService extends AsyncDialog<GenerationAuthBackupCodeDialogComponent, GenerationAuthBackupCodeDialogDataModel, string> {

	async open(data: GenerationAuthBackupCodeDialogDataModel): Promise<MatDialogRef<GenerationAuthBackupCodeDialogComponent, string>> {
		const {GenerationAuthBackupCodeDialogModule} = await import('../generation-auth-backup-code-dialog.module');

		return this.matDialog.open(GenerationAuthBackupCodeDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: GenerationAuthBackupCodeDialogDataModel): Observable<string | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}
}
