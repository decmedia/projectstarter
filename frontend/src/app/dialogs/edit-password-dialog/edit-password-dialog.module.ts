import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EditPasswordDialogComponent} from './edit-password-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../components/hint-error-message";
import {QwPasswordStrengthMeterModule} from "../../components/password-strength-meter";
import {SpinnerModule} from "../../components/spinner";

@NgModule({
	declarations: [EditPasswordDialogComponent],
	imports: [
		CommonModule,
		MatDialogModule,
		MatButtonModule,
		ReactiveFormsModule,
		MatIconModule,
		HintErrorMessageModule,
		QwPasswordStrengthMeterModule,
		SpinnerModule,
	],
})
export class EditPasswordDialogModule {
	static getComponent(): typeof EditPasswordDialogComponent {
		return EditPasswordDialogComponent;
	}
}
