import {Injectable} from '@angular/core';
import {EditPasswordDialogComponent} from '../edit-password-dialog.component';
import {EditPasswordDialogDataModel} from '../models/edit-password-dialog-data.model';
import {AsyncDialog} from '../../async-dialog';
import {MatDialogRef} from '@angular/material/dialog';
import {Observable} from "rxjs";
import {fromPromise} from "rxjs/internal-compatibility";
import {switchMap} from "rxjs/operators";

@Injectable({providedIn: 'root'})
export class EditPasswordDialogService extends AsyncDialog<EditPasswordDialogComponent, EditPasswordDialogDataModel, boolean> {

	async open(data: EditPasswordDialogDataModel): Promise<MatDialogRef<EditPasswordDialogComponent, boolean>> {
		const {EditPasswordDialogModule} = await import('../edit-password-dialog.module');

		return this.matDialog.open(EditPasswordDialogModule.getComponent(), {data, width: '500px'});
	}

	open$(data: EditPasswordDialogDataModel): Observable<boolean | undefined> {
		return fromPromise(this.open(data))
			.pipe(
				switchMap(dialogRef => dialogRef.afterClosed()),
			);
	}
}
