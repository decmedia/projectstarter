import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppSettings} from "../../../config";

@Injectable()
export class HttpService {

	constructor(
		private http: HttpClient,
	) {}

	saveUserPassword(old_password: string, new_password: string): Observable<number> {
		return this.http.put<number>(AppSettings.API_ENDPOINT + '/user/password', JSON.stringify({old_password, new_password}));
	}
}
