import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {finalize, takeUntil} from 'rxjs/operators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpErrorResponse} from '@angular/common/http';
import {EditPasswordDialogDataModel} from "./models/edit-password-dialog-data.model";
import {HttpService} from "./services/http.service";
import {DestroyService} from "../../core/services";
import {setFormErrorMessage} from "../../core/util";

@Component({
	selector: 'app-edit-password-dialog',
	templateUrl: './edit-password-dialog.component.html',
	styleUrls: ['./edit-password-dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [HttpService]
})
export class EditPasswordDialogComponent {

	hide = true;
	hide_old = true;
	form: FormGroup;
	loading: boolean = false;

	get roles(): FormArray {
		return this.form.get('roles') as FormArray;
	}

	constructor(
		public dialogRef: MatDialogRef<EditPasswordDialogComponent>,
		private fb: FormBuilder,
		private httpService: HttpService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
		@Inject(MAT_DIALOG_DATA) public data: EditPasswordDialogDataModel
	) {
		this.form = this.fb.group({
			old_password: ['', Validators.required],
			new_password: ['', Validators.required],
			password_strength: ['', Validators.compose([Validators.required, Validators.min(3)])],
		});
	}

	save(): void {
		const {old_password, new_password} = this.form.getRawValue();

		this.loading = true;
		this.httpService
			.saveUserPassword(old_password, new_password)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading = false)
			)
			.subscribe(
				() => this.dialogRef.close(true),
				(err: HttpErrorResponse) => setFormErrorMessage(err, this.form)
			);
	}

	onPasswordStrengthChanged(strength: number) {
		this.form.patchValue({password_strength: strength});
	}
}
