import {Component} from '@angular/core';

@Component({
	selector: 'app-profile-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss']
})
export class ProfileIndexComponent {

	components: (Promise<unknown>)[];

	constructor() {
		this.components = [
			import('../profile/profile.component').then(({ProfileComponent}) => ProfileComponent),
			import('../two-step-authentication/two-step-authentication.component').then(({TwoStepAuthenticationComponent}) => TwoStepAuthenticationComponent),
			import('../preferences/preferences.component').then(({PreferencesComponent}) => PreferencesComponent),
			import('../login-sessions/login-sessions.component').then(({LoginSessionsComponent}) => LoginSessionsComponent),
		];
	}
}

