import {Component, Inject, Injector, NgModule, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {filter, map, switchMap, takeUntil, tap} from "rxjs/operators";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {changeDetection, setFormErrorMessage} from "../../../../core/util";
import {IUser} from "../../../../models/User";
import {DestroyService, PubSubService, UserStateService} from "../../../../core/services";
import {HttpService} from "../http.service";
import {AlertDialogService} from "../../../../dialogs/alert-dialog/services/alert-dialog.service";
import {PasswordProtectionDialogService} from "../../../../dialogs/password-protection-dialog/services/password-protection-dialog.service";
import {CommonModule} from "@angular/common";
import {SpinnerModule} from "../../../../components/spinner";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../../../components/hint-error-message";
import {EditPasswordDialogService} from "../../../../dialogs/edit-password-dialog/services/edit-password-dialog.service";
import {ConfirmationEmailNotificationModule} from "../../../../components/confirmation-email-notification/confirmation-email-notification.module";
import {TooltipInfoModule} from "../../../../components/tooltip-info/tooltip-info.module";
import {TranslateModule} from "@ngstack/translate";
import {FormatTimeModule} from "../../../../core/directive/format-time";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatRadioModule} from "@angular/material/radio";
import {HttpErrorResponse} from "@angular/common/http";
import {FilePondModule} from "../../../../components/filepond/filepond.module";
import {FilePondOptions} from "filepond";
import {INameValue} from "../../../../models/NameValue";
import {IMedia} from "../../../../models/Media";

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss'],
	changeDetection,
})
export class ProfileComponent implements OnInit {

	form: FormGroup;
	initialData: string = '';

	editMode$ = new BehaviorSubject<boolean>(false);
	loading$ = new BehaviorSubject<boolean>(false);
	isDataChanged$ = new Subject<boolean>();
	user$: Observable<IUser>;

	pondOptions: FilePondOptions = {
		labelIdle: 'Выберите файл',
		imagePreviewHeight: 170,
		stylePanelLayout: 'compact circle',
		styleLoadIndicatorPosition: 'center bottom',
		styleProgressIndicatorPosition: 'right bottom',
		styleButtonRemoveItemPosition: 'left bottom',
		styleButtonProcessItemPosition: 'right bottom',
		allowMultiple: false,
		dropOnPage: false,
		dropOnElement: true,
		acceptedFileTypes: ['image/*'],
		allowFileTypeValidation: true,
		allowFileSizeValidation: true,
		maxFileSize: '10MB'
	};

	pondAdditionalData: INameValue[] = [
		{name: 'mediaCollection', value: 'avatar'}
	];

	pondFiles: FilePondOptions['files'] = [];
	pondFiles$: Observable<FilePondOptions['files']>;

	constructor(
		private injector: Injector,
		private fb: FormBuilder,
		private pubSubService: PubSubService,
		private httpService: HttpService,
		private snackBar: MatSnackBar,
		private userStateService: UserStateService,
		private alertDialogService: AlertDialogService,
		private passwordProtectionDialogService: PasswordProtectionDialogService,
		private editPasswordDialogService: EditPasswordDialogService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.form = this.fb.group({
			name: '',
			email_old: ['', Validators.required],
			email: ['', Validators.required],
		});

		this.pondFiles$ = this.userStateService.getCurrentAvatarId().pipe(
			map((id) => {
				return !!id ? [{
					source: String(id),
					options: {
						type: 'limbo',
					},
				}] : [];
			})
		);


		this.user$ = this.userStateService.user$.pipe(
			tap((user) => {

				this.form.patchValue({
					email_old: user.email,
					email: user.email,
					name: user.name,
				}, {emitEvent: false})

				this.initialData = JSON.stringify(this.form.getRawValue());
			})
		);
	}

	ngOnInit(): void {
		this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(val => {
			this.isDataChanged$.next(JSON.stringify(val) !== this.initialData);
		});

		this.onEvent();
	}

	pondHandleRemoveFile() {
		this.userStateService.setAvatarUrl('');
	}

	pondHandleServerResponse(media: IMedia) {
		this.userStateService.setAvatarUrl(media.preview_url || media.original_url);
	}

	changePasswordAction() {
		this.editPasswordDialogService.open$({})
			.pipe(
				filter(Boolean),
				tap(() => this.snackBar.open('Пароль изменен')),
				takeUntil(this.destroy$)
			).subscribe();
	}

	setEditMode(mode: boolean): void {
		this.editMode$.next(mode);
	}

	save() {
		const data = this.form.getRawValue();
		const newState: IUser = {
			...this.userStateService.state,
			...data,
		};

		this.loading$.next(true);
		this.passwordProtectionDialogService
			.pass$()
			.pipe(
				tap(() => this.loading$.next(false)),
				filter(password => typeof password === "string"),
				tap(() => this.loading$.next(true)),
				switchMap((password) => this.userStateService.update(newState)),
				tap(() => this.loading$.next(false)),
				takeUntil(this.destroy$)
			)
			.subscribe((user) => {
				this.snackBar.open('Изменения сохранены');
				this.setEditMode(false);

				if (data.email !== data.email_old) {
					this.emailConfirmAlertDialog(data.email);
				}
			}, (err: HttpErrorResponse) => {
				this.loading$.next(false);
				if (err.status === 422 && err.error.error) {
					setFormErrorMessage(err, this.form);
				}
			});
	}

	emailConfirmAlertDialog(email: string): void {
		this.alertDialogService
			.open$({
				title: 'Подтверждение электронного адреса',
				content: `Чтобы завершить изменение адреса электронной почты, мы отправили письмо на адрес <strong>${email}</strong>. Просто щелкните ссылку в электронном письме, чтобы завершить процесс.`,
				confirmButtonText: 'Понятно'
			})
		;
	}

	private onEvent(): void {
		this.pubSubService.on('App\\Notifications\\User\\VerifiedEmailNotification')
			.pipe(takeUntil(this.destroy$))
			.subscribe((payload) => this.userStateService.setEmail(payload.email));
	}
}

@NgModule({
	declarations: [
		ProfileComponent
	],
	imports: [
		CommonModule,
		SpinnerModule,
		MatButtonModule,
		MatIconModule,
		HintErrorMessageModule,
		ReactiveFormsModule,
		ConfirmationEmailNotificationModule,
		TooltipInfoModule,
		TranslateModule.forChild(),
		FormatTimeModule,
		MatDatepickerModule,
		MatFormFieldModule,
		MatRadioModule,
		FilePondModule,
	]
})
export class ProfileModule {
}

