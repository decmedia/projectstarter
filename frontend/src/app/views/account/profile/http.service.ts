import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {publishReplay, refCount} from 'rxjs/operators';
import {IUser} from "../../../models/User";
import {AppSettings} from "../../../config";
import {IPreferences} from "../../../models/Preferences";
import {ISession} from "../../../models/Session";
import {ITwoFaStatus} from "../../../models/TwoFaStatus";
import {ITwoFaQr} from "../../../models/TwoFaQr";


@Injectable()
export class HttpService {

	// @ts-ignore
	twoFaStatus$: Observable<ITwoFaStatus> | null = null;

	constructor(
		private http: HttpClient,
	) {}

	// кешированный запрос.
	// Вернет данные новым подписчикам без нового запроса
	/*postItem(itemData): Observable<any> {
		const observable = this.http.post<any>('/api/items', itemData).pipe(publishReplay());
		(observable as ConnectableObservable<any>).connect(); return observable;
	}*/


	clearCache() {
		this.twoFaStatus$ = null;

		return this;
	}


	// PROFILE
	saveUserProfile(user: IUser): Observable<number> {
		return this.http.put<number>(AppSettings.API_ENDPOINT + '/user/profile', JSON.stringify(user));
	}

	saveUserPassword(old_password: string, new_password: string): Observable<number> {
		return this.http.put<number>(AppSettings.API_ENDPOINT + '/user/password', JSON.stringify({old_password, new_password}));
	}

	preferences(): Observable<IPreferences> {
		return this.http.get<IPreferences>(`${AppSettings.API_ENDPOINT}/user/preferences`);
	}

	updatePreferences(data: any): Observable<IPreferences> {
		return this.http.put<IPreferences>(`${AppSettings.API_ENDPOINT}/user/preferences`, JSON.stringify(data));
	}

	resendEmailVerify(): Observable<IPreferences> {
		return this.http.post<IPreferences>(`${AppSettings.API_ENDPOINT}/user/resend-email-verify`, {});
	}

	sessions(): Observable<ISession[]> {
		return this.http.get<ISession[]>(AppSettings.API_ENDPOINT + '/user/sessions');
	}

	closeOtherSessions(): Observable<boolean> {
		return this.http.delete<boolean>(AppSettings.API_ENDPOINT + '/user/sessions');
	}

	// Two Factor Authentication...
	twoFaAuthStatus(): Observable<ITwoFaStatus> {
		if (!this.twoFaStatus$) {
			this.twoFaStatus$ = this.http.get<ITwoFaStatus>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`).pipe(publishReplay(), refCount());
		}

		return this.twoFaStatus$;
	}

	twoFaQr(): Observable<ITwoFaQr> {
		return this.http.get<ITwoFaQr>(`${AppSettings.API_ENDPOINT}/user/two-factor-qr`);
	}

	twoFaEnable(data: any): Observable<boolean> {
		return this.http.post<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`, JSON.stringify(data));
	}

	twoFaGenBackupCodes(data: any): Observable<string[]> {
		return this.http.post<string[]>(`${AppSettings.API_ENDPOINT}/user/two-factor/recovery-code`, JSON.stringify(data));
	}

	twoFaDisable(): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`);
	}

}
