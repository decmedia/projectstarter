import {Component, Inject, NgModule, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {finalize, mergeMap, takeUntil, tap} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {changeDetection} from "../../../../core/util";
import {IPreferences} from "../../../../models/Preferences";
import {DestroyService} from "../../../../core/services";
import {HttpService} from "../http.service";
import {CommonModule} from "@angular/common";
import {SpinnerModule} from "../../../../components/spinner";
import {MatButtonModule} from "@angular/material/button";
import {MatCheckboxModule} from "@angular/material/checkbox";

@Component({
	selector: 'app-preferences',
	templateUrl: './preferences.component.html',
	styleUrls: ['./preferences.component.scss'],
	changeDetection,
})
export class PreferencesComponent implements OnInit {

	refresh$ = new BehaviorSubject<boolean>(false);
	loading$ = new BehaviorSubject<boolean>(false);
	isDataChanged$ = new Subject<boolean>();
	preferences: IDataPreferences[];
	preferences$: Observable<IPreferences>;
	form: FormGroup;
	initialData: string = '';

	constructor(
		private fb: FormBuilder,
		private httpService: HttpService,
		private snackBar: MatSnackBar,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.preferences = PREFERENCES;

		this.form = this.fb.group({
			newsletter_subscribe: false,
			new_invite_subscribe: false,
			new_webinar_subscribe: false,
			desc_update_subscribe: false,
			payment_subscribe: false,
			binary_change_subscribe: false,
			leader_program_subscribe: false,
		});

		this.preferences$ = this.refresh$.pipe(
			mergeMap((): Observable<IPreferences> => this.httpService.preferences()),
			tap((preferences) => {
				for (const [key, value] of Object.entries(preferences)) {
					this.form.get(key)?.setValue(value, {emitEvent: false})
				}

				this.initialData = JSON.stringify(this.form.getRawValue());
			})
		);
	}

	undo() {
		for (const [key, value] of Object.entries(JSON.parse(this.initialData))) {
			this.form.get(key)?.setValue(value)
		}
	}

	save() {
		this.loading$.next(true);
		this.httpService
			.updatePreferences(this.form.getRawValue())
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading$.next(false))
			)
			.subscribe(() => {
				this.snackBar.open('Настройки сохранены');
				this.initialData = JSON.stringify(this.form.getRawValue());
				this.isDataChanged$.next(false);
			})
		;
	}

	selection(em: string): void {
		this.form.get(em)?.patchValue(!this.form.get(em)?.value);
	}

	ngOnInit(): void {
		this.form.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(val => {
			this.isDataChanged$.next(JSON.stringify(val) !== this.initialData);
		});
	}
}

@NgModule({
	declarations: [
		PreferencesComponent
	],
	imports: [
		CommonModule,
		SpinnerModule,
		ReactiveFormsModule,
		MatCheckboxModule,
		MatButtonModule,
	]
})
export class PreferencesModule {
}

interface IDataPreferences {
	key: string;
	title: string;
	desc: string;
}

const PREFERENCES: IDataPreferences[] = [
	{key: 'newsletter_subscribe', title: 'Новости и обновления', desc: 'Уведомления об обновлениях продукта или актуальные для вас новости'},
];
