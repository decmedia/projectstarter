import {Component, ViewChild, Inject, NgModule} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {finalize, mergeMap, takeUntil} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";
import {changeDetection} from "../../../../core/util";
import {ISession} from "../../../../models/Session";
import {DestroyService} from "../../../../core/services";
import {HttpService} from "../http.service";
import {AppSettings} from "../../../../config";
import {CommonModule} from "@angular/common";
import {SpinnerModule} from "../../../../components/spinner";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";

@Component({
	selector: 'app-login-sessions',
	templateUrl: './login-sessions.component.html',
	styleUrls: ['./login-sessions.component.scss'],
	changeDetection,
})
export class LoginSessionsComponent {

	refresh$ = new BehaviorSubject<boolean>(false);
	loading$ = new BehaviorSubject<boolean>(false);
	loadingSessionDestroy$ = new BehaviorSubject<boolean>(false);
	companyName: string = AppSettings.APP_NAME;
	displayedColumns = ['country', 'device', 'ip', 'time', 'current'];
	dataRows: Observable<ISession[]>;

	constructor(
		private httpService: HttpService,
		private snackBar: MatSnackBar,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.dataRows = this.refresh$.pipe(
			mergeMap((): Observable<ISession[]> => this.httpService.sessions().pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading$.next(false))
			)),
		);
	}

	closeOtherSessions() {
		this.loadingSessionDestroy$.next(true);
		this.httpService
			.closeOtherSessions()
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loadingSessionDestroy$.next(false))
			)
			.subscribe(() => {
				this.refresh$.next(true);
				this.snackBar.open('Все остальные сеансы успешно завершены');
			})
		;
	}
}

@NgModule({
	declarations: [
		LoginSessionsComponent
	],
	imports: [
		CommonModule,
		SpinnerModule,
		MatTableModule,
		MatButtonModule,
	]
})
export class LoginSessionsModule {
}
