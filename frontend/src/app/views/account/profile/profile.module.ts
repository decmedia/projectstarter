import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfileIndexComponent} from './index/index.component';
import {RouterModule} from "@angular/router";
import {generateRoutes} from "../../../core/util";
import {HttpService} from "./http.service";
import {SpinnerModule} from "../../../components/spinner";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {HintErrorMessageModule} from "../../../components/hint-error-message";
import {ReactiveFormsModule} from "@angular/forms";
import {ConfirmationEmailNotificationModule} from "../../../components/confirmation-email-notification/confirmation-email-notification.module";
import {TooltipInfoModule} from "../../../components/tooltip-info/tooltip-info.module";
import {TranslateModule} from "@ngstack/translate";
import {FormatTimeModule} from "../../../core/directive/format-time";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {LazyComponentResolverModule} from "../../../core/components/lazy-component-resolver/lazy-component-resolver.module";

@NgModule({
	declarations: [
		ProfileIndexComponent,
	],
    imports: [
        CommonModule,
        RouterModule.forChild(
            generateRoutes(ProfileIndexComponent)
        ),
        SpinnerModule,
        MatButtonModule,
        MatIconModule,
        HintErrorMessageModule,
        ReactiveFormsModule,
        ConfirmationEmailNotificationModule,
        TooltipInfoModule,
        TranslateModule.forChild(),
        FormatTimeModule,
        MatDatepickerModule,
        MatFormFieldModule,
        LazyComponentResolverModule,
    ],
	exports: [
		ProfileIndexComponent
	],
	providers: [
		HttpService
	],
})
export class ProfileModule {
}
