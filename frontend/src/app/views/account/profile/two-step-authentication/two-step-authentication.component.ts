import {Component, Inject, Injector, NgModule} from '@angular/core';
import {mergeMap} from "rxjs/operators";
import {BehaviorSubject, Observable} from "rxjs";
import {changeDetection} from "../../../../core/util";
import {ITwoFaStatus} from "../../../../models/TwoFaStatus";
import {DestroyService} from "../../../../core/services";
import {HttpService} from "../http.service";
import {SettingsTwoFactorAuthenticationDialogService} from "../../../../dialogs/settings-two-factor-authentication-dialog/services/settings-two-factor-authentication-dialog.service";
import {PasswordProtectionDialogService} from "../../../../dialogs/password-protection-dialog/services/password-protection-dialog.service";
import {AlertDialogService} from "../../../../dialogs/alert-dialog/services/alert-dialog.service";
import {ConfirmationDialogService} from "../../../../dialogs/confirmation-dialog/services/confirmation-dialog.service";
import {DisableTwoFaAuthenticatorAction} from "../../../../action-definitions/jobs/disable-twofa-authenticator-action/disable-twofa-authenticator-action";
import {ContextMenuActionModel} from "../../../../components/context-menu/models/context-menu-action.model";
import {DisableTwoFaAuthenticatorActionParams} from "../../../../action-definitions/jobs/disable-twofa-authenticator-action/disable-twofa-authenticator-action-params";
import {GenerationAuthBackupCodeAction} from "../../../../action-definitions/jobs/generation-auth-backup-code-action/generation-auth-backup-code-action";
import {GenerationAuthBackupCodeActionParams} from "../../../../action-definitions/jobs/generation-auth-backup-code-action/generation-auth-backup-code-action-params";
import {EnableTwoFaAuthenticatorAction} from "../../../../action-definitions/jobs/enable-twofa-authenticator-action/enable-twofa-authenticator-action";
import {EnableTwoFaAuthenticatorActionParams} from "../../../../action-definitions/jobs/enable-twofa-authenticator-action/enable-twofa-authenticator-action-params";
import {CommonModule} from "@angular/common";
import {SpinnerModule} from "../../../../components/spinner";
import {MatButtonModule} from "@angular/material/button";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";

@Component({
	selector: 'app-two-step-authentication',
	templateUrl: './two-step-authentication.component.html',
	styleUrls: ['./two-step-authentication.component.scss'],
	changeDetection
})
export class TwoStepAuthenticationComponent {

	loading = new BehaviorSubject(false);
	statusRefresh$ = new BehaviorSubject(false);
	status$: Observable<ITwoFaStatus>;

	disableTwoFaAuthenticatorAction: ContextMenuActionModel<DisableTwoFaAuthenticatorActionParams>;
	generationAuthBackupCodeAction: ContextMenuActionModel<GenerationAuthBackupCodeActionParams>;
	enableTwoFaAuthenticatorAction: ContextMenuActionModel<EnableTwoFaAuthenticatorActionParams>;

	constructor(
		private injector: Injector,
		private httpService: HttpService,
		private passwordProtectionDialogService: PasswordProtectionDialogService,
		private alertDialogService: AlertDialogService,
		private confirmationDialogService: ConfirmationDialogService,
		private settingsTwoFactorAuthenticationDialogService: SettingsTwoFactorAuthenticationDialogService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.status$ = this.statusRefresh$.pipe(
			mergeMap((): Observable<ITwoFaStatus> => this.httpService.twoFaAuthStatus())
		);

		this.generationAuthBackupCodeAction = this.injector.get(GenerationAuthBackupCodeAction).build();
		this.enableTwoFaAuthenticatorAction = this.injector.get(EnableTwoFaAuthenticatorAction).build({
			onSuccess: () => this.reload()
		});
		this.disableTwoFaAuthenticatorAction = this.injector.get(DisableTwoFaAuthenticatorAction).build({
			onSuccess: () => this.reload()
		});
	}

	soon() {
		this.alertDialogService.open$({
			title: 'В настоящее время недоступно',
			content: 'Запрошенное действие в настоящий момент недоступно. Это может быть связано с его доработкой, либо с ограничениями учетной записи. Пожалуйста, попробуйте позже',
			confirmButtonText: 'Понятно',
		});
	}

	reload(): void {
		this.httpService.clearCache();
		this.statusRefresh$.next(true)
	}
}

@NgModule({
	declarations: [
		TwoStepAuthenticationComponent
	],
	imports: [
		CommonModule,
		SpinnerModule,
		MatButtonModule,
		MatMenuModule,
		MatIconModule,
	]
})
export class TwoStepAuthenticationModule {
}
