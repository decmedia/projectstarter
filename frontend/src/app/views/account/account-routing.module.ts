import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexAccountComponent} from './index/index.component';
import {TokenGuard} from "../../core/guard/token.guard";

export const routes: Routes = [
	{
		path: '',
		component: IndexAccountComponent,
		canActivate: [TokenGuard],
		children: [
			{
				path: 'profile',
				loadChildren: () => import(/* webpackChunkName: "profile-module" */'./profile/profile.module').then(m => m.ProfileModule),
				pathMatch: 'prefix'
			},
			{
				path: '',
				loadChildren: () => import(/* webpackChunkName: "dashboard-module" */'./dashboard/dashboard.module').then(m => m.DashboardModule),
				pathMatch: 'prefix'
			},

		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AccountRoutingModule {
}
