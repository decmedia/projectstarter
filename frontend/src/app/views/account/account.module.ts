import {NgModule} from '@angular/core';
import {AccountRoutingModule} from './account-routing.module';
import {MatMenuModule} from '@angular/material/menu';
import {CommonModule} from '@angular/common';
import {MatTooltipModule} from '@angular/material/tooltip';
import {IndexAccountComponent} from './index/index.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from "@angular/material/button";
import {TokenGuard} from "../../core/guard/token.guard";
import {MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {AsideMenuItemModule} from "../../components/aside-menu-item/aside-menu-item.module";
import {AvatarModule} from "../../components/avatar/avatar.module";
import {NzSkeletonModule} from 'ng-zorro-antd/skeleton';

@NgModule({
	declarations: [
		IndexAccountComponent,
	],
	imports: [
		AccountRoutingModule,
		MatMenuModule,
		CommonModule,
		MatTooltipModule,
		MatSidenavModule,
		MatIconModule,
		MatButtonModule,
		MatDialogModule,
		ReactiveFormsModule,
		AsideMenuItemModule,
		AvatarModule,
		NzSkeletonModule,
	],
	exports: [],
	providers: [TokenGuard]
})
export class AccountModule {
}
