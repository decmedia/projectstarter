import {Component, Inject, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {changeDetection} from "../../../core/util";
import {IUser} from "../../../models/User";
import {DestroyService, PubSubService} from "../../../core/services";
import {UserStateService} from "../../../core/services/user/user-state.service";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
	changeDetection,
})
export class DashboardComponent implements OnInit {

	user$: Observable<IUser | null>;

	constructor(
		private pubSubService: PubSubService,
		private userStateService: UserStateService,
		private snackBar: MatSnackBar,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.user$ = this.userStateService.user$
	}

	ngOnInit(): void {
	}
}
