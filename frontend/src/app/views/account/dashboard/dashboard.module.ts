import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {generateRoutes} from "../../../core/util";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
	declarations: [
		DashboardComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild(
			generateRoutes(DashboardComponent)
		),
		MatButtonModule
	],
	exports: [
		DashboardComponent
	],
	providers: [],
})
export class DashboardModule {
}
