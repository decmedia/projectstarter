import {AfterViewInit, Component, Inject, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {BreakpointObserver} from '@angular/cdk/layout';
import {BehaviorSubject, Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {changeDetection} from "../../../core/util";
import {IUser} from "../../../models/User";
import {DestroyService, PubSubService, TokenService} from "../../../core/services";
import {AppSettings} from "../../../config";
import {IEvent} from "../../../models/Event";
import {UserStateService} from "../../../core/services";

declare var window: any;

@Component({
	selector: 'app-index',
	templateUrl: './index.component.html',
	styleUrls: ['./index.component.scss'],
	changeDetection
})
export class IndexAccountComponent {

	@ViewChild('sidenav') sidenav: MatSidenav | undefined;

	reason = '';
	mode = 'side';
	isHandset: boolean = false;

	sidebarIsOpened: boolean = false;
	loading$: BehaviorSubject<boolean>;
	user$: Observable<IUser | null>;

	private readonly channel = 'User.';
	private ws: any;

	constructor(
		private router: Router,
		private breakpointObserver: BreakpointObserver,
		private pubSubService: PubSubService,
		private userStateService: UserStateService,
		private tokenService: TokenService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.loading$ = this.userStateService.loading$;
		this.user$ = this.userStateService.user$.pipe(
			tap((user) => this.wsSubscribe(user))
		);
	}

	toggleSidebar() {
		this.sidebarIsOpened = !this.sidebarIsOpened;
	}

	close(reason: string): void {
		if (this.isHandset) {
			this.reason = reason;
			this.sidenav?.close();
		}
	}

	logout() {
		this.tokenService.logout();
		window.location = "/";
	}

	private wsSubscribe(user: IUser | null) {

		if (!this.ws && user && AppSettings.WS_ENABLED) {

				this.ws = new window.Echo({
					broadcaster: 'pusher',
					key: AppSettings.PUSHER_APP_KEY,
					wsHost: AppSettings.WS_HOST,
					wsPort: AppSettings.WS_PORT,
					forceTLS: false,
					disableStats: true,
					authEndpoint: AppSettings.BROADCASTING_AUTH,
					auth: {headers: {'Authorization': 'Bearer ' + this.tokenService.token }}
				});

				const channel = this.channel + user.id;

				console.log(`Listen: ${channel}`);

				this.ws.private(channel)
					.notification((notification: IEvent<any>) => {
						console.log('Notification', notification);
						this.pubSubService.publish(notification.type, notification.payload);
					});
		}
	}
}
