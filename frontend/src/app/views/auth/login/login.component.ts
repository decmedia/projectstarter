import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {finalize, takeUntil} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {BehaviorSubject} from "rxjs";
import {changeDetection, setFormErrorMessage} from "../../../core/util";
import {DestroyService, PubSubService, TokenService} from "../../../core/services";
import {HttpService} from "../http.service";

enum STEPS {
	email = 1,
	code = 2,
	recovery_code = 3
}

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	changeDetection
})
export class LoginComponent {

	hide = true;
	resetCodeInputComponent: any[] = [{}];
	step$ = new BehaviorSubject<number>(STEPS.email);
	returnUrl: string;
	loading: boolean = false;
	form: FormGroup;
	STEPS = STEPS;

	constructor(
		private route: ActivatedRoute,
		private httpService: HttpService,
		private router: Router,
		private fb: FormBuilder,
		private pubSubService: PubSubService,
		private tokenService: TokenService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/ac';

		this.form = this.fb.group({
			email: ['', Validators.required],
			password: ['', Validators.required],
			code: '',
			recovery_code: ''
		});
	}

	toggleStep(num: number): void {
		this.step$.next(num);

		if (num === STEPS.email) {
			this.form.get('code')?.reset();
			this.form.get('recovery_code')?.reset();
		}

		if (num === STEPS.code) {
			this.form.get('recovery_code')?.reset();
		}

		if (num === STEPS.recovery_code) {
			this.form.get('code')?.reset();
		}
	}

	next() {
		const data = this.form.getRawValue();
		this.loading = true;
		this.httpService.login(data)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.subscribe((res) => {
					this.tokenService.setToken(res.token);
					this.router.navigateByUrl(this.returnUrl);
				},
				(err: HttpErrorResponse) => {

					if (err.status === 500 && err.error.error) {
						if (err.error.error?.code && Number(err.error.error.code) === 2001) {
							this.toggleStep(STEPS.code);
						}
					}

					if (err.status === 422 && err.error.error) {
						this.resetCodeInputComponent[0] = {};
						setFormErrorMessage(err, this.form);
					}
				});
	}

	onCodeChanged() {
		this.form.get('code')?.reset();
	}

	onCodeCompleted(code: string) {
		this.form.patchValue({code})
		this.next();
	}
}
