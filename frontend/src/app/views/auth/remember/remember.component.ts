import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BehaviorSubject, Subject, timer} from "rxjs";
import {finalize, scan, switchMap, takeUntil, takeWhile} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {DestroyService} from "../../../core/services";
import {changeDetection, setFormErrorMessage} from "../../../core/util";
import {HttpService} from "../http.service";

@Component({
	selector: 'app-remember',
	templateUrl: './remember.component.html',
	styleUrls: ['./remember.component.scss'],
	changeDetection,
})
export class RememberComponent {

	hide = true;
	timer$: any;
	initTimer$ = timer(0, 1000);
	loading$ = new Subject<boolean>();
	codeHasBeenSend$ = new Subject<boolean>();
	currentStep = 1;
	readonly reloadTimer$ = new BehaviorSubject(false);

	public iteration = 0;
	public contactForm: FormGroup;
	public codeForm: FormGroup;
	public passwordForm: FormGroup;

	constructor(
		private fb: FormBuilder,
		private httpService: HttpService,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.contactForm = this.fb.group({
			contact: ['', Validators.required],
		});
		this.codeForm = this.fb.group({
			request_id: ['', Validators.required],
			code: ['', Validators.required],
		});
		this.passwordForm = this.fb.group({
			request_id: ['', Validators.required],
			code: ['', Validators.required],
			new_password: ['', Validators.required],
			new_password_confirmation: ['', Validators.required],
		});
	}

	resendCode(): void {
		this.rememberRequest();
		this.startTimer(120);
	}

	/**
	 * Пользователь что забыл доступ в свою учетную запись, может восстановить доступ, указав свой контакт
	 * Система произведет поиск, и если совпадение будет найдено, отправит ему письмо и смс с кодом подтверждения
	 * формы создания нового пароля, и ссылкой на раздел. Введя код в форму, можно будет создать новый пароль
	 *
	 * Шаг: 1
	 * Проверка введенного контакта и отправка на него код, если что-то будет найдено
	 * Если пришел список contact_list показываем его на выбор. Возможно пользователь захочет изменить контакт получения кода
	 */
	rememberRequest(): void {
		this.loading$.next(true);

		this.sleep(this.iteration * 1000).then(() => {
			this.httpService
				.rememberRequest(this.contactForm.get('contact')?.value)
				.pipe(
					takeUntil(this.destroy$),
					finalize(() => this.loading$.next(false))
				)
				.subscribe(
					id => {
						this.toggleStep(2);
						this.codeForm.patchValue({request_id: id});
						this.passwordForm.patchValue({request_id: id});

						// Запускаем таймер и показываем об этом сообщение спустя 5 сек.
						// чтобы юзер успел осмыслить предшествующее этому смену формы (шага)
						this.startTimer(120);
						setTimeout(() => this.codeHasBeenSend$.next(true), 5000);
					},
					(err: HttpErrorResponse) => setFormErrorMessage(err, this.contactForm)
				);
		});
	}

	// Шаг 2:
	// Проверка корректности введенного кода подтверждения
	rememberConfirm(): void {
		const data = this.codeForm.getRawValue();
		this.loading$.next(true);
		this.httpService
			.rememberConfirm(data.request_id, data.code)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading$.next(false))
			)
			.subscribe(
				result => {
					this.passwordForm.patchValue({code: this.codeForm?.get('code')?.value});
					this.toggleStep(3);
				},
				(err: HttpErrorResponse) => setFormErrorMessage(err, this.codeForm)
			);
	}

	// Шаг 3:
	// Создание нового пароля входа
	rememberSetPassword(): void {
		const data = this.passwordForm.getRawValue();
		this.loading$.next(true);
		this.httpService
			.rememberSetPassword(data.request_id, data.code, data.new_password, data.new_password_confirmation)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading$.next(false))
			)
			.subscribe(
				() => this.goLogin(this.contactForm?.get('contact')?.value, this.passwordForm?.get('new_password')?.value),
				(err: HttpErrorResponse) => setFormErrorMessage(err, this.passwordForm)
			);
	}

	// Авторизация пользователя по новому паролю
	private goLogin(email: string, password: string): void {
		this.loading$.next(true);
		this.httpService
			.login({email, password})
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => this.loading$.next(false))
			)
			.subscribe(() => this.toggleStep(4))
		;
	}
	private sleep(time: number): any {
		return new Promise(resolve => setTimeout(resolve, time));
	}

	private toggleStep(step: number): void {
		this.currentStep = step;
	}

	private startTimer(seed: number): void {
		this.timer$ = this.reloadTimer$.pipe(
			switchMap((e) => {
				return this.initTimer$.pipe(
					scan(acc => --acc, seed),
					takeWhile(x => x >= 0),
					finalize(() => console.log('fin!'))
				);
			})
		);
	}
}
