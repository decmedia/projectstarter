import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {RememberComponent} from './remember.component';
import {HintErrorMessageModule} from "../../../components/hint-error-message";
import {generateRoutes} from "../../../core/util";
import {SpinnerModule} from "../../../components/spinner";

@NgModule({
	declarations: [
		RememberComponent
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatSnackBarModule,
		RouterModule.forChild(
			generateRoutes(RememberComponent)
		),
		MatIconModule,
		HintErrorMessageModule,
		SpinnerModule
	],
	providers: []
})
export class AuthRememberModule {
}
