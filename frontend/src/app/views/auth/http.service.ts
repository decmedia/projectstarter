import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AppSettings} from "../../config";
import {IUser} from "../../models/User";

@Injectable()
export class HttpService {

	constructor(private http: HttpClient,) {}

	login(data: any): Observable<IResponseToken> {
		return this.http.post<IResponseToken>(`${AppSettings.API_ENDPOINT}/auth`, JSON.stringify(data));
	}

	register(data: any): Observable<IResponseToken> {
		return this.http
			.post<IResponseToken>(`${AppSettings.API_ENDPOINT}/auth/register`, JSON.stringify(data));
	}

	/**
	 * Сброс пароля
	 * 3 этапа:
	 * - запрос контакта/логина и поиск по нему
	 * - ввод кода подтверждения что пришел на контакт из профиля пользователя
	 * (либо выбор контакта на который надо отправить код, если их несколько и он не был указан явно)
	 * - создание нового пароля
	 */

	// Шаг 1: Проверка переданного контакта на существование
	rememberRequest(contact: string): Observable<number> {
		return this.http.post<number>(AppSettings.API_ENDPOINT + '/auth/rememberRequest', JSON.stringify({contact}));
	}

	// Шаг 2: Проверка переданного кода на корректность
	rememberConfirm(request_id: string, code: string): Observable<boolean> {
		return this.http.post<boolean>(AppSettings.API_ENDPOINT + '/auth/rememberConfirm', JSON.stringify({request_id, code}));
	}

	// Шаг 3: Создание нового пароля
	rememberSetPassword(request_id: string, code: string, new_password: string, new_password_confirmation: string): Observable<string> {
		return this.http.post<string>(`${AppSettings.API_ENDPOINT}/auth/rememberSetPassword`, JSON.stringify(
			{request_id, code, new_password, new_password_confirmation}
		));
	}
}

interface IResponseToken {
	token: string;
	user: IUser;
}
