import {NgModule} from '@angular/core';
import {AuthComponent} from "./auth.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {LoginComponent} from './login/login.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {QwCodeInputModule} from "../../components/code-input";
import {HintErrorMessageModule} from "../../components/hint-error-message";
import {SpinnerModule} from "../../components/spinner";
import {HttpService} from "./http.service";

const routes: Routes = [
	{
		path: '',
		component: AuthComponent,
		children: [
			{path: '', component: LoginComponent},
			{
				path: 'register',
				loadChildren: () => import(/* webpackChunkName: "register-module" */'./register/register.module').then(m => m.AuthRegisterModule),
				pathMatch: 'prefix'
			},
			{
				path: 'remember',
				loadChildren: () => import(/* webpackChunkName: "remember-module" */'./remember/remember.module').then(m => m.AuthRememberModule),
				pathMatch: 'prefix'
			},
			{
				path: '**',
				redirectTo: ''
			}
		],
	},
];

@NgModule({
	declarations: [
		AuthComponent,
		LoginComponent,
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		MatButtonModule,
		MatProgressBarModule,
		MatSnackBarModule,
		RouterModule.forChild(routes),
		MatIconModule,
		MatCheckboxModule,
		QwCodeInputModule,
		HintErrorMessageModule,
		SpinnerModule,
	],
	providers: [
		HttpService
	]
})
export class AuthModule {
}
