import {Component} from '@angular/core';
import {changeDetection} from "../../core/util";

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.scss'],
	changeDetection
})
export class AuthComponent {
}
