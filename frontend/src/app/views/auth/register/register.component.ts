import {ChangeDetectorRef, Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {finalize, takeUntil} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {changeDetection} from "../../../core/util";
import {DestroyService} from "../../../core/services";
import {HttpService} from "../http.service";

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
	changeDetection
})
export class RegisterComponent {

	hide = true;
	returnUrl: string;
	loading: boolean = false;
	form: FormGroup;

	constructor(
		protected cd: ChangeDetectorRef,
		private route: ActivatedRoute,
		private httpService: HttpService,
		private router: Router,
		private fb: FormBuilder,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {
		this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/ac';

		this.form = this.fb.group({
			name: ['', Validators.required],
			email: ['', Validators.required],
			password: ['', Validators.required],
			not_subscribe: false
		});
	}

	next() {
		const data = this.form.getRawValue();
		this.loading = true;
		this.httpService.register(data)
			.pipe(
				takeUntil(this.destroy$),
				finalize(() => (this.loading = false))
			)
			.subscribe(() => this.router.navigateByUrl(this.returnUrl),
				(err: HttpErrorResponse) => {

					if (err.status === 422 && err.error.error) {
						Object.keys(err.error.error.fields).map(key => {
							this.form.get(key)?.setErrors({incorrect: true, message: err.error.error.fields[key].join()});
						});
					}

					this.loading = false
					this.cd.detectChanges();
				});
	}
}
