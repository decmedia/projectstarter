import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {RegisterComponent} from './register.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {generateRoutes} from "../../../core/util";
import {SpinnerModule} from "../../../components/spinner";

@NgModule({
	declarations: [
		RegisterComponent
	],
	imports: [
		CommonModule,
		ReactiveFormsModule,
		MatButtonModule,
		RouterModule.forChild(
			generateRoutes(RegisterComponent)
		),
		MatIconModule,
		MatCheckboxModule,
		SpinnerModule,
	],
	providers: []
})
export class AuthRegisterModule {
}
