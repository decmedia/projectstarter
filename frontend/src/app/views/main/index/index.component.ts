import {Component} from '@angular/core';
import {changeDetection} from "../../../core/util";

@Component({
    selector: 'app-main-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
	changeDetection
})
export class MainIndexComponent {
}
