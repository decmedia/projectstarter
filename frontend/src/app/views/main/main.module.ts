import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {generateRoutes} from "../../core/util";
import {MainIndexComponent} from "./index/index.component";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
	declarations: [
		MainIndexComponent
	],
	imports: [
		CommonModule,
		RouterModule.forChild(
			generateRoutes(MainIndexComponent)
		),
		MatButtonModule,
	],
	providers: []
})
export class MainIndexModule {
}
