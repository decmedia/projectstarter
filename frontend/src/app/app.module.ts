import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";
import {DestroyService, PubSubService, TokenService} from "./core/services";
import {WindowRefService} from "./core/services/window.service";
import {TranslateModule, TranslateService} from "@ngstack/translate";
import {MatPaginatorIntl} from "@angular/material/paginator";
import {CustomMatPaginatorIntl} from "./config/material/mat-paginator-intl";
import {AuthInterceptor} from "./core/guard/auth-interceptor.service";
import {AppSettings} from "./config";
import {MatDateFnsDateModule} from "./core/adapters/mat-datefns-date-adapter-master/mat-datefns-date-adapter.module";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import {MAT_DATEFNS_LOCALES} from "./core/adapters/mat-datefns-date-adapter-master/mat-datefns-locales";
import {ru} from "date-fns/locale";

// https://github.com/ngstack/translate
export function setupTranslateService(service: TranslateService) {
	return () => service.load();
}

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		MatSnackBarModule,
		HttpClientModule,
		MatIconModule,
		MatDateFnsDateModule,
		TranslateModule.forRoot({
			supportedLangs: ['ru', 'en'],
			disableCache: true,
			activeLang: localStorage.getItem(AppSettings.APP_PREFIX + 'lang') || 'ru',
		}),
	],
	providers: [
		DestroyService,
		TokenService,
		PubSubService,
		WindowRefService,
		TranslateService,
		{
			provide: APP_INITIALIZER,
			useFactory: setupTranslateService,
			deps: [TranslateService],
			multi: true,
		},
		{provide: LOCALE_ID, useValue: 'ru'},
		{provide: MatPaginatorIntl, useClass: CustomMatPaginatorIntl},
		{provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
		{
			provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
			useValue: {duration: 5000, horizontalPosition: 'center', verticalPosition: 'bottom'}
		},
		{provide: MAT_DATE_LOCALE, useValue: "ru",},
		{provide: MAT_DATEFNS_LOCALES, useValue: [ru]}
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
