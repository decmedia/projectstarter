import {HttpErrorResponse} from "@angular/common/http";
import {FormGroup} from "@angular/forms";

export function setFormErrorMessage(err: HttpErrorResponse, form: FormGroup): FormGroup {
	if (err.status === 422 && err.error.error) {
		Object.keys(err.error.error.fields).map(key => {
			form.get(key)?.setErrors({incorrect: true, message: err.error.error.fields[key].join()});
		});
	}

	return form;
}
