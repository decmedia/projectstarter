import {SizeRubber, SizeXS, SizeXXL} from "../types/size";

const SIZES: ReadonlyArray<SizeXS | SizeXXL | SizeRubber> = ['xs', 's', 'm', 'l', 'xl', 'xxl', 'inherit'];

/**
 * Compares size constants to determine if first size is bigger than the second
 *
 * @param size size that we need to compare
 * @param biggerThanSize size to compare with, 's' by default
 */
export function sizeBigger(
	size: SizeXS | SizeXXL | SizeRubber,
	biggerThanSize: SizeXS | SizeXXL = 's',
): boolean {
	return SIZES.indexOf(size) > SIZES.indexOf(biggerThanSize);
}
