export function random(len: number = 17): string {
	return Math.random().toString(36).substring(2, len);
}
