'use strict';

/**
 * var good_json = '{"a":"obja","b":[0,1,2],"c":{"d":"some object"}}';
 * var bad_json = '{"a":"obja""b":[0,1,2],"c":{"d":"some object"}}';
 * var str_number = '121212';
 *
 * console.log(isJSON(good_json)); // true
 * console.log(isJSON(bad_json)); // false
 * console.log(isJSON(str_number)); // false
 *
 * check is an object
 *
 * var object = {a: 12, b: [1,2,3]};
 *
 * console.log(isJSON(object, true)); // true
 *
 * can use isJSON.strict (uses try/catch) if wants something more robust
 *
 * console.log(isJSON.strict('{\n "config": 123,\n "test": "abcde" \n}')); // true
 */
export function isJSON(str: any, pass_object = false): boolean {
	if (pass_object && isObject(str)) { return true; }

	if (!isString(str)) { return false; }

	str = str.replace(/\s/g, '').replace(/\n|\r/, '');

	if (/^\{(.*?)\}$/.test(str)) {
		return /"(.*?)":(.*?)/g.test(str);
	}

	if (/^\[(.*?)\]$/.test(str)) {
		return str.replace(/^\[/, '')
			.replace(/\]$/, '')
			.replace(/},{/g, '}\n{')
			.split(/\n/)
			.map(function (s: any) {
				// @ts-ignore
				return isJSON(s);
			})
			.reduce(function (prev: any, curr: any) { return !!curr; });
	}

	return false;
}

isJSON.strict = strict;

function strict (str: string) {
	if (isObject(str)) {
		return true;
	}

	try {
		return JSON.parse(str) && true;
	} catch (ex) {
		return false;
	}
}

function isString (x: any) {
	return Object.prototype.toString.call(x) === '[object String]';
}

function isObject (obj: any) {
	return Object.prototype.toString.call(obj) === '[object Object]';
}
