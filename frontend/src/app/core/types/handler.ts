export type AppHandler<T, G> = (item: T) => G;
export type AppBooleanHandler<T> = AppHandler<T, boolean>;
export type AppStringHandler<T> = AppHandler<T, string>;
export type AppNumberHandler<T> = AppHandler<T, number>;
