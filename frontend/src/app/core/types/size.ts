/**
 * Various sizes for components
 *
 * 'xs' - extra small
 * 's' - small
 * 'm' - medium (typically default)
 * 'l' - large
 * 'xl' - extra large
 * 'xxl' - extra extra large
 * 'rubber' - inherit
 */

export type SizeRubber = 'inherit';

export type SizeM = 'm';

export type SizeS = 's' | SizeM;

export type SizeL = SizeM | 'l';

export type SizeXS = 'xs' | SizeS;

export type SizeXL = SizeL | 'xl';

export type SizeXXL = SizeXL | 'xxl';
