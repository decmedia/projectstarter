import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from "../../../models/User";
import {AppSettings} from "../../../config";
import {IPreferences} from "../../../models/Preferences";

@Injectable({
	providedIn: 'root',
})
export class UserBackendService {

	constructor(private http: HttpClient) {}

	user(): Observable<IUser> {
		return this.http.get<IUser>(`${AppSettings.API_ENDPOINT}/user/view`);
	}

	resendEmailVerify(): Observable<IPreferences> {
		return this.http.post<IPreferences>(`${AppSettings.API_ENDPOINT}/user/resend-email-verify`, {});
	}

	update(user: IUser): Observable<IUser> {
		return this.http.put<IUser>(`${AppSettings.API_ENDPOINT}/user/profile`, JSON.stringify(user));
	}

	restoreAvatar(): Observable<number> {
		return this.http.get<number>(`${AppSettings.API_ENDPOINT}/user/restore-avatar`);
	}
}
