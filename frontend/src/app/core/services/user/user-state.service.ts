import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {debug, RxJsLoggingLevel} from "../../util/debug";
import {StateService} from "../state.service";
import {IUser} from "../../../models/User";
import {UserBackendService} from "./user-backend.service";
import {filter, finalize, tap} from "rxjs/operators";
import {IPreferences} from "../../../models/Preferences";

@Injectable({
	providedIn: 'root'
})
export class UserStateService extends StateService<IUser | any> {

	user$: Observable<IUser> = this.state$.asObservable().pipe(
		filter<IUser>(Boolean),
		debug(RxJsLoggingLevel.DEBUG, 'Loading user from UserStateService')
	);

	constructor(
		private userBackendService: UserBackendService,
	) {
		super(null);

		this.loadInitialData();
	}

	private loadInitialData() {
		this.loading$.next(true);
		this.userBackendService.user()
			.pipe(
				debug(RxJsLoggingLevel.DEBUG, 'Set state UserStateService'),
				finalize(() => this.loading$.next(false))
			)
			.subscribe(user => this.setState(user))
		;
	}

	resendEmailVerify(): Observable<IPreferences> {
		return this.userBackendService.resendEmailVerify();
	}

	setEmail(email: string): void {
		return this.setState({email});
	}

	update(user: IUser) {
		return this.userBackendService.update(user).pipe(
			tap((user) => this.setState(user))
		);
	}

	getCurrentAvatarId(): Observable<number> {
		return this.userBackendService.restoreAvatar().pipe(

			tap((res) => console.log(res))
		);
	}

	setAvatarUrl(url: string): void {
		this.setState({avatar: url});
	}
}
