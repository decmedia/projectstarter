# Cookie Service

Angular 12 IVY Ready service for cookies. Originally based on
the [ng2-cookies](https://www.npmjs.com/package/ng2-cookies) library.

The experienced team behind [Studytube](https://www.studytube.nl/) will take care of our cookie service from now on.

Add the cookie service to your `app.module.ts` as a provider:

```typescript
import { CookieService } from './cookie-service';

@NgModule({
  ...
    providers:
[CookieService],
...
})

export class AppModule {
}
```

Then, import and inject it into a constructor:

```typescript
constructor(private cookieService: CookieService) {
  this.cookieService.set('Test', 'Hello World');
  this.cookieValue = this.cookieService.get('Test');
}
```

That's it!

# Methods

### check( name: string ): boolean;

```typescript
const cookieExists: boolean = cookieService.check('test');
```

Checks if a cookie with the given`name` can be accessed or found.

### get( name: string ): string;

```typescript
const value: string = cookieService.get('test');
```

Gets the value of the cookie with the specified `name`.

### getAll(): {};

```typescript
const allCookies: {} = cookieService.getAll();
```

Returns a map of key-value pairs for cookies that can be accessed.

### set( name: string, value: string, expires?: number | Date, path?: string, domain?: string, secure?: boolean, sameSite?: 'Lax' | 'Strict' | 'None' ): void;

### set( name: string, value: string, options?: { expires?: number | Date, path?: string, domain?: string, secure?: boolean, sameSite?: 'Lax' | 'None' | 'Strict'}): void;

```typescript
cookieService.set('test', 'Hello World');
cookieService.set('test', 'Hello World', { expires: 2, sameSite: 'Lax' });
```

Sets a cookie with the specified `name` and `value`. It is good practice to specify a path. If you are unsure about the
path value, use `'/'`. If no path or domain is explicitly defined, the current location is assumed. `sameSite` defaults
to `Lax`.

**Important:** For security reasons, it is not possible to define cookies for other domains. Browsers do not allow this.
Read [this](https://stackoverflow.com/a/1063760) and [this](https://stackoverflow.com/a/17777005/1007003) StackOverflow
answer for a more in-depth explanation.

**Important:** Browsers do not accept cookies flagged sameSite = 'None' if secure flag isn't set as well. CookieService
will override the secure flag to true if sameSite='None'.

### delete( name: string, path?: string, domain?: string, secure?: boolean, sameSite: 'Lax' | 'None' | 'Strict' = 'Lax'): void;

```typescript
cookieService.delete('test');
```

Deletes a cookie with the specified `name`. It is best practice to always define a path. If you are unsure about the
path value, use `'/'`.

**Important:** For security reasons, it is not possible to delete cookies for other domains. Browsers do not allow this.
Read [this](https://stackoverflow.com/a/1063760) and [this](https://stackoverflow.com/a/17777005/1007003) StackOverflow
answer for a more in-depth explanation.

### deleteAll( path?: string, domain?: string, secure?: boolean, sameSite: 'Lax' | 'None' | 'Strict' = 'Lax' ): void;

```typescript
cookieService.deleteAll();
```

Deletes all cookies that can currently be accessed. It is best practice to always define a path. If you are unsure about
the path value, use `'/'`.

## General tips

Checking out the following resources usually solves most of the problems people seem to have with this cookie service:

- [article about cookies in general @MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies) (recommended read!)
- [common localhost problems @StackOverflow](https://stackoverflow.com/questions/1134290/cookies-on-localhost-with-explicit-domain)
- [problems with secure cookies @StackOverflow](https://stackoverflow.com/questions/8064318/how-to-read-a-secure-cookie-using-javascript)
- [how do browser cookie domains work? @StackOverflow](https://stackoverflow.com/questions/1062963/how-do-browser-cookie-domains-work)
- [get cookies from different paths](https://github.com/7leads/ngx-cookie-service/issues/7#issuecomment-351321518)
