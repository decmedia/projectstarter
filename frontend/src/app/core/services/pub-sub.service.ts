import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class PubSubService {

	private subjects: Subject<any>[] = [];
	private connect: boolean = false;

	public _getConnect(): boolean {
		return this.connect;
	}
	public _setConnect(status: boolean): void {
		this.connect = true;
	}

	publish(eventName: string, params: any = null): void {
		// ensure a subject for the event name exists
		(this.subjects as { [key: string]: any })[eventName] = (this.subjects as { [key: string]: any })[eventName] ?? new Subject<any>();

		// publish event
		(this.subjects as { [key: string]: any })[eventName].next(params);
	}

	on(eventName: string): Observable<any> {
		// ensure a subject for the event name exists
		(this.subjects as { [key: string]: any })[eventName] = (this.subjects as { [key: string]: any })[eventName] ?? new Subject<any>();

		// return observable
		return (this.subjects as { [key: string]: any })[eventName].asObservable();
	}
}
