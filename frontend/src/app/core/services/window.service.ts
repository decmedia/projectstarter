import {Injectable} from '@angular/core';

export interface Window {
	awt: any;
	opener: any;
	close: any;
	DOMParser: any;
}

function getWindow(): any {
	return window;
}
function getWindowLocation(): any {
	return window.location.origin;
}

@Injectable()
export class WindowRefService {
	private _window: any;
	constructor() {
		this._window = getWindow();
	}

	/*
	 * Константы ширины экрана по точкам bootstrap
	 * */
	getMediaWidthPoints(): string {
		const screen = this.getScreen();
		if (screen.width < 575) {
			return 'xs';
		} else if (screen.width >= 575 && screen.width < 767) {
			return 'sm';
		} else if (screen.width >= 767 && screen.width < 1199) {
			return 'md';
		} else {
			return 'lg';
		}
	}

	public get nativeWindow(): any {
		return this._window;
	}
	public get getWindowLocation(): any {
		return getWindowLocation();
	}

	public getScreen(): {
		width: number;
		height: number;
	} {
		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
		return {
			width: width,
			height: height,
		};
	}

	// tslint:disable-next-line:cyclomatic-complexity
	public popup(url: string, title: string, w: number, h: number): any {
		const window = this._window;

		// Fixes dual-screen position                         Most browsers      Firefox
		const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : 100;
		const dualScreenTop = window.screenTop !== undefined ? window.screenTop : 100;

		const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		const left = width / 2 - w / 2 + dualScreenLeft;
		const top = height / 2 - h / 2 + dualScreenTop;
		const newWindow = window.open(url, title, `scrollbars=yes, width=${w}, height=${h}, top=${top}, left=${left}`);

		// Puts focus on the newWindow
		if (window.focus) {
			newWindow.focus();
		}
		return newWindow;
	}
}
