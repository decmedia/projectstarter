import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IMediaFolder} from "../../../models/MediaFolder";
import {AppSettings} from "../../../config";
import {debug, RxJsLoggingLevel} from "../../util/debug";

@Injectable({
	providedIn: 'root',
})
export class MediaFolderBackendService {

	constructor(private http: HttpClient) {}

	/**
	 * Получить детей категории
	 * (все, для которых переданный номер является родителем. Если передан 0, то вернет корневые)
	 *
	 * @param id parent id
	 */
	children(id: number = 0): Observable<IMediaFolder[]> {
		return this.http.get<IMediaFolder[]>(`${AppSettings.API_ENDPOINT}/media/folder/${id}/children`);
	}

	/**
	 * Дерево целиком, всех существующих категорий
	 */
	tree(): Observable<IMediaFolder[]> {
		return this.http.get<IMediaFolder[]>(`${AppSettings.API_ENDPOINT}/media/folder`)
			.pipe(
				debug(RxJsLoggingLevel.DEBUG, 'Loading media folder tree collection from MediaFolderBackendService'),
			);
	}

	/**
	 * Плоское дерево предков категории с переданным номером
	 * @param id parent folder id
	 */
	ancestors(id: number = 0): Observable<IMediaFolder[]> {
		return this.http.get<IMediaFolder[]>(`${AppSettings.API_ENDPOINT}/media/folder/${id}/ancestors`);
	}

	/**
	 * @param name new folder name
	 * @param parent_id parent folder id
	 * @return Observable id new folder
	 */
	create(name: string, parent_id: number | null = null): Observable<IMediaFolder> {
		return this.http.post<IMediaFolder>(`${AppSettings.API_ENDPOINT}/media/folder/`, JSON.stringify({name, parent_id}));
	}

	/**
	 * @param id folder id to be deleted
	 */
	delete(id: number): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/media/folder/${id}`);
	}
}
