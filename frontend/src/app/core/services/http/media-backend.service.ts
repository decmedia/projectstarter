import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPagination} from "../../../models/Pagination";
import {IMedia} from "../../../models/Media";
import {IData} from "../../../models/Data";
import {AppSettings} from "../../../config";
import {debug, RxJsLoggingLevel} from "../../util/debug";

@Injectable({
	providedIn: 'root'
})
export class MediaBackendService {

	constructor(private http: HttpClient) {}

	list(params: any = {}): Observable<IMedia[]> {
		return this.http.get<IMedia[]>(`${AppSettings.API_ENDPOINT}/file`, {params}).pipe(
			debug(RxJsLoggingLevel.DEBUG, 'Loading media collection from MediaBackendService'),
			// shareReplay()
		);
	}



	/*list(params: any): Observable<IMediaResponse> {
		return this.http.get<IMediaResponse>(`${AppSettings.API_ENDPOINT}/file`, {params});
	}*/

	rename(id: number, name: string): Observable<null> {
		return this.http.post<null>(`${AppSettings.API_ENDPOINT}/file/${id}/rename`, JSON.stringify({name}));
	}

	moveToFolder(id: number | number[], folder_id: number): Observable<boolean> {
		return this.http.post<boolean>(`${AppSettings.API_ENDPOINT}/file/${id}/move`, JSON.stringify({folder_id}));
	}

	preview(id: number): Observable<IMedia> {
		return this.http.get<IMedia>(`${AppSettings.API_ENDPOINT}/file/${id}`);
	}

	delete(id: number | number[]): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/file/${id}`)
	}

}

export interface IMediaResponse extends IData<IMedia[]> {
	pagination: IPagination
}
