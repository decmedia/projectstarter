import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, publishReplay, refCount} from 'rxjs/operators';
import {AppSettings} from "../../config";
import {IUser} from "../../models/User";
import {IPreferences} from "../../models/Preferences";
import {ISession} from "../../models/Session";
import {ITwoFaStatus} from "../../models/TwoFaStatus";
import {ITwoFaQr} from "../../models/TwoFaQr";
import {IData} from "../../models/Data";
import {IPagination} from "../../models/Pagination";
import {IActivity} from "../../models/Activity";

@Injectable()
export class HttpService {

	deviceId ?: string;
	token ?: string;
	user$: Observable<IUser> | null = null;
	// @ts-ignore
	twoFaStatus$: Observable<ITwoFaStatus> | null = null;

	constructor(
		private http: HttpClient
	) {}


	user(): Observable<IUser> {
		if (!this.user$) {
			this.user$ = this.http.get<IUser>(`${AppSettings.API_ENDPOINT}/user/view`).pipe(publishReplay(), refCount());
		}

		return this.user$;
	}

	clearCache() {
		this.user$ = null;
		this.twoFaStatus$ = null;

		return this;
	}


	// PROFILE
	saveUserProfile(user: IUser): Observable<number> {
		return this.http.put<number>(AppSettings.API_ENDPOINT + '/user/profile', JSON.stringify(user));
	}

	saveUserPassword(old_password: string, new_password: string): Observable<number> {
		return this.http.put<number>(AppSettings.API_ENDPOINT + '/user/password', JSON.stringify({old_password, new_password}));
	}

	preferences(): Observable<IPreferences> {
		return this.http.get<IPreferences>(`${AppSettings.API_ENDPOINT}/user/preferences`);
	}

	updatePreferences(data: any): Observable<IPreferences> {
		return this.http.put<IPreferences>(`${AppSettings.API_ENDPOINT}/user/preferences`, JSON.stringify(data));
	}

	resendEmailVerify(): Observable<IPreferences> {
		return this.http.post<IPreferences>(`${AppSettings.API_ENDPOINT}/user/resend-email-verify`, {});
	}

	sessions(): Observable<ISession[]> {
		return this.http.get<ISession[]>(AppSettings.API_ENDPOINT + '/user/sessions');
	}

	actions(params: any): Observable<IActivityResponse> {
		return this.http.get<IActivityResponse>(AppSettings.API_ENDPOINT + '/user/security', {params});
	}

	closeOtherSessions(): Observable<boolean> {
		return this.http.delete<boolean>(AppSettings.API_ENDPOINT + '/user/sessions');
	}

	// Two Factor Authentication...
	twoFaAuthStatus(): Observable<ITwoFaStatus> {
		if (!this.twoFaStatus$) {
			this.twoFaStatus$ = this.http.get<ITwoFaStatus>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`).pipe(publishReplay(), refCount());
		}

		return this.twoFaStatus$;
	}

	twoFaQr(): Observable<ITwoFaQr> {
		return this.http.get<ITwoFaQr>(`${AppSettings.API_ENDPOINT}/user/two-factor-qr`);
	}

	twoFaEnable(data: any): Observable<boolean> {
		return this.http.post<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`, JSON.stringify(data));
	}

	twoFaGenBackupCodes(data: any): Observable<string[]> {
		return this.http.post<string[]>(`${AppSettings.API_ENDPOINT}/user/two-factor/recovery-code`, JSON.stringify(data));
	}

	twoFaDisable(): Observable<boolean> {
		return this.http.delete<boolean>(`${AppSettings.API_ENDPOINT}/user/two-factor/authenticator`);
	}


	// AUTH
	login(data: any): Observable<boolean> {
		return this.http
			.post<IResponseToken>(`${AppSettings.API_ENDPOINT}/auth`, JSON.stringify(data))
			.pipe(
				map(res => {
					this.setToken(res.token);
					this.user$ = of(res.user);
					return true;
				})
			);
	}

	register(data: any): Observable<boolean> {
		return this.http
			.post<IResponseToken>(`${AppSettings.API_ENDPOINT}/auth/register`, JSON.stringify(data))
			.pipe(
				map(res => {
					this.setToken(res.token);
					this.user$ = of(res.user);
					return true;
				})
			);
	}

	logout(): void {
		localStorage.removeItem(AppSettings.APP_PREFIX + 'token');
		localStorage.removeItem(AppSettings.APP_PREFIX + 'roles');
	}

	getToken(): string {
		if (!this.token) {
			this.token = localStorage.getItem(`${AppSettings.APP_PREFIX}token`) as string;
		}

		return this.token;
	}

	setToken(token: string): void {
		this.token = token;
		localStorage.setItem(`${AppSettings.APP_PREFIX}token`, token);
	}

	/**
	 * Сброс пароля
	 * 3 этапа:
	 * - запрос контакта/логина и поиск по нему
	 * - ввод кода подтверждения что пришел на контакт из профиля пользователя
	 * (либо выбор контакта на который надо отправить код, если их несколько и он не был указан явно)
	 * - создание нового пароля
	 */

	// Шаг 1: Проверка переданного контакта на существование
	rememberRequest(contact: string): Observable<number> {
		return this.http.post<number>(AppSettings.API_ENDPOINT + '/auth/rememberRequest', JSON.stringify({contact}));
	}

	// Шаг 2: Проверка переданного кода на корректность
	rememberConfirm(request_id: string, code: string): Observable<boolean> {
		return this.http.post<boolean>(AppSettings.API_ENDPOINT + '/auth/rememberConfirm', JSON.stringify({request_id, code}));
	}

	// Шаг 3: Создание нового пароля
	rememberSetPassword(request_id: string, code: string, new_password: string, new_password_confirmation: string): Observable<string> {
		return this.http.post<string>(`${AppSettings.API_ENDPOINT}/auth/rememberSetPassword`, JSON.stringify(
			{request_id, code, new_password, new_password_confirmation}
		));
	}
}

export interface ISessionsResponse extends IData<ISession[]>, IPagination {}
export interface IActivityResponse extends IData<IActivity[]>, IPagination {}
interface IResponseToken {
	token: string;
	user: IUser;
}
