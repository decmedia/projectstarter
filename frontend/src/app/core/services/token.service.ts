import {Injectable} from '@angular/core';
import {AppSettings} from "../../config";
import {StateService} from "./state.service";
import {IUser} from "../../models/User";
import {Observable} from "rxjs";
import {debug, RxJsLoggingLevel} from "../util/debug";

@Injectable({
	providedIn: 'root',
})
export class TokenService extends StateService<IToken> {

	token$: Observable<IUser | null> = this.state$.asObservable().pipe(
		debug(RxJsLoggingLevel.DEBUG, 'Loading token from TokenService')
	);

	get token(): string {
		return this.state$.getValue().token;
	}

	constructor() {
		super({token: localStorage.getItem(`${AppSettings.APP_PREFIX}token`) as string || ''});
	}

	setToken(token: string): void {
		this.setState({token});
		localStorage.setItem(`${AppSettings.APP_PREFIX}token`, token);
	}

	logout(): void {
		localStorage.removeItem(`${AppSettings.APP_PREFIX}token`);
		this.setState({token: ''});
	}
}

export interface IToken {
	token: string;
}
