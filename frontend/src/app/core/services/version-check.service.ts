import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PubSubService} from './pub-sub.service';
import {first} from 'rxjs/operators';

@Injectable()
export class VersionCheckService {
	// this will be replaced by actual hash post-build.js
	// При сборке проекта в CI (TeamCity), после ее завершения, скрипт post-build.js
	// заменяет строку {{POST_BUILD_ENTERS_HASH_HERE}} на полученный номер хеша сборки
	// Хеш сборки получается из имени файла по регулярному выражению /^main-es2015.?([a-z0-9]*)?.js$/
	// Также CI прописывает текущую версию в файл package.json, откуда ее и берет post-build.js
	private currentHash = '{{POST_BUILD_ENTERS_HASH_HERE}}';

	constructor(private http: HttpClient, private pubSubService: PubSubService) {}

	/**
	 * Checks in every set frequency the version of frontend application
	 */
	public initVersionCheck(url: string, frequency: number = 1000 * 60 * 60 * 6): void {
		setInterval(() => {
			this.checkVersion(url);
		}, frequency);
	}

	/**
	 * Will do the call and check if the hash has changed or not
	 */
	private checkVersion(url: string): void {
		// timestamp these requests to invalidate caches
		this.http
			.get(`${url}?t=${new Date().getTime()}`)
			.pipe(first())
			.subscribe(
				(response: any) => {
					const version = response.version;
					const hash = response.hash;
					const hashChanged = this.hasHashChanged(this.currentHash, hash);

					this.pubSubService.publish('update', {status: hashChanged, version});

					// store the new hash so we wouldn't trigger versionChange again
					// only necessary in case you did not force refresh
					this.currentHash = hash;
				},
				err => {
					console.error(err, 'Could not get version');
				}
			);
	}

	/**
	 * Checks if hash has changed.
	 * This file has the JS hash, if it is a different one than in the version.json
	 * we are dealing with version change
	 */
	private hasHashChanged(currentHash: string, newHash: string): boolean {
		if (!currentHash || currentHash === '{{POST_BUILD_ENTERS_HASH_HERE}}') {
			return false;
		}

		return currentHash !== newHash;
	}
}
