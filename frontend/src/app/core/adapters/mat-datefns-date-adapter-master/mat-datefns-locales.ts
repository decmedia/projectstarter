import {InjectionToken} from '@angular/core';
import {Locale} from 'date-fns';

export const MAT_DATEFNS_LOCALES = new InjectionToken<Locale[]>(
	'MAT_DATEFNS_LOCALES'
);
