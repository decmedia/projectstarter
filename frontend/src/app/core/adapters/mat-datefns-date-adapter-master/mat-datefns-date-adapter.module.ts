import {NgModule} from '@angular/core';
import {
	DateAdapter,
	MAT_DATE_FORMATS,
	MAT_DATE_LOCALE,
} from '@angular/material/core';
import {MAT_DATEFNS_DATE_FORMATS} from './mat-datefns-date-formats';
import {DateFnsDateAdapter, MAT_DATEFNS_DATE_ADAPTER_OPTIONS} from './mat-datefns-date-adapter';
import {MAT_DATEFNS_LOCALES} from './mat-datefns-locales';

@NgModule({
	providers: [
		{
			provide: DateAdapter,
			useClass: DateFnsDateAdapter,
			deps: [MAT_DATE_LOCALE, MAT_DATEFNS_LOCALES, MAT_DATEFNS_DATE_ADAPTER_OPTIONS],
		},
	],
})
export class DateFnsDateModule {
}

@NgModule({
	imports: [DateFnsDateModule],
	providers: [
		{provide: MAT_DATE_FORMATS, useValue: MAT_DATEFNS_DATE_FORMATS},
		{provide: MAT_DATEFNS_LOCALES, useValue: []},
	],
})
export class MatDateFnsDateModule {
}
