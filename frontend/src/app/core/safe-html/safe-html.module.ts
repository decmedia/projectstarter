import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {QwSafeHtmlPipe} from "./safa-html.pipe";

@NgModule({
	imports: [CommonModule],
	exports: [QwSafeHtmlPipe],
	declarations: [QwSafeHtmlPipe]
})
export class QwSafeHtmlModule {
}
