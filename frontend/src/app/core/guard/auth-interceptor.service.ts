import {Observable, of, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {
	HttpEvent,
	HttpInterceptor,
	HttpHandler,
	HttpRequest,
	HttpErrorResponse,
	HttpResponse
} from '@angular/common/http';
import {PubSubService, TokenService} from "../services";
import {CookieService} from "../services/cookie-service/cookie.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	constructor(
		private router: Router,
		private pubSubService: PubSubService,
		private tokenService: TokenService,
		private cookieService: CookieService,
	) {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		if (req.headers.get('skip')) {
			return next.handle(req);
		}

		const token = this.tokenService.token;

		let params;
		if (req.body instanceof FormData) {
			params = {
				headers: req.headers
					.set('Authorization', 'Bearer ' + token)
					.set('Accept', 'application/json')
					.set('X-Requested-With', 'XMLHttpRequest')
					.set('X-device', this.cookieService.get('device')),
			};
		} else {
			params = {
				headers: req.headers
					.set('Authorization', 'Bearer ' + token)
					.set('Content-Type', 'application/json')
					.set('Accept', 'application/json')
					.set('X-Requested-With', 'XMLHttpRequest')
					.set('X-device', this.cookieService.get('device')),
			};
		}

		const authReq = req.clone(params);
		return next.handle(authReq)
			.pipe(
				// Извлекаем поле data из всего тела ответа
				map((event: HttpEvent<any>): any => {
					if (event instanceof HttpResponse) {
						const body = (!event.body.pagination && (event.body.data || event.body.data === false || event.body.data === 0)) ? event.body.data : event.body;
						return event.clone({body});
					} else {
						return event;
					}
				})
			)
			.pipe(catchError((x: HttpErrorResponse) => this.handleError(x)));
	}

	private handleError(err: HttpErrorResponse): Observable<any> {
		if (err.status === 401) {
			const url = this.router.url;
			this.router.navigate(['/auth'], {queryParams: {returnUrl: url}});

			// if you've caught / handled the error, you don't want to rethrow
			// it unless you also want downstream consumers to have to handle it as well.
			return of(err.message);
		} else {
			this.pubSubService.publish('core', err);
		}
		return throwError(err);
	}
}
