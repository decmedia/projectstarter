import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {TokenService} from "../services";

@Injectable()
export class TokenGuard implements CanActivate {
	constructor(
		private router: Router,
		private tokenService: TokenService
	) {}

	public isLoggedIn(): boolean {
		return Boolean(this.tokenService.token);
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		if (this.isLoggedIn()) {
			return true;
		} else {
			this.router.navigate(['/auth'], {queryParams: {returnUrl: state.url}});
			return false;
		}
	}
}
