import {NgModule} from '@angular/core';
import {LazyLoadingDirective} from './lazy-loading.directive';

@NgModule({
	declarations: [LazyLoadingDirective],
	exports: [LazyLoadingDirective],
})
export class LazyLoadingModule {}
