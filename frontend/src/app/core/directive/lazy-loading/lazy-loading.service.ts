import {ChangeDetectorRef, Inject, Injectable} from '@angular/core';
import {IntersectionObserverService} from '@ng-web-apis/intersection-observer';
import {Observable, of, Subject} from 'rxjs';
import {catchError, filter, mapTo, switchMap, take, takeUntil} from 'rxjs/operators';
import {DestroyService} from "../../services";
import {watch} from "../../observables/watch";

@Injectable()
export class LazyLoadingService extends Observable<string> {
	private src$ = new Subject<string>();

	constructor(
		@Inject(ChangeDetectorRef) changeDetectorRef: ChangeDetectorRef,
		@Inject(DestroyService) destroy$: Observable<void>,
		@Inject(IntersectionObserverService)
			intersections$: Observable<IntersectionObserverEntry[]>,
	) {
		super(subscriber =>
			this.src$
				.pipe(
					switchMap(src =>
						intersections$.pipe(
							filter(([{isIntersecting}]) => isIntersecting),
							mapTo(src),
							catchError(() => of(src)),
							watch(changeDetectorRef),
							take(1),
						),
					),
					takeUntil(destroy$),
				)
				.subscribe(subscriber),
		);
	}

	next(src: string) {
		this.src$.next(src);
	}
}
