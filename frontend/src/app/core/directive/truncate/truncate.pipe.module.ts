import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TruncatePipe} from "./truncate.pipe";

@NgModule({
	imports: [CommonModule],
	exports: [TruncatePipe],
	declarations: [TruncatePipe]
})
export class TruncatePipeModule {
}
