import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {QwAdDirective} from './ad.directive';

@NgModule({
	imports: [CommonModule],
	exports: [QwAdDirective],
	declarations: [QwAdDirective]
})
export class QwAdModule {
}
