import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
	selector: '[adHost]',
})
export class QwAdDirective {
	constructor(public viewContainerRef: ViewContainerRef) { }
}
