import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {formatTimePipe} from "./format-time.pipe";

@NgModule({
	imports: [CommonModule],
	exports: [formatTimePipe],
	declarations: [formatTimePipe]
})
export class FormatTimeModule {
}
