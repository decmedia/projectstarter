import { Pipe, PipeTransform } from '@angular/core';
import differenceInHours from 'date-fns/differenceInHours';
import differenceInYears from 'date-fns/differenceInYears'
import {format} from 'date-fns';
import {ru} from 'date-fns/locale';

@Pipe({name: 'formatTime'})
export class formatTimePipe implements PipeTransform {
	transform(value: any, template: any = null): string {
		if (value) {
			const date = typeof value === 'string' ? new Date(value) : value;
			const tpl = template
				?? (differenceInHours(new Date, date) > 12
					? differenceInYears(new Date, date) > 0
						? 'dd.MM.y'
						: 'dd.MM HH:mm'
					: 'HH:mm'
				);

			return format(date, tpl, { locale: ru });
		}

		return '';
	}
}
