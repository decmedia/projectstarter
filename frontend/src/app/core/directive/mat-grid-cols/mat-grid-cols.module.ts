import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {GridColsDirective} from "./mat-grid-cols.directive";

@NgModule({
	imports: [CommonModule],
	exports: [GridColsDirective],
	declarations: [GridColsDirective]
})
export class MatGridColsDirectiveModule {
}
