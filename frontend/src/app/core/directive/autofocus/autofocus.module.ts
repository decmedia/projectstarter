import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {qwPrefixFocusAndSelect} from './autofocus.directive';

@NgModule({
	imports: [CommonModule],
	exports: [qwPrefixFocusAndSelect],
	declarations: [qwPrefixFocusAndSelect]
})
export class QwFocusModule {
}
