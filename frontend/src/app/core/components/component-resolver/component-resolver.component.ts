import {
	ChangeDetectorRef,
	Component,
	ComponentFactoryResolver,
	Inject,
	Input, OnChanges,
	OnInit, SimpleChanges,
	Type,
	ViewChild
} from "@angular/core";
import {changeDetection} from "../../util";
import {QwAdDirective} from "../../directive/ad";
import {DestroyService} from "../../services";

@Component({
	selector: 'app-component-resolver',
	template: '<ng-template adHost></ng-template>',
	styles: [':host { display: block; height: inherit; width: inherit; }'],
	changeDetection
})
export class ComponentResolverComponent implements OnInit, OnChanges {

	@Input() data ?: any = {};
	@Input() path !: any;
	@ViewChild(QwAdDirective, {static: true}) adHost!: QwAdDirective;

	constructor(
		private ref: ChangeDetectorRef,
		private componentFactoryResolver: ComponentFactoryResolver,
		@Inject(DestroyService) private readonly destroy$: DestroyService,
	) {

	}

	ngOnInit() {
		this.path.then((c: Type<unknown>) => {
			const componentFactory = this.componentFactoryResolver.resolveComponentFactory(c);
			const viewContainerRef = this.adHost.viewContainerRef;
			viewContainerRef.clear();

			const componentRef = viewContainerRef.createComponent<any>(componentFactory);
			componentRef.instance.data = this.data;

			if (this.data instanceof Object) {
				for (const [key, value] of Object.entries(this.data)) {
					componentRef.instance[key] = value;
				}
			}

			this.ref.markForCheck();
		});
	}

	ngOnChanges(changes: SimpleChanges) {

	}
}
