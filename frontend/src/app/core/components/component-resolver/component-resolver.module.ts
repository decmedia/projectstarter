import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComponentResolverComponent} from "./component-resolver.component";
import {QwAdModule} from "../../core/directive/ad";
import {QwAdModule} from "../../directive/ad";

@NgModule({
	declarations: [
		ComponentResolverComponent
	],
    imports: [
        CommonModule,
        QwAdModule,
        QwAdModule,
    ],
	exports: [
		ComponentResolverComponent
	],
	providers: [],
})
export class ComponentResolverModule {
}
