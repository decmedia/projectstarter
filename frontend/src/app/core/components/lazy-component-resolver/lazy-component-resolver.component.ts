import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, Type} from "@angular/core";
import {changeDetection} from "../../util";

@Component({
	selector: 'app-lazy-component-resolver',
	template: `
		<ndc-dynamic
			[ndcDynamicComponent]="component"
			[ndcDynamicInputs]="inputs"
			[ndcDynamicOutputs]="outputs"
			(ndcDynamicCreated)="componentCreated($event)"
		></ndc-dynamic>`,
	styles: [':host { display: block; height: inherit; width: inherit; }'],
	changeDetection
})
export class LazyComponentResolverComponent implements OnInit {

	@Input('component') path !: any;

	@Input() outputs?: any = {};
	@Input() inputs?: any = {};
	@Output() created = new EventEmitter<any>();

	component!: Type<any>;

	constructor(private ref: ChangeDetectorRef) {}

	ngOnInit() {
		this.path.then((component: Type<any>) => {
			this.component = component;
			this.ref.markForCheck();
		});
	}

	componentCreated(ev: any) {
		console.log('componentCreated');
		this.created.next(true);
	}
}
