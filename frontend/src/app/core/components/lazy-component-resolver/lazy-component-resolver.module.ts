import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LazyComponentResolverComponent} from "./lazy-component-resolver.component";
import {DynamicModule} from "../dynamic-component/lib/dynamic.module";

@NgModule({
	declarations: [
		LazyComponentResolverComponent
	],
	imports: [
		CommonModule,
		DynamicModule,
	],
	exports: [
		LazyComponentResolverComponent
	],
	providers: [],
})
export class LazyComponentResolverModule {
}
