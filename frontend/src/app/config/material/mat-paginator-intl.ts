import {Injectable} from '@angular/core';
import {MatPaginatorIntl} from "@angular/material/paginator";
import {TranslateService} from "@ngstack/translate";

@Injectable()
export class CustomMatPaginatorIntl extends MatPaginatorIntl {
	constructor(private translate: TranslateService) {
		super();

		this.translate.activeLangChanged.subscribe((event: { previousValue: string; currentValue: string }) => {
			this.getAndInitTranslations();
		});

		this.getAndInitTranslations();
	}

	getAndInitTranslations() {
		this.itemsPerPageLabel = this.translate.get('PAGINATOR.ITEMS_PER_PAGE');
		this.nextPageLabel = this.translate.get('PAGINATOR.NEXT_PAGE');
		this.previousPageLabel = this.translate.get('PAGINATOR.PREVIOUS_PAGE');
		this.changes.next();
	}

	getRangeLabel = (page: number, pageSize: number, length: number) =>  {
		if (length === 0 || pageSize === 0) {
			return `0 / ${length}`;
		}
		length = Math.max(length, 0);
		const startIndex = page * pageSize;
		const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
		return `${startIndex + 1} - ${endIndex} / ${length}`;
	}
}
