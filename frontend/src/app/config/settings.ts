import {environment} from '../../environments/environment';

export class AppSettings {
	public static APP_NAME = 'Example';
	public static APP_URL = environment.production ? 'https://example.com' : 'http://projectstarter.local';
	public static API_ENDPOINT = environment.production ? '/api' : AppSettings.APP_URL + '/api';
	public static APP_VERSION = '0.0.0';
	public static APP_PREFIX = 'ex_';
	public static APP_EMAIL_SUPPORT = 'support@example.com';

	// Websockets
	public static WS_ENABLED = false;
	public static WS_HOST = environment.production ? 'projectstarter.local' : '127.0.0.1';
	public static WS_PORT = environment.production ? 6001 : 60010;

	// Broadcasting
	public static BROADCASTING_AUTH = AppSettings.APP_URL + '/broadcasting/auth';
	public static PUSHER_APP_KEY = 'ex';
}
