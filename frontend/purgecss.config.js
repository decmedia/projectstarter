const dist = '../backend/public/assets/angular';
module.exports = {
	content: [`${dist}/index.html`, `${dist}/*.js`],
	css: [`${dist}/*.css`],
	defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
	output: `${dist}/`,
};
