const colors = require('tailwindcss/colors');
module.exports = {
	mode: 'jit',
	prefix: '',
	purge: {
		enabled: process.env.NODE_ENV === 'production',
		content: [
			'./src/**/*.{html,ts}',
		]
	},
	darkMode: 'class', // or 'media' or 'class'
	theme: {
		extend: {},
		screens: {
			sm: '640px',
			md: '768px',
			lg: '1024px',
			xl: '1280px',
			'2xl': '1420px',
		},
		colors: {
			transparent: 'transparent',
			current: 'currentColor',
			box: '#f7fafc',
			black: '#1a1f36',
			white: colors.white,
			default: '#3c4257',
			coop: {
				50: '#f8fafb',
				100: '#f1f5f9',
				200: '#e2e8f0',
				300: '#b8c1df',
				400: '#a8b1ce',
				500: '#615e9b',
				600: '#514e80',
				700: '#334155',
				800: '#1e293b',
				900: '#0f172a',
			},
			gray: colors.gray,
			red: {
				50: '#fef2f2',
				100: '#fee2e2',
				200: '#fecaca',
				300: '#fca5a5',
				400: '#ff6370', // custom
				500: '#ef4444',
				600: '#dc2626',
				700: '#b91c1c',
				800: '#991b1b',
				900: '#7f1d1d',
			},
			indigo: colors.indigo,
			yellow: colors.amber,
			green: colors.green,
			// blue: colors.blue,
			// purple: colors.violet,
			// pink: colors.pink,
		},
	},
	variants: {
		extend: {},
	},
	// plugins: [require('@tailwindcss/forms')],
};
